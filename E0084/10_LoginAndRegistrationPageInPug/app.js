var express = require('express');

var app = express();

app.set("view engine",'pug')


app.get('/', function(req, res){
	
   res.sendFile(__dirname+'/views/login.pug');
   res.render('login.pug');
});


app.get('/login', function(req, res){
	
   res.sendFile(__dirname+'/views/login.pug');
   res.render('login.pug');
});


app.get('/loginStyle.css', function(req, res){
   res.sendFile(__dirname + "/" + '/views/loginStyle.css');
});


app.get('/register', function(req, res){
	
   res.sendFile(__dirname+'/views/register.pug');
   res.render('register.pug');
});


app.get('/registerStyle.css', function(req, res){
   res.sendFile(__dirname + "/" + '/views/registerStyle.css');
});


app.get('/download.png', function(req, res){
   res.sendFile(__dirname + '/views/download.png');
});


app.listen(3013);
console.log('listening to port 3013');
