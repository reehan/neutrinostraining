import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponentComponent } from './login-component/login-component.component';
import { IndexComponentComponent } from './index-component/index-component.component';

const routes: Routes = [
  {
    path:'',
  component:IndexComponentComponent
},
  {
  path:'login',
  component:LoginComponentComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
