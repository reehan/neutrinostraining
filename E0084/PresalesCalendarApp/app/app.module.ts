import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponentComponent } from './login-component/login-component.component';
import { MatNativeDateModule,MatTabsModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IndexComponentComponent } from './index-component/index-component.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponentComponent,
    IndexComponentComponent
  ],
  imports: [
    BrowserModule, 
    BrowserAnimationsModule,
    MatNativeDateModule,
    MatTabsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
