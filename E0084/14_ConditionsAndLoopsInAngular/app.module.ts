import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ErrorComponent } from './error/error.component';

import { FormsModule } from '@angular/forms';
import { WarnComponent } from './warn/warn.component';

@NgModule({
  declarations: [
    AppComponent,
    ErrorComponent,
    WarnComponent //created by itself
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
