import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http:HttpClient,private router: Router) { }

  postData(LoginData:any) {
  console.log('in posting data',LoginData);
   return this.http.post('http://localhost:3001/api/sucess ', LoginData)
    .subscribe(replyFromServer => {
    	console.log('recived response', replyFromServer);

      if(replyFromServer.loginStatus === true){
      this.router.navigate(['/invoice']);
       }
       else
       {
 		console.log('ENtered here');     
      	this.router.navigate(['/login'])
      }
    } 
    );

    
  }
}
