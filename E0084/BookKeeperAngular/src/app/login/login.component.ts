import { Component, OnInit } from '@angular/core';
import { NgxPasswordToggleModule } from 'ngx-password-toggle';
import { FormGroup, FormControl } from '@angular/forms';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


    loginForm: FormGroup;
    submitted=false;
loading=false;


  constructor( private loginService: LoginService) { }

  ngOnInit() {
	  this.loginForm = new FormGroup({
	  email:new FormControl(),
	  password:new FormControl()
  	});
  }


//get f() { return this.loginForm.controls; 
  //    console.log(this.loginForm.controls);
    //}

     onSubmit() {
        this.submitted = true;
        this.loginService.postData(this.loginForm.value);
        // stop here if form is invalid
        
    this.loading = true;
       // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.loginForm.value))
       alert('successfully registered please login')
    }
   




}
