function Node(data) {
    this.data = data;      //data stores a value.
    this.parent = null;    //parent points to a node's parent.
    this.children = [];     //children points to the next node in the list.
}

function Tree(data) {
    var node = new Node(data);     //creates a new instance of Node
    this.root = node;               //assigns node as the root of a tree
}
 Tree.prototype.traverseDF = function(callback) {
 
    // this is a recurse and immediately-invoking function
    (function recurse(currentNode) {
      
        /* Enter a for loop and iterate once for each child of currentNode, 
         beginning with the first child. */ 
        for (var i = 0, length = currentNode.children.length; i < length; i++) {
            
          /*Inside the body of the for loop, invoke recurse with a child of currentNode.
          The exact child depends on the current iteration of the for loop.*/ 
            recurse(currentNode.children[i]);
        }
 
        /*When currentNode no longer has children, we exit the for loop and
      invoke the callback we passed during the invocation of traverseDF(callback).*/
        callback(currentNode);
 
        
    })(this.root);
 
};

//traverse a tree
var tree = new Tree(1);
 
tree.root.children.push(new Node(2));
tree.root.children[0].parent = tree;
 
tree.root.children.push(new Node(3));
tree.root.children[1].parent = tree;
 
tree.root.children.push(new Node(4));
tree.root.children[2].parent = tree;
 
tree.root.children[0].children.push(new Node(5));
tree.root.children[0].children[0].parent = tree.root.children[0];
 
tree.root.children[0].children.push(new Node(6));
tree.root.children[0].children[1].parent = tree.root.children[0];
 
tree.root.children[2].children.push(new Node(7));
tree.root.children[2].children[0].parent = tree.root.children[2];

//invoke traverseDF(callback).
tree.traverseDF(function(node) {
    console.log(node.data);
});