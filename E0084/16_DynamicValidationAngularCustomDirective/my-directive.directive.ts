import { Directive ,ElementRef,Renderer2,HostListener} from '@angular/core';

@Directive({
  selector: '[appMyDirective]'
})
export class MyDirectiveDirective {

  constructor(private elementRef: ElementRef, private renderer:
    Renderer2) { }
  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.renderer.setStyle(this.elementRef.nativeElement,'background-color', 'blue');
    this.renderer.setStyle(this.elementRef.nativeElement,'color', 'white');
    this.renderer.setStyle(this.elementRef.nativeElement,'font-size', '14px');
  }
  @HostListener('mouseenter') handleMouseEnter(eventData: Event) {
    this.renderer.setStyle(this.elementRef.nativeElement,
    'background-color', 'red');
    this.renderer.setStyle(this.elementRef.nativeElement,
      'color', 'white');
    }
    @HostListener('mouseleave') handleMouseExit(eventData: Event) {
      this.renderer.setStyle(this.elementRef.nativeElement,
      'background-color', 'blue');
      this.renderer.setStyle(this.elementRef.nativeElement,
        'color', 'white');
      }
}
