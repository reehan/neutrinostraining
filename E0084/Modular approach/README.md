# Book Keeper

Bookkeeper is an application that will record financial transactions, and is part of the process of accounting in business. Transactions include purchases, sales, receipts, and payments by an individual person or an organization/corporation.

## Getting Started

install node from  website nodejs.org
install necessary modules from npm packages
install mongodb from website mongodb

### Prerequisites


```
npm install -g nodemon

```

### Installing


```
npm install express-session --save
npm install body-parser --save
npm install express --save
npm install mongoose --save

```



### Running the application

start mongodb

for windows

```
net start mongoDB
 mongo
 
```

for linux

```
sudo service mongod start
mongo

```

start server that is nodemon 

```
nodemon app.js

```
### And coding style tests

allowing user to register, login, update, profile and transactions 

```
Give an example
```

## Deployment

deployed using MEAN technology

## Built With

* HTML5
* bootstrap
* Cascading style sheet
* javascript
* mongodb
* nodejs
* express
* angularjs

## Contributing

*create account in github and do contribute us with yours ideas
*do report bugs to wali@gmail.com, bhagya@gmial.com, sam@gmail.com

## Versioning

Beta version

## Authors

* **Shaik wali basha , Sampreeth D, Bhagya Patil** - *Initial work* - 
* **Bhagya patil** - *UI design work* - 
* **Sampreeth D** - *Registration work*-
* **Shaik wali basha** - *Login work and landing work*-

## License

This project is licensed under the GNU GPL License

## Acknowledgments

* youtube
* google 
* w3schools

