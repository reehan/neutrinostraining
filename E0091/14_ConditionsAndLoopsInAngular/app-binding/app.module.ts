import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { WarnComponent } from './warn/warn.component';
import { ErrComponent } from './err/err.component';

@NgModule({
  declarations: [
    AppComponent,
    WarnComponent,
    ErrComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
