const http=require("http");
const fs=require("fs");

var server = http.createServer(function(req, res){

	console.log('request was made: ' + req.url);

	if(req.url==="/home" || req.url==="/")
	{
    	res.writeHead(200, {'Content-Type':'text/html'});
    	var  readStream = fs.createReadStream(__dirname + "/registerForm.html", 'utf8');
    	readStream.pipe(res);
    }

   
	if(req.url==="/login.html")
	{
		res.writeHead(200, {'Content-Type':'text/html'});
		var  readStream = fs.createReadStream(__dirname + "/login.html");
	    readStream.pipe(res);
	}
	if(req.method === 'POST' && req.url === '/success.html')
	{
    	var data = "";
    	var uData ="";
		var userNameMap = new Map();
		emailMap = new Map();
    	req.on('data', function(chunk){data += chunk;console.log(data);})

 		req.on('end', function(){
			data=data.split("&");
			for(let i=0;i<data.length;i++)
			{
				let field=data[i].split("=");
				data[i]=field[1];
			}
			if(data[data.length-1]==="yes")
			{
				data.pop();
			}
			uData = fs.readFileSync('./UserData.txt', 'utf8');
			uData=uData.split("|");
			uData.pop();
			for(let j=0;j<uData.length;j++)
			{
				let eachUser=uData[j].split(",");
				for(let i=eachUser.length-1;i>=eachUser.length-3;i--)
				{
					if(eachUser[i]!=="" && eachUser[i]!=="yes")
					{
						userNameMap.set(eachUser[1],eachUser[i]);
						emailMap.set(eachUser[2],eachUser[i]);
						break;
					}
				}
			}
			var userNameKeys = [...userNameMap.keys()];
			var emailKeys = [...emailMap.keys()];
			console.log(userNameKeys);
			console.log(emailKeys);
			var user;
			var email;
			for(let q in userNameKeys)
			{
				if(userNameKeys[q]==data[0])
				{
					user=true;
					break;
				}
			}
			for(let q in emailKeys)
			{

				if(emailKeys[q]===data[0])
				{
					email=true;
					break;
				}
			}
			console.log(email+","+user);
			if((userNameMap.get(data[0])===data[1])|(emailMap.get(data[0])===data[1]))
			{
				res.writeHead(200, {'Content-Type':'text/html'});
				var  readStream = fs.createReadStream(__dirname + "/LoginSuccess.html");
	    		readStream.pipe(res);
			}
			else if(user | email)
			{
				res.writeHead(200, {'Content-Type':'text/html'});
				var  readStream = fs.createReadStream(__dirname + "/LoginAgain.html");
	    		readStream.pipe(res);
			}
			else
			{
				res.writeHead(200, {'Content-Type':'text/html'});
				var  readStream = fs.createReadStream(__dirname + "/SignUp.html");
	    		readStream.pipe(res);
			}
		})
	 };
    if(req.method === 'POST' && req.url === '/RegistrationSuccess.html')
    {
		var data = "";
		var split="";
		req.on('data', function(chunk){data += chunk;console.log(data);})
		req.on('end', function(){
		split=data.split("&");
		console.log(split);
		for(let i=0;i<split.length;i++)
		{
			let field=split[i];
			field=field.split("=");
			split[i]=field[1];
		}
		split=split.join(",");
		split=split+"|";
		console.log(split);
		fs.appendFileSync('./UserData.txt',split);
		})
		res.writeHead(200, {'Content-Type':'text/html'});
		var  readStream = fs.createReadStream(__dirname + "/RegistrationSuccess.html");
	    readStream.pipe(res);
	};
});

server.listen(3000, '127.0.0.1');
console.log('now listening to port 3000');