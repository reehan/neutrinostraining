const express = require('express');
const ejs = require('ejs');
const PORT = 3000;


var kings = new Map();
kings.set('Vikramaditya','vikramaditya');
kings.set('Akbar','akbar');
kings.set('Raja','rajakrishnachandra');

var navaratnas = new Map();
navaratnas.set('Vikramaditya', ['Amarasimha', 'Dhanvantari', 'Ghatakarapara', 'Kalidasa', 'Kshapanaka', 'Shanku', 'Varahamihira', 'Vararuchi', 'Vetala-Bhatta']);
navaratnas.set('Akbar', ['Abul-Fazl', 'Raja Todar Mal', 'Abdul Rahim Khan-I-Khana', 'Birbal', 'Mulla Do-Piyaza', 'Faizi', 'Fakir Aziao-Din', 'Tansen', 'Raja Man Singh']);
navaratnas.set('Raja', ['Gopal Bhar', 'Bharatchandra Ray', 'Ramprasad Sen']);

const app = express();
app.set('view engine', 'ejs');

app.get('/', function(req, res)
{
	console.log('Request was made to '+req.url);
   res.render('login.ejs');
});

app.post('/success.ejs', (request, response) => {
	var data='';
	request.on('data', function(chunk){
		data += chunk;
	});

	request.on('end', function(){
	console.log('Request was made to '+request.url);
	console.log('data '+data);

	data = data.split('&')
	let king = data[0].split('=');
	let pwd = data[1].split('=');
	king = king[1];
	pwd = pwd[1];
	console.log(king);
	if(kings.get(king) === pwd)
	{
		response.render('Navaratna.ejs', {kingName: king, ratnas:navaratnas.get(king)});
	}
	});

});

app.listen(PORT, () => {
    console.log('Listening to port:' + PORT);
});
