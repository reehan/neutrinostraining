const userData = require('../Model/model');
let date = require('date-and-time');
var find = function (username) {
	return new Promise(resolve => {
		dbUserData = {};
		userData.findOne({ userName: username }, function (err, dbresp) {
			if (err) {
				throw err;
			}
			if (dbresp === null) {
			dbUserData.found = false;
            resolve(dbUserData);
                
			} else {
				dbUserData.found = true;
				dbUserData.userObj = dbresp;
			}
			resolve(dbUserData);
		});
	});
}
var updateData = function (dbObject) {//to update log time and uname
	let now = new Date();
	return new Promise(
		resolve => {
			userData.update({ userName: dbObject.userName }, { $set: { 'logedIn': date.format(now, 'ddd MMM DD YYYY hh:mm A') }},
				function (err, result) {
					if (err) {
						console.log('error in updating log time', err);
						throw err;

					}
					else {
						console.log('updated log date n exit', result.ok,result.nMomdified);
						resolve(result);
					}
				});
		})
}
var save = function (userDbObject) {
	function getRandom(length) {

		  return Math.floor(Math.pow(10, length - 1) + Math.random() * 9 * Math.pow(10, length - 1));

	   }
	var userInfo = new userData({ userName: userDbObject.userName, userPassword: userDbObject.userPassword, userCompName: userDbObject.userCompName, userEmail: userDbObject.userEmail, userID: getRandom(9) });
	userInfo.save(function (err, DBresponse) {
		if (err) {
			throw err;
		}
	});

}
var passwordValid = function (userDbData) {
	return new Promise(resolve => {
		userData.findOne({ userName: userDbData.userName }, function (err, dbData) {
			if (err)
				throw err;
			if (dbData.userPassword === userDbData.userPassword) {
				resolve(true);
			}
			else {
				resolve(false);
			}
		})
	});
}
var userDao = {};
userDao.find = find;
userDao.save = save;
userDao.updateData = updateData;
userDao.passwordValid = passwordValid;
module.exports = userDao;