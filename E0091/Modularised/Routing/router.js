const express = require('express');
const bodyParser = require('body-parser');
const userService = require('../Service/userService');
const checkSignedIn = require('../SessionHandler');

var urlencodedParser = bodyParser.urlencoded({ extended: false });
const app = express();

var router = express.Router();

router.route('/')
	.get(function (req, res) {
		res.render('index');
	});
router.route('/login')
	.get(function (req, res) {
		msg1 = '';
		res.render('login', { msg: msg1 });
	});
router.route('/register')
	.get(function (req, res) {
		msg2 = '';
        res.render('register', { msg: msg2 });

	});
router.route('/invoice')
	.get(checkSignedIn, function (req, res) {
		userService.findUser(req.session.username).then(DBresponse => {
			res.render('invoice', { name: DBresponse.userName, mail: DBresponse.userEmail, compName: DBresponse.userCompName, Id: DBresponse.userID });
		});
	});
router.route('/invoicedir')
	.get(checkSignedIn, function (req, res) {
		userService.findUser(req.session.username).then(DBresponse => {
			res.render('invoicedir', { name: DBresponse.userName, mail: DBresponse.userEmail, compName: DBresponse.userCompName, Id: DBresponse.userID });
		});
	});
router.route('/logout')
	.get(function (req, res) {

		userService.findUserAndUpdate(req.session.username).then(resp => {
			console.log(resp)
		})

		req.session.destroy(function () {
			res.redirect('/');
		});
	})
router.route('/landing')
	.get(checkSignedIn, function (req, res) {
		userService.findUser(req.session.username).then(DBresponse => {
			console.log(DBresponse.userName);
			res.render('landing', { name: DBresponse.userName, mail: DBresponse.userEmail, compName: DBresponse.userCompName, Id: DBresponse.userID, login: DBresponse.logedIn });

		});

	})
router.route('/profile')
	.get(checkSignedIn, function (req, res) {
		userService.findUser(req.session.username).then(DBresponse => {
			res.render('profile', { compName: DBresponse.userCompName, Id: DBresponse.userID });

		});

	})
router.route('/register')
	.post(urlencodedParser, function (req, res) {
		var userData = {};
		userData.userName = req.body.uUserName;
		userData.userPassword = req.body.uPassword;
		userData.userCompName = req.body.uCompName;
		userData.userEmail = req.body.uEmail;
		console.log(userData);
		userService.registerIfNewUser(userData).then((result) => {
			console.log('router register, userService result', result);
			if (result.saved) {
				res.redirect('/login');
				// redirect to login page
			}
			else {
				res.render('register', { msg: 'User Name Exists' });
				// user already exist
			}
		},
			(error) => {
				console.log(error);
			});
	});
router.route('/login')
	.post(urlencodedParser, function (req, res) {
		var loginUserData = {};
		loginUserData.userName = req.body.uuName;
		loginUserData.userPassword = req.body.uuPassword;
		userService.loginOnValidation(loginUserData).then((result) => {



			req.session.username = result.Obj.userName;
			res.redirect('/landing');
			console.log(result.Obj.userName);




		}, (error) => {
			console.log(error);
			res.render('login', { msg: 'You have entered username or Password Wrong!Please Try Again' });
		})
	})

module.exports = router;
