const mongo = require('mongoose');
mongo.connect('mongodb://localhost/aribaNetwork');

var userSchema = mongo.Schema({
	userName: String,
	userPassword: String,
	userCompName: String,
	userID: String,
	userEmail: String,
	logedIn: String
});
var userData = mongo.model('aribaAccounts', userSchema);
module.exports = userData;