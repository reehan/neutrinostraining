const express = require('express');
const app=express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const multer = require('multer');
mongoose.connect('mongodb://localhost/register');
const ejs=require("ejs");
const storage = multer.diskStorage({
    destination: (req, file, callback)=>{
    callback(null, 'uploads/');
    },
    filename: (req, file, callback)=>{
    callback(null,file.originalname);
    }
    });

var mparser = multer({
    storage: storage
});

var userSchema = mongoose.Schema({
    user_name: String,
    password: String,
    email: String,
    img : String
    });
var urlParser = bodyParser.urlencoded({extended:false});

    var user = mongoose.model("people", userSchema);
    
    app.get('/',(req,res)=>{
        res.render("Register");
    });
    
    app.set("view engine","ejs");
    
    app.post("/userSchema",urlParser,mparser.single("fileupload"),(req,res)=>{
    var userInformation=req.body;

    var obj = new user({
        user_name: userInformation.Username,
        email:userInformation.email,
        password:userInformation.psw,
        img:req.file.path    

    });
    obj.save((err,dbres)=>{
        res.render("success")
    })
    });
    app.listen(3000);