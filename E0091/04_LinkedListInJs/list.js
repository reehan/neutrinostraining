function Node(data) {
    this.data = data;
    this.next = null;
  }
  
  function SinglyLinkedList() {
    this.head = null;
    this.tail = null;
    this.numberOfValues = 0;
  }
  
  SinglyLinkedList.prototype.add = function(data) {
    var node = new Node(data);
    if(!this.head) {
      this.head = node;
      this.tail = node;
    } else {
      this.tail.next = node;
      this.tail = node;
    }
    this.numberOfValues++;
  };
  
  
  SinglyLinkedList.prototype.length = function() {
    return this.numberOfValues;
  };
  SinglyLinkedList.prototype.print = function() {
    var string = '';
    var current = this.head;
    while(current) {
      string += current.data + ' ';
      current = current.next;
    }
    console.log(string.trim());
  };
  var linkedList=function(arg1,arg2){
    
  
  this.name=arg1;
  this.age=arg2;
  }; 
  linkedList.prototype.displayDetails= function(messege)
  {console.log('Name:' + this.name);
   console.log('Age:' + this.age);
    
  };
  var n1=new linkedList("Sur",21);
  var n2=new linkedList("rac",22);
  var n3=new linkedList("savi",23);
  var singlyLinkedList = new SinglyLinkedList();
   
  singlyLinkedList.add(n1);
  singlyLinkedList.add(n2);
  singlyLinkedList.add(n3);
  singlyLinkedList.add(5);
  singlyLinkedList.print();