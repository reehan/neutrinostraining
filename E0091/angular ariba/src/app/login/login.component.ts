import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  loginData: any={}
  
  constructor(private auth:AuthenticationService,private router: Router) { }

  ngOnInit() {
    
  }
  loginUser() {
    this.auth.loginUser(this.loginData)
    .subscribe (
      res => {
      localStorage.setItem('token', res.token)
      this.router.navigate(['/landin'])
      },
      err => console.log(err)
  )
}

 



  

}
