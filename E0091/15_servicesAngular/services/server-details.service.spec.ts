import { TestBed } from '@angular/core/testing';

import { ServerDetailsService } from './server-details.service';

describe('ServerDetailsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServerDetailsService = TestBed.get(ServerDetailsService);
    expect(service).toBeTruthy();
  });
});
