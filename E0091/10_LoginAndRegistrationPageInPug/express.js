var express = require('express');

var app = express();

app.set("view engine",'pug')

app.get('/', function(req, res){
	var arg={};
   res.sendFile(__dirname+'/views/login.pug');
   res.render('login.pug',arg);
});
app.get('/register', function(req, res){
	var arg={};
   res.sendFile(__dirname+'/views/register.pug');
   res.render('register.pug',arg);
});

app.listen(3007);
console.log('listening to port 3007')