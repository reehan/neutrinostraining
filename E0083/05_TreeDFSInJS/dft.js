function Node(data) {
this.data = data;
this.children = [];
}
function Tree(data) {
var node = new Node(data);
this.root = node;
}
Tree.prototype.visitDfs=function(callback) {
(function traverse(currNode) {
for (var i = 0, length = currNode.children.length; i < length; i++) {
traverse(currNode.children[i]);
 }
callback(currNode);
})
(this.root);
}
//console.log(root);
var tree = new Tree(1);
tree.root.children.push(new Node(2));
tree.root.children[0].parent=tree;
tree.root.children.push(new Node(3));
tree.root.children[1].parent=tree;
tree.root.children.push(new Node(4));
tree.root.children[2].parent=tree;
tree.root.children[0].children.push(new Node(5));
tree.root.children[0].children[0].parent = tree.root.children[0];
tree.root.children[0].children.push(new Node(6));
tree.root.children[0].children[1].parent = tree.root.children[0];
tree.root.children[2].children.push(new Node(7));
tree.root.children[2].children[0].parent = tree.root.children[2];
tree.visitDfs(function(node) {
console.log(node.data);
});

