const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/suitecrm1');

var userSchema=mongoose.Schema({
    salutation:String,
    firstname:String,
    lastname:String,
    officephone:String,
    mobileNo:String,
    title:String,
    department:String,
    accountname:String,
    fax:String,
    email:String,
    address:String,
    city:String,
    state:String,
    postalCode:String,
    country:String,
    photoPath: String
    
});
const PhotoUser=mongoose.model("contactuser",userSchema);

module.exports=PhotoUser;