var express = require('express');

var app = express();

app.set("view engine",'pug')

app.get('/', function(req, res){
   res.render('login.pug');
});
app.get('/image.jpg', function(req, res){
   res.sendFile(__dirname + '/views/image.jpg');
});

app.listen(3000);
