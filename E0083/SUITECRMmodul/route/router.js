const express = require('express');
const morgan = require('morgan')
const path = require('path');
const bodyParser = require('body-parser');


var userService=require('../services/service');

var urlencodedParser = bodyParser.urlencoded({ extended: false });

var router=express.Router();

router.route('/')
 .get(function(req,res){
 	res.render('suitefront');
 });
router.route('/index')
 .get(function(req,res){
 	res.render('index');
 });

router.route('/login')
 .get(function(req,res){
 	res.render('login');
 });
 router.route('/register')
  .get(function(req,res){
  	res.render('register');
  });
router.route('/profile1')
  .get(function(req,res){
  	res.render('profile1');
  });
router.route('/createAccounts')
  .get(function(req,res){
  	res.render('createAccounts');
  });
router.route('/createContacts')
  .get(function(req,res){
  	res.render('createContacts');
  });
router.route('/dashboardpost')
  .get(function(req,res){
  	res.render('dashboardpost');
  });
router.route('/dashboarduser')
  .get(function(req,res){
  	res.render('dashboarduser');
  });
router.route('/delete')
  .get(function(req,res){
  	res.render('delete');
  });

//back image
router.route('/back.jpg')
  .get(function(req,res){
  	res.sendFile(__dirname + '/back.jpg');
  });

/*router.get('/', (req, res) => {
    res.render('suitefront');
});
router.get('/index', (req, res) => {
    res.render('index');
});
router.get('/register',(req, res) => {
    res.render('register');
  });
router.get('/login',(req, res) => {
    res.render('login');
  });
router.get('/profile1',(req, res) => {
    res.render('profile1');
  });
router.get('/createAccounts',(req, res) => {
    res.render('createAccounts');
  });
router.get('/createContacts',(req, res) => {
    res.render('createContacts');
  });
router.get('/dashboardpost',(req, res) => {
    res.render('dashboardpost');
  });
  router.get('/dashboardpage',(req, res) => {
    res.render('dashboardpage');
  });
router.get('/dashboarduser',(req, res) => {
    res.render('dashboarduser');
  });
router.get('/delete',(req, res) => {
    res.render('delete');
  });
router.get('/back.jpg',(req,res)=>{
    res.sendFile(__dirname +'/'+ '/views/back.jpg');
});
*/
router.route('/register')
  .get(function(req,res){
    res.render('register');
});

  router.route('/register')
      .post(urlencodedParser,function(req,res){
   	var User={};
	
	console.log(User);
	userService.registerIfNewUser(User).then((result) => {
        User.ename1=req.body.username;
        User.myemail2=req.body.name;
        User.myname2=req.body.email;
        User.myconemail3=req.body.confirmationemail;
        User.myconemail3=req.body.password;
        User.mypass2=req.body.confirmationPassword;
        console.log(User);
		userService.registerIfNewUser(User).then((result) => {
		console.log('router register, userService result' , result);
		if(result.saved){
			res.redirect('/login');
			// redirect to login page
		}
		else{
			 res.render('register',{msg:'User Name Exists'});
			 // user already exist
		   }
	},
	(error)=>{
		console.log(error);
	});
});

  });

router.route('/login')
 .post(urlencodedParser,function(req,res){
 	var loginUserData={};
 	loginUserData.ename1=req.body.uName;
 	loginUserData.mypass2=req.body.uPassword;
 	userService.loginOnValidation(loginUserData).then((result)=>{
 		console.log('inLoginValidation',result);
 		if(result.exists)
 		{
 			if(result.Valid)
 			{
 				res.render('dashboardsticky');
 			}
 			else
 			{
 				res.render('login',{msg:'You have entered username or Password Wrong!Please Try Again'});
 			}
 		}
 		else
 		{
 			res.render('login',{msg:'You have entered username or Password Wrong!Please Try Again'});
 		}

 	})
 });
 

module.exports=router;
