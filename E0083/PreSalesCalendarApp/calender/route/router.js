const express = require('express');
const morgan = require('morgan')
const path = require('path');
const bodyParser = require('body-parser');


var userService=require('../services/service');
var userProfileService=require('../services/profileService');
var usercontactService=require('../services/contactService');
const checkdSignIn = require('../session');

var urlencodedParser = bodyParser.urlencoded({ extended: false });

var router=express.Router();

router.route('/')
 .get(function(req,res){
 	res.render('suitefront');
 });
router.route('/index')
 .get(function(req,res){
 	res.render('index');
 });

router.route('/login')
 .get(function(req,res){
 	res.render('login');
 });
 router.route('/register')
  .get(function(req,res){
  	res.render('register');
  });
router.route('/profile1')
  .get(checkdSignIn,function(req,res){
  	res.render('profile1');
  });
router.route('/createAccounts')
  .get(checkdSignIn,function(req,res){
  	res.render('createAccounts');
  });
router.route('/createContacts')
  .get(checkdSignIn,function(req,res){
  	res.render('createContacts');
  });
router.route('/dashboardpost')
  .get(function(req,res){
  	res.render('dashboardpost');
  });
router.route('/dashboarduser')
  .get(function(req,res){
  	res.render('dashboarduser');
  });
router.route('/dashboardsticky')
  .get(checkdSignIn,function(req,res){
  	res.render('dashboardsticky');
  });
router.route('/delete')
  .get(function(req,res){
  	res.render('delete');
  });

//back image
router.route('/back.jpg')
  .get(function(req,res){
  	res.sendFile(__dirname + '/back.jpg');
  });

router.route('/register')
  .get(function(req,res){
    res.render('register');
});

  router.route('/register')
      .post(urlencodedParser,function(req,res){
     
   	var User={};
	
	console.log(User);
	userService.registerIfNewUser(User).then((result) => {
        User.ename1=req.body.username;
        User.myemail2=req.body.name;
        User.myname2=req.body.email;
        User.myconemail3=req.body.confirmationemail;
        User.myconemail3=req.body.password;
        User.mypass2=req.body.confirmationPassword;
        console.log(User);
		userService.registerIfNewUser(User).then((result) => {
		console.log('router register, userService result' , result);
		if(result.saved){
			res.redirect('/login');
			// redirect to login page
		}
		else{
			 res.render('register',{msg:'User Name Exists'});
			 // user already exist
		   }
	},
	(error)=>{
		console.log(error);
	});
});

  });

router.route('/login')
 .post(urlencodedParser,function(req,res){
 	var loginUserData={};
 	loginUserData.ename1=req.body.uName;
 	loginUserData.mypass2=req.body.uPassword;
 	userService.loginOnValidation(loginUserData).then((result)=>{
 		console.log('inLoginValidation',result);
 		if(result.exists)
 		{
 			if(result.Valid)
 			{
                req.session.username = req.body.uName;
 				res.render('dashboardsticky');
 			}
 			else
 			{
 				res.render('login',{msg:'You have entered username or Password Wrong! Please Try Again'});
 			}
 		}
 		else
 		{
 			res.render('login',{msg:'You have entered username or Password Wrong! Please Try Again'});
 		}

 	})
 });
 
//profile
router.route('/profile')
      .post(checkdSignIn,urlencodedParser,function(req,res){
     var user1={};
//console.log(req.file);
    


user1.myname2= req.body.username;
    user1.myemail2= req.body.fname;
    user1.myconemail2= req.body.Active;
user1.mypass2= req.body.lname;

console.log(user1);
userProfileService.ProfileData(user1).then((result)=>{
  console.log('profiles',result);
  
    if(result)
    {
      res.render('dashboarduser',{reg1:res});
    }
    else
    {
      res.render('dashboarduser',{reg1:res});
    }
});

});
//contact 

  router.route('/createContacts')
      .post(checkdSignIn,urlencodedParser,function(req,res){
   	console.log(' in creating contact from body ',req.body);
		usercontactService.registerIfNewUserContact(req.body).then((result) => {
		console.log('router register, userService result' , result);
		if(result.saved){
			res.redirect('/createContacts');
			// redirect to login page
		}
		else{
			 res.render('createContacts',{msg:'User Name Exists'});
			 // user already exist
		   }
	},
	(error)=>{
		console.log(error);
	});
});

router.route('/logout')
	.get(function (req, res) {

		userService.sessiondatavalidation(req.session.username).then(response => {
			console.log(response)
		});

		req.session.destroy(function () {
			res.redirect('/login');
		});
	})



module.exports=router;
