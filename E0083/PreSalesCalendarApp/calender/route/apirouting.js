const express = require('express');
const morgan = require('morgan')
const path = require('path');
const bodyParser = require('body-parser');



var userService=require('../services/service');
const eventService=require('../services/eventService');
const checkdSignIn = require('../session');
var urlencodedParser = bodyParser.urlencoded({ extended: false });

var jsonParser = bodyParser.json();
var router=express.Router();

router.route('/')
 .get(function(req,res){
 	res.render('suitefront');
 });


router.route('/profile1')
  .get(checkdSignIn,function(req,res){
  	res.render('profile1');
  });
router.route('/createAccounts')
  .get(checkdSignIn,function(req,res){
  	res.render('createAccounts');
  });
router.route('/createContacts')
  .get(checkdSignIn,function(req,res){
  	res.render('createContacts');
  });
router.route('/dashboardpost')
  .get(function(req,res){
  	res.render('dashboardpost');
  });
router.route('/dashboarduser')
  .get(function(req,res){
  	res.render('dashboarduser');
  });
router.route('/dashboardsticky')
  .get(checkdSignIn,verifyToken,function(req,res){
  	res.render('dashboardsticky');
  });
router.route('/delete')
  .get(function(req,res){
  	res.render('delete');
  });

//back image
router.route('/back.jpg')
  .get(function(req,res){
  	res.sendFile(__dirname + '/back.jpg');
  });


  router.route('/register')
      .post(jsonParser,function(req,res){
     
   	var User={};
	
	console.log('register data',User);
	userService.registerIfNewUser(User).then((result) => {
        User.ename1=req.body.username;
        User.myemail2=req.body.name;
        User.myname2=req.body.email;
        User.myconemail3=req.body.confirmationemail;
        User.myconemail3=req.body.password;
        User.mypass2=req.body.confirmationPassword;
        console.log(User);
		userService.registerIfNewUser(User).then((result) => {
		console.log('router register, userService result' , result);
		if(result.saved){
			res.redirect('login');
			// redirect to login page
		}
		else{
			 res.render('register',{msg:'User Name Exists'});
			 // user already exist
		   }
	},
	(error)=>{
		console.log(error);
	});
});

	});
	

	//middleware for token authentication..
  function verifyToken(req,res,next){
		if(!req.headers.authorization){
			return res.status(401).send('unauthorized request');
		}
		let token=req.headers.authorization
		if(token=== 'null'){
			return res.status(401).send('unauthorized request');
		}
		let payload=jwt.verify(token,'secreatKey');
		if(!payload){
			return res.status(401).send('unauthorized request');
		}
		req.userId=payload.subject
		next();
	}



router.route('/login')
      .post(jsonParser,function(req,res){
				console.log('from login form',req.body);
 	var loginUserData={};
 	loginUserData.ename1=req.body.uName;
	 loginUserData.mypass2=req.body.uPassword;
	 console.log('in the routing where user wants to login',loginUserData);
 	userService.loginOnValidation(loginUserData).then((result)=>{

 		console.log('inLoginValidation',result);
 		if(result.exists)
 		{
 			if(result.Valid)
 			{
                req.session.username = req.body.uName;
 				res.send({value:true});
 			}
 			else
 			{
 				res.send({value:false});
 			}
 		}
 		else
 		{
 			res.send({value:false});
 		}

 	})
 });
 
//profile
router.route('/dashboard')
	  .post(jsonParser,function(req,res){
	  	  var eventData={};
	  	  eventData.eventName=req.body.eventName;
				eventData.eventStartDate=req.body.eventStartDate;
				eventData.eventEndDate=req.body.eventEndDate;
				eventData.eventStartTime=req.body.eventStartTime;
				eventData.eventEndTime=req.body.eventEndTime;
				
	  	 eventService.saveEventData(eventData).then((result)=>{
	  	 	console.log(result);
	  	 	if(result.saved==true){
	  	 	res.send(eventData);
	  	 }
	  	 else
	  	 {
	  	 res.send({msg:'Event already exists',redirect:false});	
	  	 }
		   })
		   
	  })

//insert
//var url='mongodb://localhost/calender';
//var mongoose=require('mongoose');
/*var assert=require('assert'); 
router.post('/insert', function(req, res, next) {
  var item = {
    eventName: req.body.eventName,
		eventStartDate: req.body.eventStartDate,
		eventEndDate: req.body.eventEndDate,
		eventStartTime: req.body.eventStartTime,
		eventEndTime: req.body.eventEndTime
    
  };

  mongoose.connect(url, function(err, db) {
    assert.equal(null, err);
    db.collection('user-data').insertOne(item, function(err, result) {
      assert.equal(null, err);
      console.log('Item inserted');
      db.close();
    });
  });

  res.redirect('/');
});
//getting data from mongo
*/
/*router.post('/form', function(req, res, next) {
	var resultArray = [];
	mongoose.connect(url, function(err, db) {
	  assert.equal(null, err);
	  var cursor = db.collection('eventdetails').find();
	  cursor.forEach(function(doc, err) {
		assert.equal(null, err);
		resultArray.push(doc);
		console.log('getting from database:',resultArray);
	  }, function() {
		db.close();
		res.render('form', {items: resultArray});
	  });
	});
  });*/


///getting data from database

router.route('/eventlist')
.get(function (req, res) {
	 eventService.findEvents().then((result)=>
	 {
						console.log('in findEvents db',result);
						res.send({msg:'db result',allEvents:result});
		})
});

//

router.route('/logout')
	.get(function (req, res) {

		userService.sessiondatavalidation(req.session.username).then(response => {
			console.log(response)
		});

		req.session.destroy(function () {
			res.redirect('/login');
		});
	})


	
module.exports=router;
