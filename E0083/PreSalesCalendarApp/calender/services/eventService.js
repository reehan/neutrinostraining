const eventDao=require('../Dao/eventDao');


var saveEventData=async function (eventData) {
           var returnValue = {};
	var dbResult = await eventDao.find(eventData.eventName);
	console.log('dbResult in saveEventData: saved or not', dbResult);
   console.log('in eventsrvc : user scheduled events',eventData);
	if (dbResult.found === false) { 
    await eventDao.save(eventData);
    returnValue.saved=true;
     returnValue.eventObject=eventData;
     } else
   {  	returnValue.saved=false;
     	returnValue.eventObj=dbResult.eventObj;
      }
   return returnValue;

    }


    var findEvents= async function() {
        var dbData=await eventDao.findAllEvents();
        console.log('in eventService',dbData.eventAllData);
        return dbData.eventAllData;
    }
    

    
	var eventService={};
    eventService.saveEventData=saveEventData;
    eventService.findEvents=findEvents;
	module.exports=eventService;


	