# Project Title

SuiteCRM:
SuiteCRM is a free and open source Customer Relationship Management application. Open source CRM is often used as an alternative to proprietary CRM software from major corporations such as Salesforce and Microsoft Dynamics CRM applications. SuiteCRM is a software fork of the popular customer relationship management (CRM) system from SugarCRM
## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

> express packages
 morgan packages
 path  packages
  cookieParser packages
 expressHandlebar packages
  //flash  packages
 session packages
 //mongoose packages
 passport packages
 bcrypt packages
 validator packages
 ejs  packages
 LocalStrategy packages
  multer  packages
### Installing

Install 'npm' and all required dependency packages mentioned above.

### Running the application

Step 1: Run the server using command: "node sui.js"
Step 2: Run the database using command: "mongo"
Step 3: Run the application in browser.

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* HTML, CSS, Bootstrap, Node.js, Express.js , MongoDB. 

## Contributing

Write here how one can contribute back to your project:
* You can provide a way to contact, like email
* You can point to bugs list and pick any to fix
* You can ask them to send patches in specific ways to help

## Versioning

This is the initial version being released

## Authors

* **Vinay M** - *UI design work and back end* - 
* **Rahul** - *UI design work and back end * - 
* **Roja**  - *UI design work and back end* - 
## License

This project is licensed under the GNU GPL License

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
