const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/calender');

var userSchema = mongoose.Schema({
	eventName:String,
    eventStartDate:String,
    eventEndDate:String,
    eventStartTime:String,
    eventEndTime:String
	
});
const eventData = mongoose.model("eventdetails",userSchema);

module.exports = eventData;