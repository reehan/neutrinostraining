const eventData = require('../Model/eventData');


var find = function (eventname) {
	return new Promise(resolve => {
		dbUserData = {};
		
		eventData.findOne({ eventName: eventname }, function (err, dbresp) {
			if (err) {
				throw err;
			}
			if (dbresp === null) {
			dbUserData.found = false;

				resolve(dbUserData);
			} else {
				dbUserData.found = true;
				dbUserData.eventObj = dbresp;
			}
			resolve(dbUserData);
		});
	});
}


var findAllEvents=function(){
	
	return new Promise(resolve=>{
		dbeventData={};
		console.log('in eventDao');
		eventData.find(function(err,dbresp) {
			if(err)
				{ 
					console.log(err);
				}
			dbeventData.eventAllData=dbresp;
			console.log('in dao',dbeventData.eventAllData);
               resolve(dbeventData);
		});
	})
}

var save = function (userDbObject) {

	var eventInfo = new eventData({ 
		eventName: userDbObject.eventName, 
        eventStartDate: userDbObject.eventStartDate,
        eventEndDate: userDbObject.eventEndDate,
        eventStartTime: userDbObject.eventStartTime,
        eventEndTime: userDbObject.eventEndTime
	});
	eventInfo.save(function (err, DBresponse) {
		console.log('events from  database',DBresponse);
		if (err) {
			throw err;
		}
	});

}
var eventDao={};
eventDao.save=save;
eventDao.find=find;
eventDao.findAllEvents=findAllEvents;
module.exports=eventDao;
