const http=require("http");
const fs=require("fs");

var server = http.createServer(function(req, res){

	console.log('request was made: ' + req.url);

	if(req.url==="/home" || req.url==="/")
	{
    	res.writeHead(200, {'Content-Type':'text/html'});
    	var  readStream = fs.createReadStream(__dirname + "/login.html", 'utf8');
    	readStream.pipe(res);
    }
    if(req.url === '/style.css'){
        res.writeHead(200, {'Content-Type':'text/css'});
        var  readStream = fs.createReadStream(__dirname + "/style.css");
        readStream.pipe(res);
    }

    if(req.url==="/image.jpg")
	{
		res.writeHead(200, {'Content-Type':'image/jpg'});
		var  readStream = fs.createReadStream(__dirname + "/image.jpg");
	    readStream.pipe(res);
	}
	
	if(req.method === 'POST' && req.url === '/login_success.html')
	{
    	var data = "";
    	var uData ="";
		var userMap = new Map();
    	req.on('data', function(chunk){data += chunk;console.log(data);})
        fs.writeFileSync('UserData.txt',userMap,'utf8');
 		req.on('end', function(){
			data=data.split("&");
			
			console.log(userMap);
			if(userMap.get(data[0])===data[1])
			{
				res.writeHead(200, {'Content-Type':'text/html'});
				var  readStream = fs.createReadStream(__dirname + "/login_success.html");
	    		readStream.pipe(res);
			}
			else
			{

				res.writeHead(200, {'Content-Type':'text/html'});
				var  readStream = fs.createReadStream(__dirname + "/login.html");
	    		readStream.pipe(res);
			}
		})
	 };
    if(req.method === 'POST' && req.url === '/register_success.html')
    {
		var data = "";
        
		var split="";
		req.on('data', function(chunk){data += chunk;console.log(data);})
		req.on('end', function(){
		                  split=data.split("&");
		                 console.log(split);
		              for(let i=0;i<split.length;i++)
		                 {
		                   	let field=split[i];
			                field=field.split("=");
			                split[i]=field[1];
		                   }
		                    split=split.join(",");
		                    split=split+"|";
		                    console.log(split);
		                    fs.appendFileSync('./UserData.txt',split);
		                      })
		res.writeHead(200, {'Content-Type':'text/html'});
		var  readStream = fs.createReadStream(__dirname + "/register_success.html");
	    readStream.pipe(res);
	};
});

server.listen(3000, '127.0.0.1');
console.log('now listening to port 3000');
   