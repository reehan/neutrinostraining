import { Component,OnInit  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// import custom validator to validate that password and confirm password fields match
import { MustMatch } from './must-match.validator';

import { Regdata } from './model/Regdata';

import{RegisterService} from './services/register.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'form';
  model:Regdata;



    registerForm: FormGroup;
    submitted = false;

    constructor(private formBuilder: FormBuilder, private registerService:RegisterService) { 
this.model=new Regdata();
    }

submitPersonData(){
    console.log(this.model);
    this.registerService.postPData(this.model);

  }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            firstName: [''],
            lastName: [''],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]],
            confirmPassword: ['', Validators.required]
        }, {
            validator: MustMatch('password', 'confirmPassword')
        });
    }


    // convenience getter for easy access to form fields
    get f() { 
        return this.registerForm.controls; 
        console.log(this.registerForm.controls);
    }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        confirm('you have successfully registered.');
    }
}
