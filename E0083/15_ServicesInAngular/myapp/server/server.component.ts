import { Component,Input } from '@angular/core';
import { ServerDetailsService} from '../services/server-details.service';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html'

})
export class ServerComponent {

private serverDetails: ServerDetailsService;

@Input() serName:string;


constructor(private _serverDetailsService: ServerDetailsService) {
	this.serverDetails = _serverDetailsService;
}

getServerName() {
this._serverDetailsService.serverName=this.serName;
	return this.serverDetails.getName();
}

ngOnInit() {
}

}