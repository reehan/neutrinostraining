const mongoose=require('mongoose');
const express=require('express');
const ejs = require('ejs');
const passport = require('passport');
const PORT = 3000;


var bodyParser = require('body-parser');
const validator = require('express-validator');
var session = require('express-session');
const multer = require('multer');


const app=express();
app.set("view engine","ejs");
/*
let logger=(req,res,next)=>{
  console.log('Request received from URL:' + req.url);
    next();
}; */

var vikram=["amarasimha","kalidasa","varahamihira","dhanvantari","kshapanaka","vararuchi","ghatakarapara","shanku","vetala-bhatta"];
var akbar=["Abu'l-Fazl","Faizi","Raja Todar Mal","Fakir Aziao-Din","Abdul Rahim Khan-I-Khana","Birbal","Tansen"
,"Raja Man Singh I","Mulla Do-Piyaza"];
var raja=["Gopal Bhar","Bharatchandra Ray","Ramprasad Sen"];


var kings=new Map();
kings.set("vikram","vikram");
kings.set("akbar","akbar");
kings.set("raja","raja");

var urlencodedParser = bodyParser.urlencoded({ extended: false });
app.use(validator());

app.use(passport.initialize());
app.use(passport.session());


//registerpart

const storage = multer.diskStorage({
destination: (req, file, callback)=>{
callback(null, 'uploads/');
},
filename: function (req, file, callback) {
    callback(null, file.originalname);
  }
});


var parser = multer({storage:storage});

mongoose.connect('mongodb://localhost/users2');

var userSchema=mongoose.Schema({
    username1:String,
    email1:String,
    password1:String,
    conformpassword1:String,
    photoPath: String
    
});
const PhotoUser=mongoose.model("person1",userSchema);

var UserScheam=mongoose.Schema({
 name:String,
 email:String,
 password:String
});

var User=mongoose.model("person2",UserScheam);

app.get("/",(req,res)=>{
res.render("login.ejs");

});

app.use(session({
  secret: 'neutrionos',
  resave: false,
  saveUninitialized: true,
  //cookie: { myvar: 'dfgh' }  // needed certificate
}));



app.use(validator());
app.use(passport.initialize());
app.use(passport.session());


app.post('/register',urlencodedParser,parser.single('userImage'),(req,res)=>{
    
     req.checkBody('username','User name is invalid').notEmpty();
    req.checkBody('email','password is invalid').isEmail();
        
        req.checkBody('password','password is invalid').isLength({min:5,max:10});
        req.checkBody('conformpassword','password is not matched').isLength({min:5,max:10});
        req.checkBody('userImage','password is invalid');
        console.log(req.file);
    
   

    let errors=req.validationErrors();

var newuser = new PhotoUser({
    username1: req.body.username,
    email1: req.body.email,
    password1: req.body.password,
    conformpassword1: req.body.conformpassword,
    photoPath: req.file.path
    });
    console.log('Printing user');
    console.log(newuser);
    newuser.save(function(err, user){
        if(err)
      res.send('error occured while saving to db');
        else
         res.send('register data got created');
          });
});

app.post("/login",(request,response)=>{
var data=" ";
		request.on('data',function(chunk){
			data+=chunk;
		});
		request.on('end',function()
		{
			data=data.split("&");
			for(let i=0;i<data.length;i++)
			{
				let field=data[i].split("=");
				data[i]=field[1]
			}
			
			if(data[data.length-1]==="")
			{
				data.pop();
			}
			console.log(data);


if(kings.get("vikram")==data[1]){
response.render("navaratna.ejs",{name:vikram});
}
else if(kings.get("akbar")==data[1]){
response.render("navaratna.ejs",{name:akbar});
}
else if(kings.get("raja")==data[1]){
response.render("navaratna.ejs",{name:raja});
}
else{
response.send("please provide a valid username or password.")
}
});
    
});


app.get('/dp/:username/:userpassword/:useremail',function(req,res){
    var newUser=new User({
        username:req.params.username,
        email:req.params.useremail,
        password:req.params.userpassword    
    });
    
    //console.log(newUser);
    newUser.save((err)=>{
       if(err){
           res.send('error in creating user');
       }else{
             res.send('user got created'); 
       }
    });
});




User.findOne({name: "vikram"}, (err, response)=>{
//console.log('Name of the user:' + response);
});


passport.serializeUser(function(user, done) {
  		done(null, user);
  	});
  	passport.deserializeUser(function(user, done) {
	    done(null, user);
	});


app.listen(PORT, () => {
    console.log('Listening to port:' + PORT);
});
