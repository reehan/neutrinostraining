import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-component2',
  templateUrl: './component2.component.html',
  styleUrls: ['./component2.component.css']
})
export class Component2Component implements OnInit {
  ngOnInit() {}

  pdng='20px';
  colour='black';
  divColor='2px solid red'
  changeColor(){
    if(this.colour === 'black')
    {
      this.colour='red';
    }
    else
    this.colour = 'black';
  }  
  textArray= [];
  inputText='';
  display(){
    this.textArray.push(this.inputText);
  }
}
