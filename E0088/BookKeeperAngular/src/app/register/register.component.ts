import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MustMatch } from '../must-match.validator';
import { RegisterService } from '../services/register.service';
import { Router } from '@angular/router';

import { Regdata } from '../model/Regdata';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

   model:Regdata;
    registerForm: FormGroup;
    submitted = false;
loading=false;
display = false;
 
    constructor(private formBuilder: FormBuilder, private registerService:RegisterService,private router: Router) { 
this.model=new Regdata();
    }

submitPersonData(){
    console.log('model value..',this.model);
    console.log('register form value',this.registerForm.value);
    this.registerService.postPData(this.registerForm.value,(data)=>{
        console.log('recived response', data);
        if(data.status === true){
        this.router.navigate(['/login']);
        }
        else
        {
        this.display = true;
        this.router.navigate(['/register']);
        }
        });
        );

  }

  ngOnInit() {
  this.registerForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            tele: ['', Validators.required],
          
            gst: ['', Validators.required],
             country: [''],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]],
         
            confirmPassword: ['', Validators.required]
        }, {
            validator: MustMatch('password', 'confirmPassword')
        });
  
  }
    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; 
      console.log(this.registerForm.controls);
    }

    onSubmit() {
        this.submitted = true;
        console.log('form builder value:', this.registerForm.value )
        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return ;
        }
    this.loading = true;
       // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value))
       alert('successfully registered please login')
    }
   

}
