import { Injectable } from '@angular/core';
import { HttpClientModule} from "@angular/common/http";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class RegisterService {
//redirect to login
  constructor(private http:HttpClient,private router: Router) { }


  postPData(Regdata:any, callback) {
  console.log('in posting data',Regdata);
   return this.http.post('http://localhost:3001/api/RegSuccess ', Regdata).subscribe(replyFromServer =>{
    	console.log('response:',replyFromServer );
      callback(replyFromServer);
    });

    
  }
}
