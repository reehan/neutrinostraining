import { NgModule } from '@angular/core';
import { Routes, RouterModule,Router } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import {InvoiceComponent} from './invoice/invoice.component';
const routes: Routes = [
  {path:'', component:IndexComponent },
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent}
  {path:'invoice', component:InvoiceComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
