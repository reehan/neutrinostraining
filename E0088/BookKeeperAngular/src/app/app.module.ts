import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import{RouterModule,Routes} from '@angular/router';
import { CarouselComponent } from './carousel/carousel.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '../../node_modules/@angular/http';
import { RegisterService } from './services/register.service';
import { HttpClientModule } from '@angular/common/http';
import { NgxPasswordToggleModule } from 'ngx-password-toggle';
import { LoginService } from './services/login.service';
import { InvoiceComponent } from './invoice/invoice.component';
import {MatNativeDateModule,MatTabsModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';




@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    LoginComponent,
    RegisterComponent,
    CarouselComponent,
    InvoiceComponent,
 


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
   
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgxPasswordToggleModule,
     BrowserAnimationsModule,
    MatNativeDateModule,
    MatTabsModule

  ],
  providers: [RegisterService,LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }
