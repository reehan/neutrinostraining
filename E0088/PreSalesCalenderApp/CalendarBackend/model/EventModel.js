const mongoose = require('mongoose');
const eventSchema = mongoose.Schema({
	title: String,
	eventDateTime: String,
	file: String
});

var Event=mongoose.model('events', eventSchema);

module.exports.eventObject = function(){
    return Event;
}