const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const cors = require('cors');
const passport = require('passport');
const session = require('express-session');
const calenderRouter = require('./routing/CalenderRouter');
const app = express();
const server = http.createServer(app);
const jsonParser = bodyParser.json();

app.use(cors());
app.use('/login', jsonParser);
app.use('/saveEvent',jsonParser);
app.use('/', calenderRouter);
server.listen('3000');
console.log('Listening to the port 3000');
