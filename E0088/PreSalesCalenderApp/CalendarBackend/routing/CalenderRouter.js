const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const eventService = require('../service/EventService');
const empService = require('../service/EmployeeService');

mongoose.connect('mongodb://localhost/CrudDB');


router.get('/',(req,res)=>{
	console.log('serving the events data');
	Response = {};
	Response.data = null;
	Response.error = null;
	eventService.getEventsData().then((eventsData)=>{
		Response.data = eventsData;
		
		res.send(Response);
	}).catch((error)=>{
		Response.error = error.toString();
		res.send(Response);
	})
})
.post('/saveEvent', (req, res)=>{
	Response = {};
	Response.data = null;
	Response.error = null;
	console.log("in save event",req.body);
	eventService.saveEventData(req.body).then((result)=>{
		console.log(result);
		if(result)
		{
			eventService.getEventsData().then((eventsData)=>{
				Response.data = eventsData;

				res.send(Response);
			}).catch((error)=>{
				Response.error = error.toString();
				res.send(Response);
			})
		}
	}).catch((err)=>{
		Response.error = err.toString();
		res.send(Response);
	});
})
.post('/login', (req, res)=>{
	console.log('Serving the specific employee calender');
	Response = {};
	Response.data = null;
	Response.error  = null;
	Response.isLogged = false;
	empService.getLoginStatus(req.body).then((result)=>{
		console.log('in router ',result);
		if(result)
		{
			console.log("in login1!!!!!!!!!!!!")
			Response.isLogged = true;
			eventService.getEmployeeEvents(req.body.email).then((respData)=>{
				Response.data = respData;
			}).catch((err)=>{
				Response.error = err.toString();
				res.send(Response);
			});
		}else{
			console.log("in login.............");
			res.send(Response);
		}
	}).catch((err)=>{
		Response.error = err.toString();
		res.send(Response);
	});
});
module.exports = router;