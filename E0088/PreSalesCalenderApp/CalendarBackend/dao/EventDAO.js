const mogoose = require('mongoose');
const eventmodel = require('../model/EventModel');

module.exports.saveEventData = function(body){
	var myPromise = new Promise(function(resolve, reject){
		
		var eventObject = eventmodel.eventObject();
		var event = new eventObject({
			title: body.title,
			eventDateTime:body.eventDateTime,
			file: body.file
		})
		
		event.save((err)=>{
			if(err)
				reject('Error while saving the event data to the database');
			else
				resolve(true);
		});
	});
	return myPromise;
}

module.exports.getEventsData = function(){
	var myPromise = new Promise(function(resolve, reject){
		var eventObject = eventmodel.eventObject();
		eventObject.find((err, response)=>{
			if(err)
				reject('Error while retrieving the events data drom database');
			else
				resolve(response);
		});
	});
	return myPromise;
}

module.exports.getEmployeeEvents = function(email){
	var myPromise = new Promise(function(resolve, reject){
		var eventObject = eventmodel.eventObject();
		eventObject.find({email:email}, (err, response)=>{
			if(err)
				reject('Error while retrieving the employee events data drom database');
			else
				resolve(response);
			console.log("in employ event event dao",response)
		});
	});
	return myPromise;
}