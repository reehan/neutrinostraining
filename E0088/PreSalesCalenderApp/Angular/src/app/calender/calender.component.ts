import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms'
import { Router } from '@angular/router';
import { CalenderService } from '../calender.service';

@Component({
  selector: 'app-calender',
  templateUrl: './calender.component.html',
  styleUrls: ['./calender.component.css']
})
export class CalenderComponent implements OnInit {

  eventForm:FormGroup;
  eventsData =''; //JSON.parse(sessionStorage.getItem('eventsData'));
  show = false;
  constructor(private calenderService:CalenderService, private router:Router) { }

  ngOnInit() {
  console.log('session data:');
  	this.eventForm = new FormGroup({
  		title: new FormControl(),
  		eventDateTime:new FormControl(),
  		file:new FormControl()
  	});
  }

  addEvent(){
  	console.log(this.eventForm.value);
    this.calenderService.postEvent(this.eventForm.value,(data)=>{
      if(data.error)
      {
        console.log('Error:', data.error);
        this.router.navigate(['error']);
      }else{
        this.eventsData = data.data;
        sessionStorage.setItem('eventsData', JSON.stringify(data.data));
        this.router.navigate(['']);
      }
    })
  }

  getLogin(){
    
    this.router.navigate(['login']);
  }

  displayAddEvent(){
    if(this.show == false)
    this.show = true;
    else
    this.show = false;
  }
}
