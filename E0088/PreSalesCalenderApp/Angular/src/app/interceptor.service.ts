import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor{

  constructor(private injector:Injector) { }
  intercept(req, next){
console.log('session storage:', sessionStorage.getItem('email'));
  	var clonedReq = req.clone({
  	headers:req.headers.set('email', 'calender '+sessionStorage.getItem('email'))
  	})
  	return next.handle(clonedReq);
  }

}
