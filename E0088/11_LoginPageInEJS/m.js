var mongoose = require('mongoose');
const express = require('express');
const ejs = require('ejs');
const bodyParser = require('body-parser');
const session = require('express-session');
const app = express();


//multer
const multer=require('multer');

//image folder
const storage = multer.diskStorage({
destination: (req, file, callback)=>{
callback(null, 'fold/');
},
filename: (req, file, callback)=>{
//	callback(null, 'bhagyadalakshmi');
callback(null, file.originalname);
//console.log(new Date().getTime());
}
});

app.set('view engine', 'ejs');
const PORT = 3000;

//for url encoder
//var urlencodedParser = bodyParser.urlencoded({ extended: false });
var mparser=multer({storage: storage});



mongoose.connect('mongodb://localhost/users');

var userSchema=mongoose.Schema({
	name:String,
	pass: String,
    email: String,
	img:String
});

var User = mongoose.model("users", userSchema);


app.get('/',function(req,res){
	res.render("Register");
});



app.post('/user', mparser.single("ipage"),function(req,res){
	var info=req.body;
	console.log(req.file);
	var nuser=new User({
	name:info.firstname,
	pass:info.pass,
    email:info.email,
	img:req.file.path
	});

nuser.save(function(err, dbResp){
if(err)
res.render('error');
else
res.render('success');
});
});

	
	
app.listen(PORT, () => {
    console.log('Listening to port:' + PORT);
});

