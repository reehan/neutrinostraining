const express= require('express');
const app=express();
const session = require('express-session');

const PORT=3000;

var bodyParser=require('body-parser');
var urlenc=bodyParser.urlencoded({extended:false});


var mon=require('mongoose');
mon.connect('mongodb://localhost/expressDb');

var user=mon.Schema({
  username:String,
  tele:String,
  password:String,
  Fname:String,
  Lname:String,
  Gst:String,
  country:String
});

var userdb=mon.model('databaseRecord',user);

app.set('view engine','ejs');

//static for images
app.use(express.static('images'));
//static for javascript
app.use(express.static('js'));
//session
app.use(session({
  cookie: { secure:false },
  secret: 'neutrinos',
  saveUninitialized: true,
  resave: false
}));


var checkSignedIn = function(req,res,next){
        console.log("checkSignedIn checking for username:",req.session.username);

    if(req.session.username){
        next();

    }else{
        res.redirect('/login.ejs');
    }
};







app.get('/sucess',checkSignedIn,(req,res)=>
{
	res.render('invoice.ejs');
});








app.get('/',(req,res)=>{
  res.render('index');
});
app.get('/register.ejs',(req,res)=>{
  res.render('register');
});
app.get('/register',(req,res)=>{
  res.redirect('register.ejs');
});
// app.post('/register',(req,res)=>{
//   res.render('register');
// });

app.get('/login.ejs',(req,res)=>{
  res.render('login');
});
app.get('/login',(req,res)=>{
  res.redirect('login.ejs');
})
app.post("/RegSuccess",urlenc,(req,res)=>{
  var userInfo=new userdb({
    username:req.body.UserName,
    tele:req.body.tele,
    password:req.body.pwd,
    Fname:req.body.fname,
    Lname:req.body.lname,
    Gst:req.body.gst,
    country:req.body.country
  });
  userInfo.save((err,resp)=>{
    if(err){
      res.redirect('RegSuccess');
}
        res.render('login');

  });
});
app.get('/pro',checkSignedIn,(req,res)=>{
		res.render('invoice.ejs');

});
/*app.get('/invoice',checkSignedIn,(req,res)=>{
		res.render('invoice.ejs');

});*/


//logout
	app.get('/logout',(req,res)=>{
    req.session.destroy(function(){
    res.redirect('/');
});
    });

app.post('/sucess', urlenc,(req,res)=>{
//userdb.find({username:req.body.UserName},(err,ress)=>{

//})
var reqBodyUsername = req.body.UserName;
console.log("Request body username", reqBodyUsername);
userdb.findOne({username: reqBodyUsername},(err,resdb)=>{
	console.log("DB response:",resdb);
	console.log("DB error:", err);
	if(err || !resdb){
	res.send('user not registered!!!Please register')
	}
	else{
  if(resdb.password===req.body.pwd){
//session
 req.session.username = reqBodyUsername;
   //res.redirect('/');
   res.redirect('success');
  }
  else{
    res.send('sorry error!!!');
  }
	}
});
});



app.get('/success',checkSignedIn,(req,res)=>{
	let sessionUserName = req.session.username;
	console.log("Request session in success:", sessionUserName);
	userdb.findOne({username:sessionUserName},(err,resdb)=>{
	if(err || !resdb){
		res.send('user not registered')
	}
	else{
		// req.session.username = resdb.username;
		console.log("DB response:",resdb);
		console.log("User name in session:", sessionUserName);
		var data={country:resdb.country,username:sessionUserName,tele:resdb.tele,password:resdb.pwd,Fname:resdb.Fname,Lname:resdb.Lname,gst:resdb.Gst}
		// req.session.username = data.username;
    console.log("Country data:", data.country);
    console.log("fname in success:",);
		res.render("invoice",{data:data});
	}
})
});
app.post('/update',urlenc,(req,res)=>{
  console.log('inside update')
  let sessionUserName = req.session.username;
  userdb.findOne({username:sessionUserName},(err,resdb)=>{
    if(err){
      console.log('err');
      res.send('oops error');
    }
    else{
     userdb.update({tele:resdb.tele,Fname:resdb.Fname,Lname:resdb.Lname,Gst:resdb.Gst,country:resdb.country},{tele:req.body.stele,Fname:req.body.sname,Lname:req.body.slname,Gst:req.body.sgst,country:req.body.scountry},(err,user)=>{

      if(err){
        console.log('error while updating');
        res.send('Sorry,Error while updating try again!!!!!');
      }
      else{
        console.log('updated');
        res.send('profile updated')
      }
     })
    }
  });
});


app.listen(PORT);
console.log('listening to port:'+PORT);
