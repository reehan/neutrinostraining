import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-component1',
  templateUrl: './component1.component.html',
  styleUrls: ['./component1.component.css']
})
export class Component1Component implements OnInit {
  ngOnInit() {
  }

  pdng='20px';
  mgn='5px';
  colour='black';
  divColor='2px solid orange'
  inputText='';
  status='false';
  textArrray=[];
  changeColor(){
    if(this.colour === 'black')
    {
      this.colour='orange';
    }
    else
    this.colour = 'black';
  }
  textArray= [];
  display(){
    this.textArray.push(this.inputText);
  }
}
