import { Component } from '@angular/core';
//import { Component1Component } from './component1/Component1.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  // It will maintain list of sub-components
  title = 'AngularAssignment';
}
