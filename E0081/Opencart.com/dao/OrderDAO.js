const mogoose = require('mongoose');
const orderModel = require('../models/OrderModel.js');

module.exports.saveOrderData = function(userName, totalCost){
    var myPromise = new Promise(function(resolve, reject){
        var date = new Date();
        var orderNumber=Math.round(Math.random()*10000);
        var LatestOrder = orderModel.orderObject();
        var latestOrder = new LatestOrder({
                orderId : orderNumber,
                customer :userName,
                status : 'pending',
                date : date.getDate()+'/'+date.getMonth()+'/'+date.getFullYear(),
                cost : totalCost
        });
        latestOrder.save((err)=>{
            if(err)
                reject('Something went wrong while storing latest order data into the database');
            else
                resolve(true);
        });
    });
    return myPromise;
}

module.exports.getOrderData = function(){
    var myPromise = new Promise(function(resolve, reject){
        var LatestOrder = orderModel.orderObject();
        LatestOrder.find((err, response)=>{
            if(err){reject("Error while getting the products data from the database");}
            resolve(response);
        });
    });
    return myPromise;
}