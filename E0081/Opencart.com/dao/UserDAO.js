const mogoose = require('mongoose');
const userModel = require('../models/UserModel.js');

module.exports.getUserData = function(body){
    var myPromise = new Promise(function(resolve, reject){
        var User = userModel.userObject();
        User.findOne({email : body.email.trim()},(err, response)=>{
            if(err){reject("Error while getting the user data into the database");}
            resolve(response);
        });
    });
    return myPromise;
}

module.exports.saveUserData = function(body){
    var myPromise = new Promise(function(resolve, reject){
        var User = userModel.userObject();
        var user = new User({
                    userName: body.username,
                    name : body.firstname+' '+body.lastname,
                    email : body.email,
                    countryCode : body.country_id,
                    password : body.password
                });
        user.save((err)=>{   
            if(err){
                reject("Error while saving the user data into the database");
            }
            else
                {
                    resolve(true);
                }
        });
    });
    return myPromise;
}

