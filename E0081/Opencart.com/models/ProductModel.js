const mongoose = require('mongoose');

var productSchema = mongoose.Schema({
        productName: String,
        description: String,
        model: String,
        price : String,
        dimensions: String,
        weight: String,
        quantity: Number,
        status : String,
        image: String
        });
    
var Product = mongoose.model("products", productSchema);

module.exports.productObject = function(){
    return Product;
}