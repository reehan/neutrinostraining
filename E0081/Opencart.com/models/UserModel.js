const mongoose = require('mongoose');
    var userSchema=mongoose.Schema({
	userName: String,
	name: String,
	email: String,
	countryCode: String,
	password: String
    });
    
var User=mongoose.model('users', userSchema);

module.exports.userObject = function(){
    return User;
}