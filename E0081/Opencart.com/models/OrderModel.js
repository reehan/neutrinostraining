const mongoose = require('mongoose');

var latestOrderSchema =mongoose.Schema({
        orderId : Number,
        customer : String,
        status : String,
        date : String,
        cost : Number,
    });

var LatestOrder = mongoose.model('orders', latestOrderSchema);

module.exports.orderObject = function(){
    return LatestOrder;
}