const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const opencartRouter = require('./Routing/OpencartRouter.js');
const app = express();
const server = http.createServer(app);
const passport = require('passport');
const session = require('express-session');

app.use(express.static('Images'));
app.use(express.static('css'));
app.use(express.static('Java Script'));
app.use(express.static('fonts'));
app.use('/Uploads', express.static('Uploads'));

app.set('view engine', 'ejs');

app.use('/Login', bodyParser.urlencoded({ extended: true}));
app.use('/Dashboard', bodyParser.urlencoded({ extended: true }));
app.use('/Cart', bodyParser.urlencoded({ extended: true }));
app.use('/StoreFront', bodyParser.urlencoded({ extended: true}));

app.use(session({
secret: 'neutrinos',
resave: false,
saveUninitialized: false,
}));

app.use(passport.initialize());
app.use(passport.session());
passport.serializeUser(function(user_id, done) {
done(null, user_id);
});

passport.deserializeUser(function(user_id, done) {
done(null, user_id);
});
app.use('/',opencartRouter);
server.listen(9999);
console.log('Main JS Running');