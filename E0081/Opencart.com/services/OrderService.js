const orderDAO = require('../dao/OrderDAO.js');

var getTotalCost = function(productData, quantity){
    var promise = new Promise((resolve, reject)=>{
    var totalCost = 0;
    for(let i=0; i<productData.length; i++)
    {
        totalCost += (productData[i].price*quantity[i]);
    }
    resolve(totalCost);
    });
    return promise;
}
module.exports.saveOrderData = async function(productData, quantity, userName){
    var totalCost = await getTotalCost(productData, quantity);
    var isSaved =await orderDAO.saveOrderData(userName, totalCost);
    return isSaved;
}

module.exports.getOrdersData =async function(){
    var orders = await orderDAO.getOrderData();
    return orders;
}