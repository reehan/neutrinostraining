var express = require('express');

var app = express();

app.set('view engine', 'pug')

app.get('/login', function(req, res){

//   res.sendFile(__dirname+'/views/Login.pug');
   res.render(__dirname + '/views/Login.pug');
});

app.get('/gitlogo.jpg', function(req, res){
   res.sendFile(__dirname + '/views/gitlogo.jpg');
});
app.listen(3000);
console.log('Server started listening to port 3000')