import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ComponentServicesService {

  constructor() { }
  abc = 'No name';
  
  store(data){
    this.abc=data;
  }
  
  retrieve(){
    return this.abc;
  }
}
