import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { Component1Component } from './component1/component1.component';
import { Component2Component } from './component2/component2.component';
import { FormsModule } from '@angular/forms';
import { ComponentServicesService } from './services/component-services.service';
import { Services } from '@angular/core/src/view';

@NgModule({
  declarations: [
    AppComponent,
    Component1Component,
    Component2Component
  ],
  imports: [
    BrowserModule,
    FormsModule,

  ],
  providers: [ComponentServicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
