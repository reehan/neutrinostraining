import { Component, OnInit, Input } from '@angular/core';
import { ComponentServicesService } from '../services/component-services.service';

@Component({
  selector: 'app-component2',
  templateUrl: './component2.component.html',
  styleUrls: ['./component2.component.css']
})
export class Component2Component{

@Input() inputText:string;
status= Math.random()>0.5?'online':'offline';

private serv: ComponentServicesService;  
constructor(private _services :ComponentServicesService ) {
  this.serv=_services;  
}
func(){
this.serv.store(this.inputText);//without function call errors happens thats why a function call is written 
}
printName(){
  this.func();
  return this.serv.retrieve();
  }
}