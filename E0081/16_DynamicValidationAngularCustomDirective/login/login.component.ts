import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

SignInForm:FormGroup;
upperCaseStatus= true;
lowerCaseStatus = true;
numberStatus = true;
lengthStatus = true;
password: string;
  constructor() { }

ngOnInit() {
    this.SignInForm = new FormGroup({
      userName: new FormControl(),
      password: new FormControl()
    })
  }
  
  suggestion(){
    this.password = '1W';
    //this.SignInForm.get(password);
    if(this.password.length>8)
      {
        this.numberStatus = false;
      }
      for(let i=0;i<this.password.length;i++)
      {
          var char = this.password.charAt(i);
          var number=parseInt(char);
          if(!(isNaN(number)))
          {
            this.numberStatus = false;
          }
          if(char >= 'A' && char <= 'Z')
          {
            this.upperCaseStatus = false;
          }
          if(char >= 'a' && char <= 'z')
          {
            this.lowerCaseStatus = false;
          }
      }
      console.log('Password suggestions');
  }
}
