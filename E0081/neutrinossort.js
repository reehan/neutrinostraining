var keyObj = new Object();

var notnumObj = keyObj/0;
var notnumFun = keyObj/0;
var m=new Map();
m.set("a",3);
m.set("c",4);
m.set("b",1);
m.set("d",2);
var innerArray = [notnumObj, true, 'abc', 345, 789, notnumFun, 'true', false ];
var anotherInnerArray = [keyObj, 899.08, 12.89];

var sortArray = [innerArray, 'zoo', 'doc', anotherInnerArray];

function sort(arrayToSort)
{
  var sortedArray = arrayToSort;
  var booleanArray = [];
  var subArrayList = [];
  for(let iCounter in sortedArray)
  {
    let theElement = sortedArray[iCounter];
    if(Array.isArray(theElement))
    {
      sortedArray.splice(iCounter, 1);
      let sortedSubArray = sort(theElement);
      subArrayList.push(sortedSubArray);
    }
      else if(typeof theElement == 'boolean')
    {
      booleanArray.push(theElement);
      sortedArray.splice(iCounter, 1);
    }
    else if(inArray  instanceof Map)
    {
        var c=[];
        for(let value in map)
        {
            console.log(value);
            c.push(value);
        }
        c.sort(function(a,b) 
        {
           var x = a.value.toLowerCase();
           var y = b.value.toLowerCase();
           return x < y ? -1 : x > y ? 1 : 0;
        });         
    }  
  }
  // Sorting the base array
  sortedArray = sortedArray.sort();
  // This is to put the booleans sorted together
  booleanArray.sort();
  for(let key in booleanArray)
  {
    let theElement = booleanArray[key];
    sortedArray.push(theElement);
  }
  // Adding any sub-arrays that were sorted as part of recursive call
  for(let key in subArrayList){
    let theElement = subArrayList[key];
    sortedArray.push(theElement);
  }
  return sortedArray;
}

console.log(sort(sortArray));
