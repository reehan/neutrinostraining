var fs = require('fs');

var readMe1 = fs.readFileSync('./Department.txt', 'utf8');
var dept=readMe1.split("|");

var readMe2 = fs.readFileSync('./Employee.txt', 'utf8');
var emp=readMe2.split("|");

//2n iteration
var deptMap=new Map();
for(let j=2;j<dept.length-1;j++)
{
	let eachDept=dept[j].split(",");
	deptMap.set(eachDept[0],eachDept[1]);
}
for(let i=2;i<emp.length-1;i++)
{
	let eachEmp=emp[i].split(",");
	let deptno = eachEmp.pop();
	eachEmp.push(deptMap.get(deptno));
	emp[i]=eachEmp.join(",");
}
var final=emp.join("|");

fs.writeFileSync('Output.txt', final);

//Create new directory
fs.mkdir('Somefile', function(){console.log("Done creating Directory");});

//Add files to the new directory
fs.readFile('EmpDb/Department.txt', 'utf8', function(err, data){fs.writeFile('./somefile/Department.txt', data, function(){console.log("Done writing Department details in new directory");});});

fs.readFile('./Empdb/Employee.txt', 'utf8', function(err, data){fs.writeFile('./somefile/Employee.txt', data, function(){console.log("Done writing Employee details in new directory");});});

//remove old directory
/*fs.unlink('./Empdb/Employee.txt', function(){fs.unlink('./Empdb/Department.txt', function(){fs.rmdir('', function(){console.log("Done4");});});});*/