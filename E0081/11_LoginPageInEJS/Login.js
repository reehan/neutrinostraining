const express = require('express');
const ejs = require('ejs');
const PORT = 3000;
const bodyParser = require('body-parser')
const passport = require('passport');
const session = require('express-session');
const expressvalidator = require('express-validator');
const mongoose = require('mongoose');
const multer = require('multer');

//userName = Vikramaditya, password = vikramaditya
//userName = Akbar, password =  akbar
//userName = Raja, password = rajakrishnachandra

var navaratnas = new Map();
navaratnas.set('Vikramaditya', ['Amarasimha', 'Dhanvantari', 'Ghatakarapara', 'Kalidasa', 'Kshapanaka', 'Shanku', 'Varahamihira', 'Vararuchi', 'Vetala-Bhatta']);
navaratnas.set('Akbar', ['Abul-Fazl', 'Raja Todar Mal', 'Abdul Rahim Khan-I-Khana', 'Birbal', 'Mulla Do-Piyaza', 'Faizi', 'Fakir Aziao-Din', 'Tansen', 'Raja Man Singh']);
navaratnas.set('Raja', ['Gopal Bhar', 'Bharatchandra Ray', 'Ramprasad Sen']);

mongoose.connect('mongodb://localhost/users');
var userSchema = mongoose.Schema({
	userName : String,
	pwd : String,
	filePath: String
});

var storage = multer.diskStorage({
	destination:(req, file, callback)=>{
		callback(null, 'uploads/');
		},
	filename:(req, file, callback)=>{
		var fileName = file.originalname;
		callback(null, fileName);
	}
});

var User = mongoose.model("person",userSchema);

const app = express();
app.set('view engine', 'ejs');

app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: false,
  //cookie: { myvar: 'dfgh' }
}));

 //var urlencodedParser =bodyParser.urlencoded({ extended: false });
app.use('/LoginSuccess.html', bodyParser.urlencoded({ extended: true }));
//app.use(bodyParser.json());

var multerParser = multer({storage: storage});//dest:upload


app.use(expressvalidator());
app.use(passport.initialize());
app.use(passport.session());

app.get('/',(req,res)=>{
	console.log('Request was made to :'+req.url);
    var data={};
    data.crowd=[];
    data.gender='other';
    res.render('GitLab',data);
});
app.get('/gitlogo.jpg', function(req, res){
   res.sendFile(__dirname + '/views/gitlogo.jpg');
});

app.post('/LoginSuccess.html', (request, response) => {

	console.log('Request was made to :'+request.url);
	//request.checkBody('pwd', 'Invalid Password').isLength({min: 4,max:12});
	var data={};
	console.log('UserName:'+request.body.user);
	console.log('Password:'+request.body.pwd);
	const errors=request.validationErrors();
	//console.log(errors);

	User.findOne({userName: request.body.user}, (err, abcd)=>{

		console.log(abcd);
		console.log('Password from database:'+abcd.pwd);
		if(request.body.pwd === abcd.pwd)
		{
			request.login(request.body.user,function(err){});

			data.kingName = request.body.user;
			if(navaratnas.get(request.body.user)!== undefined)
			{data.ratnas = navaratnas.get(request.body.user);}
				else
				{
					data.ratnas =[];
				}
			response.render('Navaratna.ejs',data);
		}
		else
		{
			response.send('Cannot login '+request.body.user+', go back and login again');
		}

	});
	/*if(errors){
			data.errors = errors;
			data.success= false;
			console.log();
			response.redirect('/');
		}else if(kings.get(request.body.user) === request.body.pwd){
			data.success= true;
			request.login(request.body.user,function(err){
				data.kingName = request.body.user;
				data.ratnas = navaratnas.get(request.body.user);
			response.render('Navaratna.ejs',data);
		});
		}
		else
		{
			response.send('Cannot login '+request.body.user+', go back and login again');
		}*/
});

app.post('/RegistrationSuccess.html', multerParser.single('photo'), (request, response)=>{
	console.log('inside registration success');
console.log(request.file);
console.log(request.file.path);
var newUser = new User({
	userName : request.body.userName,
	pwd : request.body.pwd,
	filePath : request.file.path
	});
	newUser.save((err)=>
	{
		if(err)
		response.send('Error in creating user');
		else
		response.render('GitLab'	);
	});
});

passport.serializeUser(function(user, done) {
	done(null, user);
 });
 passport.deserializeUser(function(user, done) {
    done(null, user);
});
app.listen(PORT, () => {
    console.log('Listening to port:' + PORT);
});