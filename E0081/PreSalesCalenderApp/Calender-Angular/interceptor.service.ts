import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor{

 /* constructor(private injector:Injector) { }
  intercept(req, next){
	console.log('session storage:', sessionStorage.getItem('userName'));
  	var clonedReq = req.clone({
  		headers:req.headers.set('userName', 'Calender '+sessionStorage.getItem('userName'));
  	})
  	return next.handle(clonedReq);
  }*/
  constructor(private injector:Injector) { }
  intercept(req, next){
console.log('session storage:', sessionStorage.getItem('userName'));
  	var clonedReq = req.clone({
  	headers:req.headers.set('username', 'XYZ '+sessionStorage.getItem('userName'))
  	});
  	return next.handle(clonedReq);
  }
}