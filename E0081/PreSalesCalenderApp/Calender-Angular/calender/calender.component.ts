import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms'
import { Router } from '@angular/router';
import { CalenderService } from '../calender.service';

@Component({
  selector: 'app-calender',
  templateUrl: './calender.component.html',
  styleUrls: ['./calender.component.css']
})
export class CalenderComponent implements OnInit {
  eventForm:FormGroup;
  updateForm:FormGroup;
  eventsData =[];
  dispLogin = true;
  dispUpdateForm = false;
  dispLogout = false;
  eventsObject = sessionStorage.getItem('eventsData');
  show = false;
  message = true;
  validRole = false;
  eventButton = true;
  todaysEvents = [];
  overlappingData = [];
  constructor(private calenderService:CalenderService, private router:Router) {}
  ngOnInit() {
    var ssusername = sessionStorage.userName;
    if(typeof(ssusername) === 'string')
    {
      this.dispLogin = false;
      this.dispLogout = true;
      var role = JSON.parse(sessionStorage.role);
      console.log('Role:', role);
      if(role === 'manager')
      {
        this.validRole = true;
      }    
    }else{
      this.eventButton = false;
    }
    var todaysDate = new Date();
    var tomorrowsDate = new Date((new Date()).setDate(todaysDate.getDate() + 1));
    this.eventsData =  JSON.parse(this.eventsObject);
    for(let i=0; i<this.eventsData.length;i++)
    {
      var startTime = new Date(this.eventsData[i].startTime).toString();
      var endTime = new Date(this.eventsData[i].endTime).toString();
      //if((todaysDate.toString() < startTime.toString()) && (tomorrowsDate.toString()>endTime.toString()))
      //this.todaysEvents.push(this.eventsData[i]);
      this.eventsData[i].startTime = startTime.split('G')[0];
      this.eventsData[i].endTime = endTime.split('G')[0];
    }
    if(this.eventsData.length > 0){
      this.message = false;
    }
    this.overlappingData = JSON.parse(sessionStorage.getItem(overlappingData));  
    this.eventForm = new FormGroup({
      title: new FormControl(),
      startTime:new FormControl(),
      endTime:new FormControl(),
      file:new FormControl(),
      manager:new FormControl()
    });

    this.updateForm = new FormGroup({
      startTime:new FormControl(),
      endTime:new FormControl()
    })
  }

  addEvent(){
    console.log(this.eventForm.value);
    this.calenderService.postEvent(this.eventForm.value,(data)=>{
      if(data.error)
      {
        console.log('Error:', data.error);
        this.router.navigate(['error']);
      }else{
        console.log('events data:', data.data);
        this.eventsData = data.data;
        if(this.eventsData.length > 0){
          this.message = false;
        }
        sessionStorage.setItem('eventsData', JSON.stringify(data.data));
        this.router.navigate(['calender']);
      }
    })
  }

  getLogin(){
    this.router.navigate(['login']);
  }

  displayAddEvent(){
    if(this.show == false)
    this.show = true;
    else
    this.show = false;
  }

  logout(){
  sessionStorage.clear();
  this.router.navigate(['']);
  }
id = null;
  getUpdateForm(_id){
  this.id = _id;
    if(this.dispUpdateForm === false)
    this.dispUpdateForm = true;
    else
    this.dispUpdateForm = false;
  }

  updateEvent(){
    this.updateForm.value.id=this.id;
    this.calenderService.postUpdate(this.updateForm.value,(data)=>{
      if(data.error)
      {
        console.log('Error:', data.error);
        this.router.navigate(['error']);
      }else{
        this.overlappingData = data.overlappingData;
        this.updateForm.reset();
        this.router.navigate(['calender']);
      }
    })
  }
  listener(event){
    console.log('The image event:',event.files[0]);
  }
}
