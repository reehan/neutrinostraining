import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { CalenderService } from '../calender.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm:FormGroup;

  constructor(private calenderService:CalenderService, private router:Router) { }

  ngOnInit() {
	this.loginForm = new FormGroup({
  		userName: new FormControl(),
  		password:new FormControl()
  	})
  }

  login(){
  	this.calenderService.postLogin(this.loginForm.value,(data)=>{
    console.log('data:', data);
      if(data.error)
      {
      console.log('Error', data.error);
  		  this.router.navigate(['error']);
      }
  		else if(!data.isLogged)
      {
        this.loginForm.reset();
        this.router.navigate(['login']);
      }
      else{
        sessionStorage.setItem('userName', this.loginForm.value.userName);
        sessionStorage.setItem('overlappingData', JSON.stringify(data.overlappingData));
        sessionStorage.setItem('eventsData', JSON.stringify(data.data));
        sessionStorage.setItem('role', JSON.stringify(data.role));
        this.router.navigate(['calender']);
      }
  	})
  }

}
