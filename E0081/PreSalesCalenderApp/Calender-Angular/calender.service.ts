import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CalenderService {

  constructor(private http:HttpClient, private router:Router) { }

  getCalender(callback){
  	return this.http.get('http://localhost:3000').subscribe((response)=>{
  	callback(response);
  	},(err)=>{
  		console.log(err);
  	})
  }

  postEvent(eventData:any, callback){
	return this.http.post('http://localhost:3000/saveEvent', eventData).subscribe((response)=>{
  	callback(response);
  	},(err)=>{
  		console.log(err);
  	})
  }

  postLogin(loginData:any, callback){
  	return this.http.post('http://localhost:3000/login', loginData).subscribe((response)=>{
  		callback(response);
  		},(err)=>{
  			console.log(err);
        this.router.navigate[('')];
  	})
  }

  postUpdate(updateData:any, callback){
    return this.http.post('http://localhost:3000/update', updateData).subscribe((response)=>{
      callback(response);
    },(err)=>{
      console.log(err);
      this.router.navigate[('')];
    })
  }
}