import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CalenderService } from '../calender.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  constructor(private calenderService:CalenderService, private router:Router) { }

  ngOnInit() {
  }
  getCalender(){
    this.calenderService.getCalender((data)=>{
      if(data.error)
      this.router.navigate(['error']);
      else
      {
        if(!sessionStorage.userName)
        sessionStorage.setItem('eventsData', JSON.stringify(data.data));
        this.router.navigate(['calender']);
      }
    })
  }
  getLogin(){
  	this.router.navigate(['login']);
  }
}
