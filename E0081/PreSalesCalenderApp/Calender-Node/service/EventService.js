const eventDAO  = require('../dao/EventDaO');

module.exports.saveEventData =async function(body, userName){
	var isSaved =await eventDAO.saveEventData(body, userName);
	return isSaved;
}

module.exports.getEventsData = async function(){
	var data =await eventDAO.getEventsData();
	return data;
}

module.exports.getEmployeeEvents  = async function(userName){
	var events = await eventDAO.getEmployeeEvents(userName);
	return events;
}

module.exports.isEventsOverlapping = async function(body){
	var overLappingEvents = await eventDAO.getOverlappingEvents(body);
	if(overLappingEvents.length>0)
		return true;
	else
		return false;
}

module.exports.saveOverlappingEvent =async function(body, userName){
	var isSaved = await eventDAO.saveOverlappingEvent(body, userName);
	return isSaved;
}

module.exports.getSpecificOverlappingEvents =async function(manager){
	var overLappingEvents = await eventDAO.getSpecificOverlappingEvents(manager);
	return overLappingEvents;
}

module.exports.removeAnOverlappingEvent =async function(id){
	var removed = await eventDAO.removeAnOverlappingEvent(id);
	return removed;
}

module.exports.updateAnEvent =async function(removedData, body){
	console.log('Update an event service');
	var isUpdated = await eventDAO.updateAnEvent(removedData, body);
	return isUpdated;
}