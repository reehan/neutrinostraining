const employeeDAO = require('../dao/EmployeeDAO');

module.exports.getEmpData =async function(body){
	var empData = await employeeDAO.getEmpData(body.userName);
	return empData;
}