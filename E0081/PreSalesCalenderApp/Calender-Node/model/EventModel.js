const mongoose = require('mongoose');
const eventSchema = mongoose.Schema({
	userName:String,
	manager:String,
	title: String,
	startTime: Date,
	endTime: Date,
	file: String
});

var Event=mongoose.model('events', eventSchema);
var overlappingEvent = mongoose.model('overlappingevents', eventSchema);

module.exports.eventObject = function(){
    return Event;
}

module.exports.overlappingEventObject = function(){
    return overlappingEvent;
}