const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/calenderApp',{ useNewUrlParser: true });
const employeeSchema = mongoose.Schema({
	name:String,
	userName:String,
	password:String,
	role:String,
	email:String,
	manager:String
})
 var emp = mongoose.model('employees', employeeSchema);

 module.exports.employeeObject = function(){
 	return emp;
 }