const mongoose = require('mongoose');
const eventmodel = require('../model/EventModel');

module.exports.saveEventData = function(body, userName){
	var myPromise = new Promise(function(resolve, reject){
		var result={}
		var eventObject = eventmodel.eventObject();
		var event = new eventObject({
			title: body.title,
			userName: userName,
			manager: body.manager,
			startTime:body.startTime,
			endTime:body.endTime,
			file: body.file
		})
		
		event.save((err)=>{
			if(err)
				reject('Error while saving the event data to the database');
			else
				{result.status = true;
				resolve(result);
			}
		});
	});
	return myPromise;
}

module.exports.getEventsData = function(){
	var myPromise = new Promise(function(resolve, reject){
		var eventObject = eventmodel.eventObject();
		eventObject.find((err, response)=>{
			if(err)
				reject('Error while retrieving the events data drom database');
			else
				resolve(response);
		});
	});
	return myPromise;
}

module.exports.getEmployeeEvents = function(usersUserName){
	var myPromise = new Promise(function(resolve, reject){
		var eventObject = eventmodel.eventObject();
		eventObject.find({userName:usersUserName}, (err, response)=>{
			if(err)
				reject('Error while retrieving the employee events data from database');
			else
			{
				resolve(response);
			}
		});
	});
	return myPromise;
}

module.exports.getOverlappingEvents = function(body){
	var promise = new Promise((resolve, reject)=>{
		var eventObject = eventmodel.eventObject();
		eventObject.find({endTime:{$gt:new Date(body.startTime)},startTime:{$lt:new Date(body.endTime)},manager:body.manager}, (err, response)=>{
			if(err)
			{
				console.log("Error while checking the overlapping:", err);
				reject(err);
			}
			resolve(response);
		});
	});
	return promise;
}

module.exports.saveOverlappingEvent = (body, userName)=>{
	var promise = new Promise((resolve, reject)=>{
		var overlappingEvent = eventmodel.overlappingEventObject();
		var event = new overlappingEvent({
			title: body.title,
			userName: userName,
			manager: body.manager,
			startTime:body.startTime,
			endTime:body.endTime,
			file: body.file
		});
		event.save((err)=>{
			if(err)
			{
				console.log('Error from saving the overlapping event:', err);
				reject('Error while saving the overlapping event data to the database');
			}
			else{
				resolve(true);
			}
		});
	});
	return promise;
}

module.exports.getSpecificOverlappingEvents = function(manager){
	var promise = new Promise((resolve, reject)=>{
		var overlappingEvent = eventmodel.overlappingEventObject();
		overlappingEvent.find({manager:manager},(err, response)=>{
			if(err)
				reject('Error while retrieving the overlapped events for specific employee');
			resolve(response);
		})
	});
	return promise;
}

module.exports.removeAnOverlappingEvent = function(id){
	var promise = new Promise((resolve, reject)=>{
		var overlappingEvent = eventmodel.overlappingEventObject();
		overlappingEvent.findOneAndDelete({_id:id}, (err, response)=>{
			if(err)
			{
				console.log(err.toString());
				reject('Error while removing the overlapped event from collection');
			}
			else
			resolve(response);
		});
	});
	return promise;
}

module.exports.updateAnEvent = function(removedData, body){
	console.log('update an event dao');
	var myPromise = new Promise(function(resolve, reject){
		var eventObject = eventmodel.eventObject();
		console.log('Manager event dao:', removedData.manager );
		eventObject.update({userName:removedData.userName, manager:removedData.manager, startTime:new Date(removedData.startTime), endTime:new Date(removedData.endTime)},{$set:{startTime:new Date(body.startTime), endTime:new Date(body.endTime)}}, (err, response)=>{
			if(err)
				reject('Error while retrieving the events data drom database');
			else
				resolve(response);
		});
	});
	return myPromise;
}
/*,{manager:'body.manager'}*/
/*eventObject.find({$all:[{endTime:{$gt:new Date(body.startTime)}},{startTime:{$lt:new Date(body.endTime)}}]}*/