const mongoose = require('mongoose');
const empModel = require('../model/EmployeeModel');
mongoose.connect('mongodb://localhost/calenderApp');

module.exports.getEmpData = function(UsersUserName){
	var promise  = new Promise(function(resolve, reject){
		var emp = empModel.employeeObject();
		emp.findOne({userName:UsersUserName}, (err, response)=>{
			if(err){
				console.log('There has been a error in DB response:', err);
				reject(err);
			}
			else
			{
				resolve(response);
			}
		});
	});
	return promise;
}