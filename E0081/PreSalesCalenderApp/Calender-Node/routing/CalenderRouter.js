const express = require('express');
const mongoose = require('mongoose');
var nodemailer = require('nodemailer');
const router = express.Router();
const eventService = require('../service/EventService');
const empService = require('../service/EmployeeService');

mongoose.connect('mongodb://localhost/calenderApp', { useNewUrlParser: true });

var userName = '';
router.get('/',(req,res)=>{
	console.log('serving the events data');
	Response = {};
	Response.data =[];
	Response.error = null;
	eventService.getEventsData().then((eventsData)=>{
		Response.data = eventsData;
		res.send(Response);
	}).catch((error)=>{
		Response.error = error.toString();
		res.send(Response);
	})
})
.post('/saveEvent', (req, res)=>{
	console.log('Saving the event data');
	Response = {};
	Response.data = [];
	Response.error = null;
	Response.isExisting = false;
	var username = req.headers.username.split(' ')[1];
	eventService.isEventsOverlapping(req.body).then((isOverlapping)=>{
		console.log('Is overlapping:', isOverlapping);
		if(isOverlapping)
		{
			eventService.saveOverlappingEvent(req.body, username).then((isSaved)=>{
				if(isSaved)
				{
					var transporter = nodemailer.createTransport({
						service:'gmail',
						port: 25,
						secure: false,
						auth: {
							user: 'manjunathar21996@gmail.com',
							pass: '8693972564'
						}
					});
					var mailOptions = {
						from:"'Manjunatha R' <manjunathar21996@gmail.com>",
						to: 'manjunathar21996@yahoo.com',
						subject:'About Overlapping event-'+req.body.title+'.',
					  	text: 'Your schedule with '+ req.headers.username.split(' ')[1] + ' is Overlapping with already existing Event. Please reschedule the event. Thank you...'
					};
					transporter.sendMail(mailOptions, function(error, info){
					  	if (error){
					   		console.log(error);
					  	} else {
							console.log('Email sent: ' + info.response);
					  	}
					});					
				}
			}).catch((err)=>{
				console.log(err.toString());
				Response.error = err.toString();
				res.send(Response);
			});
		}
	}).catch((err)=>{
		Response.error = err.toString();
		res.send(Response);
	});
	eventService.saveEventData(req.body, username).then((result)=>{
		if(result.status)
		{
			eventService.getEmployeeEvents(userName).then((eventsData)=>{
				Response.data = eventsData;
				res.send(Response);
			}).catch((error)=>{
				Response.error = error.toString();
				res.send(Response);
			});
		}
	}).catch((err)=>{
		Response.error = err.toString();
		res.send(Response);
	});
})
.post('/login', (req, res)=>{
	console.log('Serving the specific employee calender');
	Response = {};
	Response.data = [];
	Response.error  = null;
	Response.isLogged = false;
	Response.role = null;
	Response.overlappingData = [];
	empService.getEmpData(req.body).then((empData)=>{
		if(empData.password === req.body.password)
		{
			req.login(req.body.userName, function(err){
	            if(err){
    		        Response.error = err
		            res.send(Response);
    	        }
    	        Response.role = empData.role;
    	        req.session.username = req.body.userName; 
				Response.isLogged = true;
				userName = req.body.userName;
				eventService.getEmployeeEvents(req.body.userName).then((respData)=>{
					Response.data = respData;
					if(empData.role === 'manager')
					{
						eventService.getSpecificOverlappingEvents(req.body.userName).then((overlappingEvents)=>{
							Response.overlappingData = overlappingEvents;
							res.send(Response);
						}).catch((err)=>{
							console.log('Error:', err);
							Response.error = err.toString();
							res.send(Response);
						});
					}else{
						res.send(Response);
					}
				}).catch((err)=>{
					console.log('error: ',err);
					Response.error = err.toString();
					res.send(Response);
				});
			});
		}else{
			res.send(Response);
		}
	}).catch((err)=>{
		Response.error = err.toString();
		res.send(Response);
	});
})
.post('/update',(req, res)=>{
	Response = {};
	Response.overlappingData = [];
	Response.error = null;
	eventService.removeAnOverlappingEvent(req.body.id).then((removedData)=>{
		console.log('The remove result:',removedData);
		req.body.manager = req.headers.username.split(' ')[1];
		eventService.isEventsOverlapping(req.body).then((isOverlapping)=>{
			if(isOverlapping)
			{
				eventService.updateAnEvent(removedData, req.body).then((isUpdated)=>{
					console.log('Is updated:', isUpdated);
				}).catch((err)=>{
					console.log(err);
					Response.error = err.toString();
					res.send(Response);
				});
				removedData.startTime = req.body.startTime;
				removedData.endTime = req.body.endTime;
				eventService.saveOverlappingEvent(removedData, removedData.userName).then((isSaved)=>{
					eventService.getSpecificOverlappingEvents(req.headers.username.split(' ')[1]).then((data)=>{
						Response.overlappingData = data;
						res.send(Response);
						console.log('Overlapping data:', data);
					}).catch((err)=>{
						Response.error = err.toString();
						res.send(Response);
					});
				}).catch((err)=>{
					Response.error = err.toString();
					res.send(Response);
				});
			}
			else
			{
				eventService.updateAnEvent(removedData, req.body).then((isUpdated)=>{
					console.log('Is updated:', isUpdated);
					eventService.getSpecificOverlappingEvents(req.headers.username.split(' ')[1]).then((data)=>{
						Response.overlappingData = data;
						res.send(Response);
					}).catch((err)=>{
						Response.error = err.toString();
						res.send(Response);
					});
				}).catch((err)=>{
					console.log(err);
					Response.error = err.toString();
					res.send(Response);
				});
			}
		}).catch((err)=>{
			console.log(err);
			Response.error = err.toString();
			res.send(Response);
		})
	}).catch((err)=>{
		console.log(err);
	})
})
module.exports = router;