const express = require('express');
const morgan = require('morgan')
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const expressHandlebars = require('express-handlebars');
const flash = require('connect-flash');
const session = require('express-session');
const mongoose = require('mongoose');
const passport = require('passport');
const bcrypt = require('bcryptjs');
const validator = require('express-validator');
const ejs = require('ejs');
const LocalStrategy = require('passport-local').Strategy;
//const mongoose = require('mongoose');
const multer = require('multer');






//require('./config/passport');

//mongoose.connect('mongodb://localhost/codework3');

const app = express();

// View Engine
//app.set('static', path.join(__dirname, 'static'));
//app.engine('handlebars', expressHandlebars({ defaultLayout: 'layout' }));
app.set('view engine', 'ejs');
      
app.set('view engine', 'ejs');

 const storage = multer.diskStorage({
destination: (req, file, callback)=>{
callback(null, 'uploads/');
},
filename: function (req, file, callback) {
    callback(null, file.originalname);
  }
});


var parse = multer({storage:storage});


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
var urlencodedParser = bodyParser.urlencoded({ extended: false });

app.use(session({
  cookie: { maxAge: 60000 },
  secret: 'codeworkrsecret',
  saveUninitialized: false,
  resave: false
}));

app.use(passport.initialize());
app.use(passport.session());

app.get('/', (req, res) => {
    res.render('suitefront');
});

app.get('/index', (req, res) => {
    res.render('index');
});

app.get('/register',(req, res) => {
    res.render('register');
  });

app.get('/login',(req, res) => {
    res.render('login');
  });

app.get('/profile1',(req, res) => {
    res.render('profile1');
  });

app.get('/dashboardsticky',(req, res) => {
    res.render('dashboardsticky');
  });

app.get('/createAccounts',(req, res) => {
    res.render('createAccounts');
  });

app.get('/createContacts',(req, res) => {
    res.render('createContacts');
  });

app.get('/dashboardpost',(req, res) => {
    res.render('dashboardpost');
  });
  
app.get('/dashboardpage',(req, res) => {
    res.render('dashboardpage');
  });

app.get('/dashboarduser',(req, res) => {
    res.render('dashboarduser');
  });

  



app.use('/css',express.static('css'));
app.use('/js',express.static('js'));






app.get('/back.jpg',(req,res)=>{
    
    res.sendFile(__dirname +'/'+ '/views/frontstyle/back.jpg');

});

app.get('/slide.jpg',(req,res)=>{
    
    res.sendFile(__dirname +'/'+ '/views/frontstyle/slide.jpg');

});

app.get('/icon1.png',(req,res)=>{
    
    res.sendFile(__dirname +'/'+ '/views/frontstyle/icon1.png');

});

app.get('/suite.png',(req,res)=>{
    
    res.sendFile(__dirname +'/'+ '/views/frontstyle/suite.png');

});


app.get('/bootsnav.css',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ './views/frontstyle/bootsnav.css');
});

app.get('/bootsnav.js',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ './views/frontstyle/bootsnav.js');
});

app.get('/bootstrap.min.js',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ './views/frontstyle/bootstrap.min.js');
});

app.get('/bootstrap.min.css',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ './views/frontstyle/bootstrap.min.css');
});

app.get('/bootstrap.css',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ './views/frontstyle/bootstrap.css');
});

app.get('/font-awesome.min.css',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ './views/frontstyle/font-awesome.min.css');
});

app.get('/jquery.min.js',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ './views/frontstyle/jquery.min.js');
});







app.get('/register/1b.png',(req,res)=>{
    
    res.sendFile(__dirname +'/'+ '/views/register/1b.png');

});


app.get('/suite1.png',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ '/views/suite1.png');

});

app.get('/graph.jpg',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ '/views/graph.jpg');

});

app.get('/hand1.jpg',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ '/views/hand1.jpg');

});

app.get('/thumb.png',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ '/views/thumb.png');

});

app.get('/sa1.jpg',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ '/views/sa1.jpg');

});

app.get('/assured.png',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ '/views/assured.png');

});

app.get('/setting.png',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ '/views/setting.png');

});

app.get('/suite4.png',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ '/views/suite4.png');

});

app.get('/suite5.png',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ '/views/suite5.png');

});
app.get('/back.jpg',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ '/views/back.jpg');

});

app.get('/1b.png',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ '/views/register/1b.png');

});

app.get('/suite1.png',(req,res)=>{
    
    res.sendFile(__dirname +'/'+ '/views/suite1.png');

});

app.get('/1a.png',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ '/views/1a.png');
});

app.get('/bootstrap.css',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ './views/bootstrap.css');
});

app.get('/bootstrap.min.css',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ './views/bootstrap.min.css');
});
app.get('/font-awesome.min.css',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ './views/font-awesome.min.css');
});

app.get('/jquery.DonutWidget.min.css',(req,res)=>{
   
    res.sendFile(__dirname +'/'+ './views/jquery.DonutWidget.min.css');
});

app.get('/linearicons.css',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ './views/linearicons.css');
});

app.get('/owl.carousel.css',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ './views/owl.carousel.css');
});

app.get('/main.css',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ './views/main.css');
});

app.get('/footer.css',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ './views/footer.css');
});

app.get('/coll1.js',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ './views/coll1.js');
});

app.get('/dashboardsticky/bootstrap.min.css',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ './views/frontstyle/bootstrap.min.css');
});

app.get('/boot.css',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ './views/boot.css');
});

app.get('/dashboardpost/bootstrap.min.css',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ './views/frontstyle/bootstrap.min.css');
});

app.get('/dashboardpage/bootstrap.min.css',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ './views/frontstyle/bootstrap.min.css');
});

app.get('/dashboarduser/bootstrap.min.css',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ './views/frontstyle/bootstrap.min.css');
});
app.get('/style.css',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ './views/frontstyle/style.css');
});



mongoose.connect('mongodb://localhost/codework5');

var userSchema=mongoose.Schema({
    myname3: String,
    //myemail2: String,
    //myname2: String,
    //myconemail3: String,
    myconemail2: String,
    //mypass2:String
});
const user=mongoose.model("us11",userSchema);

app.post('/register',urlencodedParser,(req,res)=>{
    
      
//Get the parsed information

var newuser = new user({
    myname3:req.body.username,
    //myemail2: req.body.name,
    //myname2: req.body.email,
    //myconemail3:req.body.confirmationemailcmdcm
    myconemail2: req.body.password,
    //mypass2: req.body.confirmationPassword
    


});
    console.log(newuser);
newuser.save((err)=>{
    if(err)
        res.send('errrrrrr');
     else
    res.redirect('/login');
   //res.redirect('/login');
});
});

app.post('/login',urlencodedParser,(req,res)=>{
    console.log(req.body.uName);
    console.log(req.body.uPassword);
    //console.log(newuser);
    //console.log(user.pass1);
    
    
    user.findOne({myname3:req.body.uName}, (err, response)=>{
    console.log(response);
       console.log(response.myname3);

           if(req.body.uPassword===response.myconemail2){

//res.send('registered')
      //res.redirect('/dashboardsticky');
      res.render('dashboardsticky',{reg:response.myname3});
           }else
               {
      res.redirect('/login');
               }
     });

  });

/*app.get('/profile1',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.render('profile1',{reqdata:username})
});*/


//mongoose.connect('mongodb://localhost/profileDetails');

var userSchema=mongoose.Schema({
    myname2:String,
    myemail2:String,
    myconemail2:String,
    mypass2:String,
    //photoPath: String,
    myname3:String,
    myemail3:String,
    myconemail3:String,
    mypass3:String,
    myname4:String,
    myemail4:String,
    myconemail4:String,
    mypass4:String,
    myname5:String,
    myemail5:String,
    myconemail5:String,
    mypass5:String,
    myname6:String,
    myemail6:String,
    myconemail6:String,
    mypass6:String,
    myname7:String,
    myemail7:String,
    myconemail7:String,
    mypass7:String,
    address:String,
    client:String,
    editor:String
   
    
});
const user1=mongoose.model("acc2",userSchema);
app.post('/profile',urlencodedParser,parse.single('userImage'),(req,res)=>{
    
     /*req.checkBody('username','User name is invalid');
    req.checkBody('fname','password is invalid');
        req.checkBody('Active','password is invalid');

        req.checkBody('lname','password is invalid');
        req.checkBody('status','password is inva7id');
    req.checkBody('file1','User name is invalid');
    req.checkBody('ename','password is invalid');
        req.checkBody('wphone','password is invalid');

        req.checkBody('title','password is invalid');
        req.checkBody('mobile','password is invalid');
    req.checkBody('dept','User name is invalid');
    req.checkBody('ophone','password is invalid');
        req.checkBody('report','password is invalid');

        req.checkBody('fax','password is invalid');
        req.checkBody('field','password is invalid');
     req.checkBody('hphone','User name is invalid');
    req.checkBody('field1','password is invalid');
        req.checkBody('iname','password is invalid');

        req.checkBody('imname','password is invalid');
        req.checkBody('city','password is invalid');
    req.checkBody('street','User name is invalid');
    req.checkBody('postal','password is invalid');
        req.checkBody('state','password is invalid');

        req.checkBody('country','password is invalid');
        req.checkBody('text1','password is invalid');
            req.checkBody('report1','password is invalid');*/

        console.log(req.file);
    
   

    //let errors=req.validationErrors();
      
//Get the parsed information

var newuser = new user1({
myname2: req.body.username,
    myemail2: req.body.fname,
    myconemail2: req.body.Active,
mypass2: req.body.lname,
    //photoPath: req.file.path,
    myname3: req.body.status,
    myemail3: req.body.file1,
    myconemail3: req.body.ename,
mypass3: req.body.wphone,
    myname4: req.body.title,
    myemail4: req.body.mobile,
    myconemail4: req.body.dept,
mypass4: req.body.ophone,
    myname5: req.body.report,
    myemail5: req.body.fax,
    myconemail5: req.body.field,
mypass5: req.body.iname,
    myname6: req.body.imname,
    myemail6: req.body.city,
    myconemail6: req.body.street,
mypass6: req.body.postal,
    myname7: req.body.state,
    myemail7: req.body.country,
    myconemail7: req.body.text1,
mypass7: req.body.report1,
    address: req.body.email11,
    client: req.body.element1,
editor: req.body.element2
   
    


});
    

    console.log(newuser);
newuser.save(function(err, user1){
if(err){
    res.render('errPage', {message: "Database error", type:
"error"});}else
    {
        res.redirect('/profile1');
    }

});
    /*user1.findOne({myname3:req.body.myname2},function(err,response){
        console.log(response.myname2);
        res.render('dashboard',{reg1:response.myname2});
        });*/

});

mongoose.connect('mongodb://localhost/contactDetails');

var userSchema=mongoose.Schema({
    salutation:String,
    firstname:String,
lastname:String,
officephone:String,
mobileNo:String,
title:String,
department:String,
accountname:String,
fax:String,
email:String,
address:String,
city:String,
state:String,
postalCode:String,
country:String,
    photoPath: String
    
});
const PhotoUser=mongoose.model("users",userSchema);


app.get("/contactDetails",(req,res)=>{
res.render("createContacts.ejs");

});

app.use(session({
  secret: 'neutrionos',
  resave: false,
  saveUninitialized: true,
  //cookie: { myvar: 'dfgh' }  // needed certificate
}));



app.use(validator());
app.use(passport.initialize());
app.use(passport.session());


app.post('/createContacts',urlencodedParser,(req,res)=>{
var newuser = new PhotoUser({
    salutation:req.body.salutation,
    firstname: req.body.username,
    lastname: req.body.lastname,
    officephone: req.body.officephone,
    mobileNo: req.body.mobileNo,
    title:req.body.title,
    department:req.body.department,
    accountname:req.body.accountname,
    fax: req.body.fax,  
    email:req.body.email,
    address:req.body.address,
    city:   req.body.city,
    state:req.body.state,
    postalCode:req.body.postalCode,
    country:req.body.country
    
    });
    console.log('Printing user');
    console.log(newuser);
    newuser.save(function(err, user){
        if(err)
      res.send('error occured while saving to db');
        else
         res.send('register data got created');
          });
});

//creating accounts

var urlencodedParser = bodyParser.urlencoded({ extended: false });
app.use(validator());

app.use(passport.initialize());
app.use(passport.session());

var parser = multer({storage:storage});

mongoose.connect('mongodb://localhost/accountDetails');

var userSchemaa=mongoose.Schema({
    name:String,
    officephone:String,
website:String,
fax:String,
email:String,
billing_address:String,
city:String,
state:String,
postal_code:String,
country:String,
description:String,
    photoPath:String
    
    
});
const AccountUser=mongoose.model("user",userSchemaa);


app.get("/accountDetails",(req,res)=>{
res.render("createAccounts.ejs");

});

app.use(session({
  secret: 'neutrionos',
  resave: false,
  saveUninitialized: true,
  //cookie: { myvar: 'dfgh' }  // needed certificate
}));



app.use(validator());
app.use(passport.initialize());
app.use(passport.session());



app.post('/create',urlencodedParser,(req,res)=>{
var newuser = new AccountUser({
    name:req.body.name,
    officephone:req.body.officephone,
website:req.body.website,
fax:req.body.fax,
email:req.body.email,
billing_address:req.body.billing_address,
city:req.body.city,
state:req.body.state,
postal_code:req.body.postal_code,
country:req.body.country,
description:req.body.description
    
    });
    console.log('Printing users Account Details');
    console.log(newuser);
    newuser.save(function(err, user){
        console.log('to save');
        if(err)
      res.send('error occured while saving to db');
        else
         res.send('accounts data got created');
          });
});


app.listen(3070, () => console.log('Server started listening on port 3000!'));
