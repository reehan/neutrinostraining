const express = require('express');
const ejs = require('ejs');
const PORT = 3000;
const bodyParser = require('body-parser')
const passport = require('passport');
const session = require('express-session');
const expressvalidator = require('express-validator');
const mongoose = require('mongoose');
const multer = require('multer');




mongoose.connect('mongodb://localhost/book'); 
var BookSchema = mongoose.Schema({
	userName : String,
	pwd : String,
    name:String
});



var User=mongoose.model('bookcollections',BookSchema);  // here bookcollections is our collection

const app = express();
app.set('view engine', 'ejs');

app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: false,
  //cookie: { myvar: 'dfgh' }
}));


 //var urlencodedParser =bodyParser.urlencoded({ extended: false });
app.use(bodyParser.urlencoded({ extended: true }));
//app.use(bodyParser.json());



app.use(expressvalidator());
app.use(passport.initialize());
app.use(passport.session());

app.get('/',(req,res)=>{
	console.log('Request was made to :'+req.url);
    var data={};
   
    res.render('index',data);
});




app.get('/foo.jpg',(req,res)=>{
res.sendFile(__dirname+"/views/foo.jpg")
});


app.get('/foo2.jpg',(req,res)=>{
res.sendFile(__dirname+"/views/foo2.jpg")
});
app.get('/1.png',(req,res)=>{
res.sendFile(__dirname+"/views/1.png")
});
app.get('/2.png',(req,res)=>{
res.sendFile(__dirname+"/views/2.png")
});
app.get('/apple.png',(req,res)=>{
res.sendFile(__dirname+"/views/apple.png")
});
app.get('/belowcr2.png',(req,res)=>{
res.sendFile(__dirname+"/views/belowcr2.png")
});
app.get('/belowcr3.png',(req,res)=>{
res.sendFile(__dirname+"/views/belowcr3.png")
});
app.get('/cr1.jpeg',(req,res)=>{
res.sendFile(__dirname+"/views/cr1.jpeg")
});

app.get('/cr2.jpg',(req,res)=>{
res.sendFile(__dirname+"/views/cr2.jpg")
});
app.get('/cr3.jpg',(req,res)=>{
res.sendFile(__dirname+"/views/cr3.jpg")
});
app.get('/cr4.jpg',(req,res)=>{
res.sendFile(__dirname+"/views/cr4.jpg")
});
app.get('/web-logo..png',(req,res)=>{
res.sendFile(__dirname+"/views/web-logo..png")
});


app.get("/login.ejs",(req,res)=>{
//console.log(req.body.email);
//console.log(req.body.psw);
res.render("login")
});

app.get('/register.ejs',(req,res)=>{
res.render('register')
});


app.get('/a.ejs',(req,res)=>{
res.render('a')
});

app.get('/jquery-1.3.2.min.js',(req,res)=>{
    res.sendFile(__dirname+"/views/jquery-1.3.2.min.js")
});

app.get('/custom.js',(req,res)=>{
    res.sendFile(__dirname+"/views/custom.js")
});

app.get('/style.css',(req,res)=>{
    res.sendFile(__dirname+"/views/style.css")
});




app.post('/RegSuccess',(request, response)=>{
	console.log('inside registration success'+request.body.pwd);

var newUser = new User({
	userName : request.body.UserName,
	pwd : request.body.pwd,
    name :request.body.name
	});
	newUser.save((err)=>
	{
		if(err)
		response.send('Error in creating user');
		else
		response.render('login');
	});
    console.log(newUser);
});



app.post('/sucess', (request, response) => {
console.log(request.body);
	console.log('Request was made to :'+request.url);
    console.log('inside login success');
	//request.checkBody('pwd', 'Invalid Password').isLength({min: 4,max:12});
	var data={};
	console.log('UserName:'+request.body.UserName);
	console.log('pwd:'+request.body.pwd);
    console.log('name:'+request.body.name);
	const errors=request.validationErrors();
	//console.log(errors);

	User.findOne({userName: request.body.UserName}, (err, abcd)=>{

		console.log(abcd);
		console.log('Password from database');
       
		if( abcd!==null && request.body.pwd === abcd.pwd)
		{
			request.login(request.body.user,function(err){});

			
			response.render('invoice.ejs',{reg:request.body.name});
		}
		/*else if(abcd!==null)
		{
			response.send('Cannot login '+request.body.UserName+', wrong email or password please type corrext details');
		}*/
        else
            {
                // response.send('Cannot login '+request.body.UserName+', go back and register please');
             response.render('a',{reg:request.body.UserName});
              /* request.flash("sometext","please check password r email");
            response.render('a',{reg:request.body.UserName}); */

            }

	});
	
});









passport.serializeUser(function(user, done) {
	done(null, user);
 });
 passport.deserializeUser(function(user, done) {
    done(null, user);
});






app.listen(PORT, () => {
    console.log('Listening to port:' + PORT);
});