# Project Title

One Paragraph of project description goes here

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo


### Running the application

Explain how to run the project and brief on the usage.

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* Any dependency management tool that was used
* Any steps that needs to be followed to build the project

## Contributing

Write here how one can contribute back to your project:
* You can provide a way to contact, like email
* You can point to bugs list and pick any to fix
* You can ask them to send patches in specific ways to help

## Versioning

This is the initial version being released

## Authors

* **First Author** - *Initial work* - 
* **Second Author** - *UI design work* - 

## License

This project is licensed under the GNU GPL License

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
