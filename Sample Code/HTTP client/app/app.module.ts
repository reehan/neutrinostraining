import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { ObjectComponent } from './object/object.component';
import { RelationShipComponent } from './relation-ship/relation-ship.component';
import { routing } from './app.routing';
import { CreateComponent } from './create/create.component';
import { HttpModule } from '../../node_modules/@angular/http';
import { ObjectService } from './services/object.service';
//import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    ObjectComponent,
    RelationShipComponent,
    CreateComponent
    
  
  ],
  imports: [
    BrowserModule,
    FormsModule,
    routing,
    HttpModule,
    ReactiveFormsModule
  ],
  providers: [ObjectService],
  bootstrap: [AppComponent]
})
export class AppModule { }
