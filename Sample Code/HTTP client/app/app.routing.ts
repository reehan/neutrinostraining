import { Routes, RouterModule } from '@angular/router';
import { ObjectComponent } from './object/object.component';
import { RelationShipComponent } from './relation-ship/relation-ship.component';
import { CreateComponent } from './create/create.component';

const appRoutes: Routes = [

    {
        path: 'create',
        component: CreateComponent,
        children:[
        {
            path: 'object',
            component: ObjectComponent
        },
        {
            path: 'relationship',
            component: RelationShipComponent
        }]
    }
   
];
export const routing = RouterModule.forRoot(appRoutes);