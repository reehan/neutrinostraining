import { Injectable } from '@angular/core';
import { Http, RequestOptions ,Headers} from "@angular/http";
// import { observable } from 'rxjs/Observable';
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';
// import 'rxjs/add/observable/throw';
import { Observable } from '../../../node_modules/rxjs';

@Injectable({
  providedIn: 'root'
})
export class ObjectService {

  constructor(private http:Http) { }
  // postPersonData(person:any)
  // {
  //   console.log(person);
  //   this.http.post('http://localhost:8080/create', person);
  // }
  postSportData(sport:any)
  {
    console.log(sport);
    this.http.post('http://localhost:3000/post', sport);
  }
  postTimeData(time:any)
  {
    
    console.log(time);
    this.http.post('http://localhost:3000/post', time); 
  }
  postEventData(event:any)
  {
     
    console.log(event);
    this.http.post('http://localhost:3000/post', event); 
  }
  postLocationData(location:any)
  {
    console.log(location);
    this.http.post('http://localhost:3000/post', location); 
  }


postPersonData(person:any) {
  let header = new Headers({ 'Content-Type': 'application/json' });
  let header_new = new Headers({ 'Access-Control-Allow-Origin': '*' });
  let options = new RequestOptions({ headers: header});
  return this.http.post('http://localhost:3000/create ', person)
    .subscribe(data => console.log(data));
    // .catch((error: Response) => {
    //   return Observable.throw(error);
    // });
  }
}