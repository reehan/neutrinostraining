import { Component, OnInit } from '@angular/core';
import { ObjectService } from '../services/object.service';
import { person } from '../model/person';

@Component({
  selector: 'app-object',
  templateUrl: './object.component.html',
  styleUrls: ['./object.component.css']
})
export class ObjectComponent implements OnInit {
  model:person;

  public objectData: any[] = [

    { "id": 1, "name": "Person" },
    { "id": 2, "name": "Time" }, { "id": 3, "name": "Sport" }, { "id": 4, "name": "Event" }, { "id": 5, "name": "Location" }];
 
 
    constructor( private objectService:ObjectService) { 
      this.model=new person();
    }

  ngOnInit() {
  }
  submitPersonData(){
    console.log(this.model);
    this.objectService.postPersonData(this.model);

  }
  submitSportData()
  {
    console.log(this.model)
    this.objectService.postSportData(this.model);
  }
  submitTimeData()
  {
    console.log(this.model)
    this.objectService.postTimeData(this.model);
  }
  submitEventData()
  {
    console.log(this.model)
    this.objectService.postEventData(this.model);
  }
  submitLocationData()
  {
    console.log(this.model)
    this.objectService.postLocationData(this.model);
  }
}
