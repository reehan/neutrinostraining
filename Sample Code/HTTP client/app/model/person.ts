export class person{
    personName:string;
    typeOfCalendar:string;
    address:string;
    birthDate:Date;
    deathDate:Date;

    
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }

}