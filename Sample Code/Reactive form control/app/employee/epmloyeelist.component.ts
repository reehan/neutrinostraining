import { Component, OnInit } from '@angular/core';
// Import FormGroup and FormControl classes
import { FormGroup, FormControl,FormBuilder ,Validators} from '@angular/forms';

@Component({
  selector: 'app-epmloyeelist',
  templateUrl: './epmloyeelist.component.html',
  styleUrls: ['./epmloyeelist.component.css']
})
export class EpmloyeelistComponent implements OnInit {
 // This FormGroup contains fullName and Email form controls
 employeeForm: FormGroup;
  constructor(private fb:FormBuilder) { }
 // Initialise the FormGroup with the 2 FormControls we need.
  // ngOnInit ensures the FormGroup and it's form controls are
  // created when the component is initialised

  
  ngOnInit() {
    this.employeeForm = this.fb.group({
      fullName: [''],
      email:['']
      });
     } 
     onSubmit(): void {
      console.log(this.employeeForm.value);
    }
 }
 
 
