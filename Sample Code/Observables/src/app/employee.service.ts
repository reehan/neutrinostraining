import { IEmployee } from './employee';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';



@Injectable()
export class EmployeeService{

    private _url: string = "assets/apidata/employeedata.json";

    constructor(private _http: HttpClient){}
    getEmployees(): Observable<IEmployee[]>{
        return this._http.get<IEmployee[]>(this._url);
    }
  }
