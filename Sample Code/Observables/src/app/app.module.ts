import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent }   from './app.component';
import { EmployeeListComponent } from './employee-list.component'
import { EmployeeService } from './employee.service'
import { EmployeeDetailComponent } from './employee-detail.component'

@NgModule({
  imports:      [ BrowserModule, HttpClientModule ],
  declarations: [ AppComponent, EmployeeListComponent, EmployeeDetailComponent ],
  providers:    [ EmployeeService ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
