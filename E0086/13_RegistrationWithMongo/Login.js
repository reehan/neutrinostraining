const express = require('express');
const ejs = require('ejs');
const PORT = 3000;

const app = express();
app.set('view engine', 'ejs');

var kings = new Map();
kings.set('Vikramaditya','vikramaditya');
kings.set('Akbar','akbar');
kings.set('Raja','rajakrishnachandra');

var nr = new Map();
nr.set('Vikramaditya', ['Amarasimha', 'Dhanvantari', 'Ghatakarapara', 'Kalidasa', 'Kshapanaka', 'Shanku', 'Varahamihira', 'Vararuchi', 'Vetala-Bhatta']);
nr.set('Akbar', ['Abul-Fazl', 'Raja Todar Mal', 'Abdul Rahim Khan-I-Khana', 'Birbal', 'Mulla Do-Piyaza', 'Faizi', 'Fakir Aziao-Din', 'Tansen', 'Raja Man Singh']);
nr.set('Raja', ['Gopal Bhar', 'Bharatchandra Ray', 'Ramprasad Sen']);


app.get('/', function(req, res)
{
console.log('Request was made to '+req.url);
res.render('Login');
});

app.get('/3.jpg', function(req, res){
res.sendFile(__dirname + '/views/3.jpg');
});

app.get('/style.css', function(req, res){
   res.sendFile(__dirname  + "/" + '/views/style.css');
});


app.post('/login', (request, response) => {
	var data='';
	request.on('data', function(chunk){
    data += chunk;
	});

	request.on('end', function(){
	console.log('Request was made to '+request.url);
	console.log('data '+data);

	data = data.split('&')
	let king = data[0].split('=');
	let pwd = data[1].split('=');
	king = king[1];
	pwd = pwd[1];
	console.log(king);
	if(kings.get(king) === pwd)
	{
		response.render('navratnas.ejs', {kingName: king, kingRatnas:nr.get(king)});
	}
	});

});

app.listen(PORT, () => {
    console.log('Listening to port:' + PORT);
});
