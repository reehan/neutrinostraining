
const express = require('express');
const ejs = require('ejs');
const bodyParser = require('body-parser');
const session = require('express-session');
const validator = require('express-validator');
const passport = require('passport');
const mongoose = require('mongoose');
const multer = require('multer');

const PORT = 3000;
const app=express();

app.set('view engine', 'ejs');

const cont = multer.diskStorage({
destination: (req, file, callback)=>{
callback(null, 'uploads/');
},
filename: function (req, file, callback) {
    callback(null, file.originalname);
  }
});

var parse = multer({storage:cont});

let logger=(req,res,next)=>{
  console.log('Request received from URL:' + req.url);
  next();
};

var router=express.Router();

router.get('/',function(req,res){
res.send('About');
});

app.use(session({
  secret: 'shotgun',
  resave: false,
  saveUninitialized: true,
}))


var urlencodedParser = bodyParser.urlencoded({ extended: false });
app.use(validator());

app.use(passport.initialize());
app.use(passport.session());

app.use(express.static('public'));

mongoose.connect('mongodb://localhost/regdata');

var userSchema=mongoose.Schema({
    fname:String,
    uname:String,
    remail:String,
    pass:String,
    photoPath: String
    
});
const user=mongoose.model("user",userSchema);

app.post('/register',urlencodedParser,parse.single('uimg'),(req,res)=>{
    
        req.checkBody('firstname','first name is invalid');
        req.checkBody('username','user name is invalid');
        req.checkBody('email','email is invalid');
        req.checkBody('Password','password is invalid');
        req.checkBody('uimg','password is invalid');
        console.log(req.file);
    
   

    let errors=req.validationErrors();
      
//Get the parsed information

var newuser = new user({
    fname: req.body.firstname,
    uname: req.body.username,
    remail: req.body.email,
    pass: req.body.Password,
    photoPath: req.file.path
});

console.log(newuser);
newuser.save(function(err, user){
if(err)
res.render('errPage', {message: "Database error", type:
"error"});
else
res.send('registeration data stored in DB');
});
    
});
         
        


app.get('/',(req,res)=>{
    var data={};
    data.crowd=[];
    data.gender='other';
    res.render('register',data);
});

app.get('/login',(req,res)=>{
    var data={};
    data.crowd=[];
    data.gender='other';
    res.render('login',data);
});

app.get('/3.jpg', function(req, res){
res.sendFile(__dirname + '/views/3.jpg');
});

app.get('/style.css', function(req, res){
   res.sendFile(__dirname  + "/" + '/views/style.css');
});

app.post('/ds',urlencodedParser,(req,res)=>{
    console.log(req.body.username);
    console.log(req.body.Password); 
    req.checkBody('username','User name is invalid').isEmail();
    req.checkBody('Password','password is invalid').isLength({min:5,max:10});
    let errors=req.validationErrors();
    
    passport.serializeUser(function(user_id, done) {
    done(null, user_id);
});

    passport.deserializeUser(function(user_id, done) {
    done(null, user_id);
});
});
    app.listen(PORT,()=>{
    console.log('listening to port:' + PORT);
});
