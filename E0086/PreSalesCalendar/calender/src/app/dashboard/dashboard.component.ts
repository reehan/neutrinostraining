




import { EventData } from '../model/eventSchema';
import { eventService } from '../Services/eventservice';
import { Router,Routes, RouterModule } from '@angular/router';

import {
  Component,OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


import { FormGroup, FormControl,FormBuilder ,Validators} from '@angular/forms';

import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView
} from 'angular-calendar';



@Component({
  selector: 'app-dashboard',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit  {
 eventform:FormGroup;
 userModel:EventData;



  @ViewChild('modalContent')
  modalContent: TemplateRef<any>;

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: CalendarEvent;
  };



  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [

  ];


  constructor(private modal: NgbModal,private  fb:FormBuilder,private eventService:eventService,private router:Router)
  {
    this.userModel=new EventData();
    }
    postEvent(){
      console.log('profile function',this.userModel);
      console.log('registerform function',this.eventform.value);

      let username=this.eventform.value;




  this.eventService.Data(this.eventform.value,(arg1)=>{
console.log('arg',arg1);
localStorage.setItem(arg1,arg1.title);

let test = localStorage.getItem('arg1.title');

    this.router.navigate(['dashboard']);

  });
  }







  ngOnInit() {

    this.eventform = this.fb.group({
              title: [''],
              start: [''],
              end:['']

          });
    }
    addEvent(): void {
      this.events.push({
        title: 'New event',
        start: startOfDay(new Date()),
        end: endOfDay(new Date()),
        draggable: true,
        resizable: {
          beforeStart: true,
          afterEnd: true
        }
      });
      this.refresh.next();
    }



}






