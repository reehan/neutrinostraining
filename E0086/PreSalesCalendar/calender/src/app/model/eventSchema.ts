export class EventData{
  title:string;
  start:string;
  end:string;

  constructor(values: Object = {})
   {
      Object.assign(this, values);
   }

};
