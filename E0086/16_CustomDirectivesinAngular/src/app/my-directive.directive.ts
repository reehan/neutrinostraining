import { Directive ,ElementRef,Renderer2,HostListener} from '@angular/core';

@Directive({
  selector: '[appMyDirective]'
})
export class MyDirectiveDirective {

  constructor(private elementRef: ElementRef, private renderer:
    Renderer2) { }
  ngOnInit(): void {
   
  }
  @HostListener('mouseenter') handleMouseEnter(eventData: Event) {
    this.renderer.setStyle(this.elementRef.nativeElement,
    'background-color', 'cyan');
    this.renderer.setStyle(this.elementRef.nativeElement,
      'color', 'white');
    }
    @HostListener('mouseleave') handleMouseExit(eventData: Event) {
      this.renderer.setStyle(this.elementRef.nativeElement,
      'background-color', 'pink');
      this.renderer.setStyle(this.elementRef.nativeElement,
        'color', 'white');
      }
}
