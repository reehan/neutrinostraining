import { MyDirectiveDirective } from './my-directive.directive';
import { ElementRef, Renderer2 } from '@angular/core';

describe('MyDirectiveDirective', () => {
  it('should create an instance', () => {
    const directive = new MyDirectiveDirective(ElementRef,Renderer2);
    expect(directive).toBeTruthy();
  });
});
