import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { PasswordValidator } from './shared/password.validator';
import { RegistrationService } from './registration.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  registrationForm: FormGroup;
   constructor(private fb: FormBuilder, private _registrationService: RegistrationService) { }

  ngOnInit() {
    this.registrationForm = this.fb.group({
      userName: ['', [Validators.required, Validators.minLength(3)]],
      password: ['',[ Validators.minLength(8),Validators.required]],
      confirmPassword: [''],
      email: ['',[Validators.email,Validators.required]],
      
    }, { validator: PasswordValidator });  
  }

  get userName() {
    return this.registrationForm.get('userName');
  }
  get password() {
    return this.registrationForm.get('password');
  }

  get email() {
    return this.registrationForm.get('email');
  }
  patchData() {
    this.registrationForm.patchValue({
      userName: 'Santosh',
      email:'santosh.rnjn13@gmail.com',
      password: 'asdfghjk',
      confirmPassword: 'asdfghjk'
    });
  }

  onSubmit() {
    console.log(this.registrationForm.value);
    this._registrationService.register(this.registrationForm.value)
      .subscribe(
        response => console.log('Success!', response),
        error => console.error('Error!', error)
      );
  }

}
