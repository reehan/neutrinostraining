import { Component, Input, OnInit } from '@angular/core';
import { ServiceService} from '../service/service.service';
@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.css']
})
export class ServerComponent implements OnInit {

  private serverDetails: ServiceService;

  @Input() serName:string;  
  
  constructor(private _serverDetailsService: ServiceService) {
    this.serverDetails = _serverDetailsService;
  }
  
  getServerName() {
  this.serverDetails.serverName=this.serName;
    return this.serverDetails.getName();
  }
  ngOnInit(){

  }

}
