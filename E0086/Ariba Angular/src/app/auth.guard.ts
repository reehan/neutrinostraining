import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private auth:AuthenticationService,private router: Router) { }

  canActivate(): boolean {
    if (this.auth.loggedIn()) {
      console.log('true');
      return true;
    } else {
      console.log('false') ;           
      this.router.navigate(['/login']);
      return false;
    }
  }
}
