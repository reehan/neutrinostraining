import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RoutingModule, routingComponents } from './routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
import { AuthGuard } from './auth.guard';

import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { LoginComponent } from './login/login.component';
import { LandinComponent } from './landin/landin.component';
import { SliderComponent }  from './slider/slider.component';
import { RegisterComponent } from './register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { TokenInterceptorService } from './token-interceptor.service';




@NgModule({
  declarations: [AppComponent,WelcomeComponent, LoginComponent,routingComponents,
    LandinComponent,SliderComponent,RegisterComponent,ProfileComponent],
  imports: [BrowserModule,RoutingModule,FormsModule,HttpClientModule,NgbModule.forRoot()],
  providers: [AuthenticationService,AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ],
   bootstrap: [AppComponent]
})
export class AppModule {}
