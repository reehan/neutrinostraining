import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-landin',
  templateUrl: './landin.component.html',
  styleUrls: ['./landin.component.css']
})
export class LandinComponent implements OnInit {

  constructor(public auth:AuthenticationService) { }

  ngOnInit() {
  }

}
