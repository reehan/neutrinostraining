import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { LoginComponent } from './login/login.component';
import { LandinComponent } from './landin/landin.component';
import { SliderComponent } from './slider/slider.component';
import { RegisterComponent } from './register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
  { path: '', redirectTo: '/welcome', pathMatch:'full' },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'landin', component: LandinComponent ,canActivate :[AuthGuard]},
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent ,canActivate :[AuthGuard] }     
];
@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RoutingModule { }
export const routingComponents = [WelcomeComponent,LoginComponent,SliderComponent,
  LandinComponent,RegisterComponent,ProfileComponent]