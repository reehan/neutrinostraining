import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})


export class RegisterComponent implements OnInit {
   
 registerData: any={}
 
  constructor(private auth:AuthenticationService,private router: Router) { }

  ngOnInit() {

  }
  registerUser() {
    this.auth.registerUser(this.registerData)
    .subscribe (
      res => {
        localStorage.setItem('token', res.token)
        this.router.navigate(['/login'])
        } ,
      err => console.log(err)
  )
}
  

}
