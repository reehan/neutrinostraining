const mongoose = require('mongoose');

var userSchema = mongoose.Schema({
	email: String,
	password: String,
	username:String,
	company:String,
	fname:String,
	lname:String,
});
const newLocal = 'user';
var userData = mongoose.model(newLocal, userSchema,'usercollection');
module.exports = userData;