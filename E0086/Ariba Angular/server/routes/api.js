const express = require('express');
const router=express.Router();
const mongoose=require('mongoose');
const jwt = require('jsonwebtoken');

const db='mongodb://localhost/aribaNetwork'
const User = require ('../models/user');


mongoose.connect(db,err=>{
    if(err){
        console.error("Error!"+err);
    }else{
        console.log("Connected to MongoDB");
    }
});

router.get('/',(req,res)=>{
    res.send("From API Route");
});

router.post('/register', (req, res) => {
    let userData = req.body;
    let user = new User(userData)
    user.save((err, registeredUser) => {
      if (err) {
        console.log(err)      
      } else {
        let payload = {subject: registeredUser._id};
                let token = jwt.sign(payload, 'secretKey');
                res.status(200).send({token});  
                // res.status(200).send(registeredUser)
      }
    })
  })


  router.post('/login', (req, res) => {
    let userData = req.body;
    User.findOne({username: userData.username}, (err, user) => {
        if (err) {
            console.log(err);
        }
        else{
            if(!user){
                res.status(401).send('Invalid username');
            }else if ( user.password !== userData.password){
                res.status(401).send('Invalid Password');
            }else{
                let payload = {subject: user._id};
                let token = jwt.sign(payload, 'secretKey');
                res.status(200).send({token});
                //res.status(200).send(user);
            }
        }

    });

    router.get('/landin',(req,res)=>{
        
    });

   
   
  })

module.exports=router;