
var express = require('express');

var app = express();

app.set("view engine",'pug')

app.get('/register', function(req, res){	
   res.sendFile(__dirname+'/views/register.pug');
   res.render('register.pug');
});

app.get('/', function(req, res){	
   res.sendFile(__dirname+'/views/login.pug');
   res.render('login.pug');
});


app.get('/style.css', function(req, res){
   res.sendFile(__dirname  + "/" + '/views/style.css');
});

app.get('/3.jpg', function(req, res){
   res.sendFile(__dirname  + "/" + '/views/3.jpg');
});

app.listen(3020);
console.log('listening to port 3020');





