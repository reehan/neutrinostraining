const express = require('express');
const ejs = require('ejs');
const app = express();
const session = require('express-session');
app.use(session({ resave: false, saveUninitialized: true, secret: 'Neutrinos' }));
//const session =require('express-session');
const port = 3060;
app.set('view engine', 'ejs');
app.use(express.static('Public'));


var route = require('./Routing/router');
app.use('/', route);
app.listen(port);
console.log('listening to:' + port);