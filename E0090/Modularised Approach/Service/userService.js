const userDao = require('../Dao/Dao');
const nodemailer = require('nodemailer');
var registerIfNewUser = async function (userData) {
	var returnValue = {};
	console.log(userData.userName);
	var dbResult = await userDao.find(userData.userName);
	console.log('dbResult in registerIfNewUser:', dbResult);


	if (dbResult.found === false) {
		await userDao.save(userData);
		let transporter = nodemailer.createTransport({
			host: 'smtp.gmail.com',
			port: 465,
			secure: true,
			auth: {
				user: 'aribanetwork13@gmail.com',
				pass: 'ariba234'
			}
		});

		let mailOptions = {
			from: '<aribanetwork13@gmail.com>',
			to: userData.userEmail,
			subject: 'Ariba Registration',
			text: 'Thank you For registering with Ariba'

		};

		transporter.sendMail(mailOptions, (error, info) => {
			if (error) {
				return console.log(error);
			}
			console.log('Message %s sent: %s', info.messageId, info.response);
		});
		returnValue.saved = true;
	}
	else if (dbResult.found === true) {
		returnValue.saved = false;
	}
	console.log('return value in Service', returnValue)
	return returnValue;
}
var findUser = async function (username) {
	var user = await userDao.find(username);
	if (user) {
		console.log(user);
		return user.userObj;
	}
	throw new Error('User data Doesnt exists');
}
var findUserAndUpdate = async function (username) {
	var userDB = await userDao.find(username);
	await userDao.updateData(userDB.userObj);

}
var loginOnValidation = async function (userData) {
	var returnValid = {};
	var dbResponse = await userDao.find(userData.userName);
	console.log(dbResponse);
	if (dbResponse.found === true) {
	returnValid.exists = true;
		var valid = await userDao.passwordValid(userData);
		console.log(valid);
		if (valid) {
			returnValid.Valid = true;
			returnValid.Obj = dbResponse.userObj;
		}
		else {
			throw new Error('User data Doesnt exists');
		}

	}
	else {
		throw new Error('User data Doesnt exists');
	}
	return returnValid;
}

userService = {};
userService.registerIfNewUser = registerIfNewUser;
userService.loginOnValidation = loginOnValidation;
userService.findUser = findUser;
userService.findUserAndUpdate = findUserAndUpdate;
module.exports = userService;



