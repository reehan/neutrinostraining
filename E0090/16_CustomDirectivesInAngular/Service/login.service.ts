import { Injectable } from '@angular/core';
import { Http} from "@angular/http";
@Injectable({
  providedIn: 'root'
})
export class LoginService {
 constructor(private http:Http) { }
postUserData(userData:any) {
   return this.http.post('http://localhost:3000/login', userData)
    .subscribe(data => console.log(data));
    
  }

}
