import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { loginFormComponent } from './Login/login.component';
import{ LoginService } from './Service/login.service';
import { HttpModule } from '../../node_modules/@angular/http';
@NgModule({
  declarations: [
    AppComponent,
   loginFormComponent
   
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule
  ],
  providers: [LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }
