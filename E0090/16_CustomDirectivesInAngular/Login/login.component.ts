import { Component , OnInit , Input } from '@angular/core';
import { FormGroup , FormBuilder , Validators } from '@angular/forms';
import { userData } from '../UserObject/userData';
import{ LoginService } from '../Service/login.service';
@Component({
	selector:'app-form',
	templateUrl:'./login.component.html',
	styleUrls:['./login.component.css']
})
export class loginFormComponent implements OnInit {
  loginForm: FormGroup;
  submit = false;
  model:userData;
  constructor(private fb: FormBuilder,private loginService:LoginService) 
  {
this.model=new userData();
  }
  logData(){
    console.log(this.model);
    this.loginService.postUserData(this.model);

  }
   ngOnInit() {
        this.loginForm = this.fb.group({
            userName: ['', Validators.required],
            
            userPassword: ['', [Validators.required, Validators.minLength(8)]]
          
        });
      }
get logForm() { 
return this.loginForm.controls;
 }

    onSubmit() {
        this.submit = true;

       
        if (this.loginForm.invalid) {
            return;
        }

       
    }
}