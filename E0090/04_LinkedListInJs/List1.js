var Person = function(name, age, state)
{
    this.name = name;
    this.age = age;
    this.state = state;
  
};
//Objects of type Person
var p=new Person("Nan",16,"Karnataka");
var p1=new Person("Achu",20,"Mp");
var p2=new Person("Abc",22,"Bp");


var createLinkedList = (function() {

    var index = 0;
  var list=[]; //Empty list
   list.push(p);
 
 list.push(p2);
  list.push(5);
  list.push(7);
  
        
       var  l = list.length;

    return {
        //Function next iterates through List elements and Display value based on index
        next: function() {
            
            if (!this.hasNext()) {
                return null;
            }
            element=list[index];
             index ++;
          return element;
            
        },
        //hasNext() checks if there is element in list
        hasNext: function() {
            return index < l;
        }
    }

}());
//Storing the created list in temp variable for iterating
var personLinkedList = createLinkedList;

while(personLinkedList.hasNext())
  {
console.log(personLinkedList.next()); //Display the list elements one aafter other
  }
