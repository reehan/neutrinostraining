var person=function(arg1,arg2){
  

this.name=arg1;
this.age=arg2;
}; 
person.prototype.displayDetails= function()
{console.log('Name:' + this.name);
 console.log('Age:' + this.age);
  };
var n1=new person("Shaik",21);
var n2=new person("Sampreeth",22);
var n3=new person("shank",23);

function Node(value, next) {
  this.value = value;
  this.next = next || null;
}

function LinkedList() {
  this.first = null;
  this.last = null;
  this.size = 0;
}

LinkedList.prototype.iterate = function(callback) {
  var current = this.first;
    
  while (current !== null) {
    callback.call(this, current);
    current = current.next; 
  }
}
LinkedList.prototype.print = function() {
  var string = '';
  var current = this.head;
  while(current) {
    string += current.value + ' ';
    current = current.next;
  }
  console.log(string.trim());
};




LinkedList.prototype.append = function(value) {
    
    var node = new Node(value);
	
    if (this.size === 0) {
        this.first = node;
    } else {
        this.last.next = node;
    }

    // node.next = null;
    this.last = node;
    this.size++;
    return this;
}

function Iterator(list) {
  this.list = list;
  this.index = list.first;
}

Iterator.prototype.hasNext = function() {
  return this.index !== null && this.index.next !== null;
}

Iterator.prototype.next = function() {
  var nextNode = this.index.next;
  this.index = nextNode;
  return this.index;
}

var list = new LinkedList();

list.append('first');
list.append('second');
list.append('third');
list.print();

var i = new Iterator(list);

while(i.hasNext()) {
    i.next();
}


var personLinkedList = LinkedList();

do{
  personLinkedList = personLinkedList.next;
  personLinkedList.displayDetails();
}while(personLinkedList.hasNext());

do{
  personLinkedList = personLinkedList.previous();
  personLinkedList.displayDetails();
}while(personLinkedList.hasPrevious());var Person = function(name, age, state){
  this.name = name;
  this.age = age;
  this.state = state;
}