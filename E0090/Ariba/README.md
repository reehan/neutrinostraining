# Ariba Services

In this project we did create a ariba page ,where user can register,on registering Mail will be sent to provided gmail Id.

## Getting Started
Ariba Services a B2B network where one can register and provide details about their supply information.

### Prerequisites

we need to install certain npm packages as below:

```

npm install ejs --save
npm install express --save
npm install mongoose --save
npm install nodemailer --save
npm install body-parser --save
npm install express-session --save

```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
npm install ejs --save
```

And repeat

```
all packages required for validations.
```

End with an example of getting some data out of the system or using it for a little demo


### Running the application



```
nodemon app.js
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```





## Built With

* NodeJs and NodeJS frameworks
* HTML5,Bootstrap,JavaScript
* MongoDB

## Contributing

Write here how one can contribute back to your project:
* email:shashank16aug19@gmail.com
* gitId:ShashankHegde18

## Versioning

ariba V 0.0

## Authors

* **Front End** - *Done by Suraj,sontosh,shashank* - 
* **Backend Validation** - *Done by Shashank,Santosh,Suraj* - 

## License

This project is licensed under the GNU GPL License

## Acknowledgments

* NodeJS pakt textbook,

*Codeevolution Tutorials
* 
