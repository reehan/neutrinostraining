const express = require('express');
const bodyParser = require('body-parser');
const userService = require('../Service/userService');
const checkSignedIn = require('../SessionHandler');


var urlencodedParser = bodyParser.urlencoded({ extended: false });
var JSONParser = bodyParser.json()
const app = express();

var router = express.Router();

/*router.route('/login')
	.get(function (req, res) {
		msg1 = '';
		res.send({ msg: msg1 });
	});
router.route('/register')
	.get(function (req, res) {
		msg2 = '';
        res.send({ msg: msg2 });

	});*/
router.route('/invoice')
	.get(checkSignedIn, function (req, res) {
		userService.findUser(req.session.username).then(DBresponse => {
			res.send( { name: DBresponse.userName, mail: DBresponse.userEmail, compName: DBresponse.userCompName, Id: DBresponse.userID });
		});
	});
router.route('/invoicedir')
	.get(checkSignedIn, function (req, res) {
		userService.findUser(req.session.username).then(DBresponse => {
			res.send({ name: DBresponse.userName, mail: DBresponse.userEmail, compName: DBresponse.userCompName, Id: DBresponse.userID });
		});
	});
router.route('/logout')
	.get(function (req, res) {
		console.log(req.query.token);

		userService.findUserAndUpdate(req.query.token).then(resp => {
			console.log( 'in logout',resp)
		})

		req.session.destroy(function () {
			res.send({msg:'loged out',redirect:true})
		});
	})

router.route('/landing')
	.get(checkSignedIn, function (req, res) {
		userService.findUser(req.session.username).then(DBresponse => {
			console.log("in db",DBresponse.userName);
			//res.send({msg:'hello'});
			res.send({ name:DBresponse.userName,mail:DBresponse.userEmail,compName: DBresponse.userCompName, Id: DBresponse.userID, login: DBresponse.logedIn });

		});

	})
router.route('/profile')
	.get(checkSignedIn, function (req, res) {
		userService.findUser(req.session.username).then(DBresponse => {
			res.send( { compName: DBresponse.userCompName, Id: DBresponse.userID });

		});

	})
router.route('/register')
	.post(JSONParser, function (req, res) {
		var userData = {};
		console.log("Req userData in api-router",req.userData);
		console.log(" req body on api-router",req.body);
		userData.userName = req.body.userName;
		userData.userPassword = req.body.userPassword;
		userData.userCompName = req.body.userCompName;
		userData.userEmail = req.body.userEmail;
		console.log(userData);
		userService.registerIfNewUser(userData).then((result) => {
			console.log('router register, userService result', result);
			if (result.saved) {
				res.send({msg:'userdata object',redirect:true});
				// redirect to login page
			}
			else {
				res.send({ msg: 'User Name Exists', redirect:false });
				// user already exist
			}
		},
			(error) => {
				console.log(error);
			});
	});
router.route('/login')
	.post(JSONParser, function (req, res) {
		var loginUserData = {};
		loginUserData.userName = req.body.userName;
		loginUserData.userPassword = req.body.userPassword;
		console.log(req.headers);
		userService.loginOnValidation(loginUserData).then((result) => {

console.log('in login ',result.Obj);

			req.session.username = result.Obj.userName;
			res.send({msg:'landing page',
				redirect:true,
				username:result.Obj.userName,
				userEmail:result.Obj.userEmail,
				userCompName:result.Obj.userCompName,
				userLogin:result.Obj.logedIn,
				userid:result.Obj.userID,
				token:result.Obj._id
			});
			console.log('in login',result.Obj.userName);




		}, (error) => {
			console.log(error);
			res.send({ msg: 'You have entered username or Password Wrong!Please Try Again',redirect:false });
		})
	})

module.exports = router;
/*username:result.Obj.userName,
				userEmail:result.Obj.userEmail,
				userCompName:result.Obj.userCompName,
				userLogin:result.Obj.logedIn,
				userid:result.Obj.userID,*/