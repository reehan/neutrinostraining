import { Component, OnInit } from '@angular/core';
import {CarouselModule} from "angular2-carousel";
import { FormGroup, FormControl,FormBuilder ,Validators} from '@angular/forms';
import { userLoginData } from '../userData/userLoginData';
import{ loginService } from '../Service/loginService';
import { Router,Routes, RouterModule } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
messege='';
 loginForm:FormGroup;
model:userLoginData;
  constructor(private fb: FormBuilder,private loginService:loginService,private router:Router) 
  {
this.model=new userLoginData();
  }
  loginData(){
    console.log('logincomponent.loginData', this.model, this.loginForm)
    this.loginService.postUserLoginData(this.model,(arg)=>{
    console.log('Loginservice.', arg);
    console.log('Loginservice. :response', arg);

    var responseObj = arg;
    let username='userName';
    let usermail='userEmail';
    let usercompname='userCompName';
     let userLogin='userLoginTime';
    let userId='userID';
    let token='token';
    localStorage.setItem(username,arg.username);
    localStorage.setItem(usermail,arg.userEmail);
    localStorage.setItem(usercompname,arg.userCompName);
    localStorage.setItem(userLogin,arg.userLogin);
    localStorage.setItem(userId,arg.userid);
    localStorage.setItem(token,arg.token);
    console.log(localStorage.getItem('token'))
    console.log(localStorage.getItem(username));
    console.log('Loginservice. :response as JSON', responseObj.redirect);
  
    if(responseObj.redirect===false)
           { this.messege=responseObj.msg;
            }
            else if(responseObj.redirect===true){
           this.router.navigate(['landing']);
            }

    });


    }

  ngOnInit() {
  
  document.body.classList.add('bg-img');
  this.loginForm = this.fb.group({
            userName: ['', Validators.required],
            userPassword: ['', Validators.required]
           
        });
  }

}
