import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { LandingComponent } from './landing/landing.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuard } from './auth.guard';
const routes: Routes = [
  {
    path: '',
    component: IndexComponent
  },
  {
    path: 'posts',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
 {
    path: 'landing',
    canActivate: [AuthGuard],
    component: LandingComponent
  },
  {
  path:'profile',
  canActivate: [AuthGuard],
  component:ProfileComponent
  }

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
