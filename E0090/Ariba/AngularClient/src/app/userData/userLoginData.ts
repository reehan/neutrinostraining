export class userLoginData
{
    userName:string;
    userPassword:string;
    
    constructor(values: Object = {})
     {
        Object.assign(this, values);
     }

}