import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class registerService {
 constructor(private http:HttpClient) { }
postUserData(userData:any,callback) {
   console.log('registerService.postUserData:Post being called');
   return this.http.post('http://localhost:3060/api/register', userData)
    .subscribe((data) => {console.log(data)
    callback(data,userData);
    
    });
    
  }

}
