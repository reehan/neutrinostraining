import { Component, OnInit } from '@angular/core';
import { Router,Routes, RouterModule } from '@angular/router';
import {ProfileService} from '../Service/profileService';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(private router:Router,private profileService:ProfileService) { }
   companyName=localStorage.getItem('userCompName');
   userID=localStorage.getItem('userID');
   userName=localStorage.getItem('userName');
 token=localStorage.getItem('token');
  logUserOut()
  {
     this.profileService.loggedOutUser(this.token)
  localStorage.clear();
 this. router.navigate(['posts']);
  }

  ngOnInit() {
  }

}
