import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,FormBuilder ,Validators} from '@angular/forms';
import { userData } from '../userData/userData';
import{ registerService } from '../Service/registerService';
import { Router,Routes, RouterModule } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
   
    messege='';
 registrationForm:FormGroup;
model:userData;
constructor(private fb: FormBuilder,private registerService:registerService,private router:Router) 
  {
this.model=new userData();
  }
   registerData(){
    console.log('registercomponent.registerData', this.model, this.registrationForm)
    this.registerService.postUserData(this.model,(arg)=>{
    console.log('Regsiterservice.', arg);
   
    var responseObj = arg;
    console.log('Regsiterservice. :response as JSON', responseObj.redirect);
  
    if(responseObj.redirect===false)
           { this.messege=responseObj.msg;
            }
            else if(responseObj.redirect===true){
           this. router.navigate(['posts']);
            }

    });

    }
    
    

  ngOnInit() { 
  this.registrationForm = this.fb.group({
            userName: ['', Validators.required],
            userPassword: ['', Validators.required],
            userCompName :[''],
            userEmail:['']
        });
  }

}
