function Node(data) {
    this.data = data;           //Node of tree with data
    this.parent = null;
    this.children = [];
}
function Tree(data) {
    var node = new Node(data);    //passing data value to node of tree
    this.root = node;
}
 
Tree.prototype.traverseDF = function(callback) {
 //Traversing the Node in DF
    
    (function recurse(currentNode) {
        
        for (var i = 0, length = currentNode.children.length; i < length; i++) {
            
            recurse(currentNode.children[i]); //Recursively calling the function to traverse nodes in DF
        }
 
        
        callback(currentNode);  
 
        
    })(this.root);
 
};
//Creating the Tree
var tree = new Tree(1);
 
tree._root.children.push(new Node(2));
tree._root.children[0].parent = tree;
 
tree._root.children.push(new Node(3));
tree._root.children[1].parent = tree;
 
tree._root.children.push(new Node(4));
tree._root.children[2].parent = tree;
 
tree._root.children[0].children.push(new Node(5));
tree._root.children[0].children[0].parent = tree._root.children[0];
 
tree._root.children[0].children.push(new Node(6));
tree._root.children[0].children[1].parent = tree._root.children[0];
 
tree._root.children[2].children.push(new Node(7));
tree._root.children[2].children[0].parent = tree._root.children[2];
 tree.traverseDF(function(node) {  //Displaying the data of tree
    console.log(node.data);
});
 