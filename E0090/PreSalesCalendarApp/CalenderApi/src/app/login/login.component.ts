import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl,FormBuilder ,Validators} from '@angular/forms';
import { userLoginData } from '../userData/userLoginData';
import{ loginService } from '../Service/loginService';
import { Router,Routes, RouterModule } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    messege='';
   loginForm:FormGroup;
   model:userLoginData;
  constructor(private fb: FormBuilder,private loginService:loginService,private router:Router) 
  {
this.model=new userLoginData();
  }
  loginData(){
    this.loginService.postUserLoginData(this.model,(arg)=>{
    console.log('Loginservice.', arg);
    console.log('Loginservice. :response', arg);
    let userName="user";
    var responseObj = arg;
     localStorage.setItem('user',responseObj.user);
  
    if(responseObj.redirect===false)
           { this.messege=responseObj.msg;
            }
            else if(responseObj.redirect===true){
               this.router.navigate(['events']);
            }

    });


    }

  ngOnInit() {
            this.loginForm = this.fb.group({
            userName: ['', Validators.required],
            userPassword: ['', Validators.required]
           
        });
  }

}

