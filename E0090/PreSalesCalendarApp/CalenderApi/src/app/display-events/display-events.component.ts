import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,FormBuilder ,Validators} from '@angular/forms';
import{ displayService } from '../Service/showEventService';
@Component({
  selector: 'app-display-events',
  templateUrl: './display-events.component.html',
  styleUrls: ['./display-events.component.css']
})
export class DisplayEventsComponent implements OnInit {

messege='';
a=JSON.parse(localStorage.getItem('event'));
eventsData=this.a;

  constructor(private displayService:displayService) { 

  }


  ngOnInit() {
    let token='token';
  let eventData="event";
  
localStorage.setItem('token',"12345");
for(let i=0; i<this.eventsData.length;i++)
    {
      var startTime = new Date(this.eventsData[i].eventStartTime).toString();
      var endTime = new Date(this.eventsData[i].eventEndTime).toString();
     
      this.eventsData[i].eventStartTime = startTime.split('G')[0];
      this.eventsData[i].eventEndTime = endTime.split('G')[0];
    }
    this.displayService.getUserDisplayData(token,(arg)=>{

     
         
        localStorage.setItem('event',JSON.stringify(arg.allEvents));
        


    });

  }

}
