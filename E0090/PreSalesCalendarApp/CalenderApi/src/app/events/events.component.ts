
import { Component,OnInit } from '@angular/core';
import { FormGroup, FormControl,FormBuilder ,Validators} from '@angular/forms';
import{ eventService } from '../Service/eventService';
import { Router,Routes, RouterModule } from '@angular/router';
@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class  EventsComponent implements OnInit  {
y='';
 z=[];
 x='';
  eventForm:FormGroup;
show=false;
  addEvent()
  { if(this.show===false)
     {
        this.show=true;
     }
     else
     {
     this.show=false;
    
     }

  }
    
  constructor(private fb: FormBuilder,private eventService:eventService,private router:Router) 
  {

  }
  eventData() {
   
  
  

    this.eventService.postUserEventData(this.eventForm.value,(arg)=>{
    console.log('EventService.', arg);
    console.log('Eventservice. :response', arg.eventDatas);
    
   
    
   
     
     
  
 
    var responseObj = arg;
    
             if(responseObj.redirect===false)
            {
            console.log('In Event component');
              
            }
            else if(responseObj.redirect===true){
            
            this.router.navigate(['']);

            }
           
        });

    }

  ngOnInit() {
  
            this.eventForm = this.fb.group({
            eventName: [''],
           
            eventStartTime:[''],
            eventEndTime:[''],
            
            avatar:['']
        });
  }

}

