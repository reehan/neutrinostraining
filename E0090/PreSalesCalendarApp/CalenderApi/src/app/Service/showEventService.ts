import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class displayService {
 constructor(private http:HttpClient) { }
getUserDisplayData(token,callback) {
   console.log('displayService.postUserEventData:Post being called');
   return this.http.get('http://localhost:3080/CalenderApi/showEvents?token=' + token)
    .subscribe((data) => {console.log('in showEventsservice',data)
    callback(data);
    
    });
    
  }
  

}
