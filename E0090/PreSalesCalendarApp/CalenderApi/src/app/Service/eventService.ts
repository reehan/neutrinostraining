import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class eventService {
 constructor(private http:HttpClient) { }
postUserEventData(eventData:any,callback) {
   console.log('eventService.postUserEventData:Post being called');
   return this.http.post('http://localhost:3080/CalenderApi/event', eventData)
    .subscribe((data) => {console.log('in service',data)
    callback(data);
    
    });
    
  }
  

}
