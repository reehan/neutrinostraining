import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA  } from '@angular/core';
import { TabsModule } from 'ngx-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { LoginComponent } from './login/login.component';
import { EventsComponent } from './events/events.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import{ loginService } from './Service/loginService';
import { ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap';
import{ eventService } from './Service/eventService';
import { DisplayEventsComponent } from './display-events/display-events.component';
import{ displayService } from './Service/showEventService';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    EventsComponent,
    DisplayEventsComponent
 
  ],
  imports: [
    BrowserModule,
    BsDatepickerModule.forRoot(),
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    TabsModule.forRoot(),
    MDBBootstrapModule.forRoot()
   
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [loginService,eventService,displayService],
  bootstrap: [AppComponent]
})
export class AppModule { }
