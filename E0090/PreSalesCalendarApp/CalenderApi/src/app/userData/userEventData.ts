export class userEventData
{
    eventName:string;
    eventTime:string;
    eventDate:string;
    
    constructor(values: Object = {})
     {
        Object.assign(this, values);
     }

}