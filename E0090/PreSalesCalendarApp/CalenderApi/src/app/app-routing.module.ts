import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './navbar/navbar.component';
import { EventsComponent } from './events/events.component';
import { DisplayEventsComponent } from './display-events/display-events.component';
import { AuthGuard } from './auth.guard';
const routes: Routes = [
  {
    path: '',
    component: NavbarComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
  path: 'events',
  canActivate: [AuthGuard],
  component:EventsComponent
  },
  {
  path:'showEvents',
  component:DisplayEventsComponent 
  }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
