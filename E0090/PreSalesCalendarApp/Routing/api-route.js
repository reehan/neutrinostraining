const express = require('express');
const bodyParser = require('body-parser');
const userService = require('../Service/userService');

const eventService=require('../Service/eventService');
const multer=require('multer');
var tc = require("timezonecomplete");   
const storage = multer.diskStorage({   
 destination:(req, file, callback)=>{        
 	callback(null, 'uploads/');   
 	 },    
 	 filename:(req, file, callback)=>{       
 	  callback(null, new Date().toISOString()+file.originalname);    
 	}   
 	});

const upload = multer({storage: storage});
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var JSONParser = bodyParser.json()
const app = express();

var route = express.Router();



route.route('/login')
	.post(JSONParser, function (req, res) {
		var loginUserData = {};
		console.log(req.body);
		loginUserData.userName = req.body.userName;
		loginUserData.userPassword = req.body.userPassword;
		console.log(req.headers);
		userService.loginOnValidation(loginUserData).then((result) => {
            req.session.username = result.Obj.userName;
           console.log(result);
			res.send({msg:'events Page',redirect:true,user:result.Obj.email});
			




		}, (error) => {
			console.log(error);
			res.send({ msg: 'You have entered username or Password Wrong!Please Try Again',redirect:false });
		})
	})
	route.route('/event')
	  .post(JSONParser,upload.single("avatar"),function(req,res){
	  	  var eventData={};
	  	  console.log(req.body);
	  	  eventData.eventName=req.body.eventName;
	  	  
	  	  eventData.eventStartTime=req.body.eventStartTime;
	  	 
	  	  eventData.eventEndTime=req.body.eventEndTime;
          eventData.eventFilePath=req.body.avatar;
        var start = new tc.DateTime(eventData.eventStartTime);
        var end = new tc.DateTime(eventData.eventEndTime);
        var duration = end.diff(start);

        console.log('in time',start);
        console.log("time difference in minutes",duration.minutes());
        console.log("time difference in hours",duration.hours()); 
	  	  eventService.saveEventData(eventData).then((result)=>{
	  	 	 
	  	 	if(result.saved==true){
	  	 	res.send({msg:'Save events',
	  	 		redirect:true,
	  	 		eventDatas:result.eventObject
	  	 	});
	  	 }
	  	 else
	  	 {
	  	 res.send({msg:'Event already exists',redirect:false});	
	  	 }
	  	 })
	  	 
	  })
	  route.route('/showEvents')
	  .get(function (req, res) {
       eventService.findEvents().then((result)=>
       {
                console.log('in findEvents db',result);
                res.send({msg:'db result',allEvents:result});
	  	  })
   });
module.exports = route;
