const express = require('express');
const ejs = require('ejs');
const app = express();
const cors=require('cors');

const session = require('express-session');
app.use(session({ resave: false, saveUninitialized: true, secret: 'Neutrinos' }));
//const session =require('express-session');
const port = 3080;
app.set('view engine', 'ejs');
app.use(express.static('Public'));
app.use(cors());


var route = require('./Routing/api-route');
app.use('/CalenderApi', route);
app.listen(port);
console.log('listening to:' + port);