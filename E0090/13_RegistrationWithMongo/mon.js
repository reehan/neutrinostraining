
var mong=require('mongoose');
const express=require('express');
const app=express();
const bodyParser=require('body-parser');
const session=require('express-session');
app.set('view engine','ejs');
const multer=require('multer');
const storage=multer.diskStorage({
	destination:(req,file,callback)=>{
		callback(null,'images/');
	},
	filename:(req,file,callback)=>{
		callback(null,file.originalname);
	}
});

var mParser=multer({storage:storage});
var urlencodedParser=bodyParser.urlencoded({extended:false});
mong.connect('mongodb://localhost/users');
var userSchema=mong.Schema({
	fname: String,
	lname: String,
	Email: String,
	password: String,
	image:String
});
var user=mong.model('users',userSchema);
app.get('/',function(req,res){
	res.render('register');
});

app.post('/landing',urlencodedParser,mParser.single("uProfile"),function(req,res){

	var nuser= new user({fname:req.body.uFirstName,
		lname:req.body.uLastName,
		Email:req.body.uemail,
		password:req.body.uPassword,
	     image:req.file.path});

	nuser.save(function(err,user){   /*Saving the user entry*/
		if(err)
			res.render('errPage',{message:"db error",type:'error'});
		else
			res.render('Success');
	});
});
app.listen(3050); 