import { Component,Input } from '@angular/core';
import { ServerService} from '../services/service.component';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html'

})
export class ServerComponent {

private serverDetails: ServerService;

@Input() serName:String;


constructor(private _serverDetailsService: ServerService) {
	this.serverDetails = _serverDetailsService;
}

getServerName() {
this.serverDetails.serverName=this.serName;
	return this.serverDetails.getName();
}

ngOnInit() {
}
  
}