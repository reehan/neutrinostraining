const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const session = require('express-session');
const mongoose = require('mongoose');
const passport = require('passport');
const validator = require('express-validator');
const ejs = require('ejs');
const multer = require('multer');

var app=express();


app.use(session({
    secret: 'neutrinos',
    resave: false,
    saveUninitialized: true,
    cookie: {secure:false } 
  }));

var router=express.Router();

var router=require('./route/router.js');


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use('/css',express.static('css'));
app.use('/js',express.static('js'));
app.use('/img',express.static('img'));

app.use(express.static('public'));
app.use('/uploads',express.static('uploads'))


app.use('/',router);

app.listen(3080, () => console.log('Server started listening on port 3080!'));




































