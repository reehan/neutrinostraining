# Project Title

SuiteCRM:
SuiteCRM is a free and open source Customer Relationship Management application. Open source CRM is often used as an alternative to proprietary CRM software from major corporations such as Salesforce and Microsoft Dynamics CRM applications. SuiteCRM is a software fork of the popular customer relationship management (CRM) system from SugarCRM.The SuiteCRM project has stated that every line of code released by the project is and will always be open source. SuiteCRM project is intended to be an enterprise-class open source alternative to proprietary alternatives.

Sales Force Automation MANAGE OPPORTUNITIES SuiteCRM enables your business to improve its performance with focus primarily on sales. MANAGE YOUR CONTACTS & LEEDS Organise your business contacts and record relevant information that will help your business create opportunities and leads. GENERATE QUOTES and MANAGE ORDERS Generate mass volumes of quotes taken from opportunities. Once the order is placed, your sales team can convert quote into an order. Right Customers. Right Time. Right Place.
    
SuiteCRM Benefits Quick to deployQuick to deploy OnDemand anywhere, at any time, from any device! You have no software to install. No Lock inNo Lock in Your application. Your data. You own it, the guarantors of continuous high quality service. 
## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

> express packages
  cookieParser packages
  session packages
  mongoose packages
  validator packages
  ejs  packages
  multer  packages
  body-parser packages.
  
### Installing

Install 'npm' and all required dependency packages mentioned above.

### Running the application

Step 1: Run the server using command: "node sui.js"
Step 2: Run the database using command: "mongo"
Step 3: Run the application in browser.

## Deployment

MEAN Technology.

## Built With

* HTML, CSS, Bootstrap, Node.js, Express.js , MongoDB. 


## Versioning

This is the initial version being released.

## Authors

* Vinay M** - *UI design work and back end* - 

## License

This project is licensed under the GNU GPL License

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
