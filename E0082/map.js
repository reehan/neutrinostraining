var map = new Map();

map.set("orange", 10);
map.set("apple", 5);
map.set("banana", 20);
map.set("cherry", 13);

map[Symbol.iterator] = function* () {
    yield* [...this.entries()].sort((a, b) => a[1] - b[1]);
}

for (let [key, value] of map) {     // get data sorted
    console.log(key + ' ' + value);
}

console.log([...map]);              // sorted order
//console.log([...map.entries()]);  


var keyObj = new Object();

var notnumObj = keyObj/0;
var notnumFun = keyObj/0;

var innerArray = [notnumObj, true, 'abc', 345, 789, notnumFun, 'true', false ];
var anotherInnerArray = [keyObj, 899.08, 12.89, keyObj];

var sortArray = [innerArray, 'zoo', 'doc', anotherInnerArray,map];

function sort(arrayToSort){
    var sortedArray = arrayToSort;
  for(let icounter in sortedArray){
   // console.log(key);
    let inArray=sortedArray[icounter];
    if(Array.isArray(inArray)){
      sort(inArray);
}
  //console.log(isArray);
}
  sortedArray.sort();
  return sortedArray;
}
//hi this is sorting
console.log(sort(sortArray));