const express = require('express');
const morgan = require('morgan')
const path = require('path');
const bodyParser = require('body-parser');
const multer = require('multer');
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');


var userService=require('../services/service');
var Events=require('../model/EventSchema');


var urlencodedParser = bodyParser.urlencoded({ extended: false });

var jsonParser = bodyParser.json()


var router=express.Router();


const storage = multer.diskStorage({
	destination: (req, file, callback)=>{
	callback(null, 'uploads/');
	},
	filename: function (req, file, callback) {
		callback(null, file.originalname);
	  }
	});

	var parse = multer({storage:storage});

	router.use('/uploads',express.static('uploads'))








router.route('/login')
 .post(jsonParser,function(req,res){

 	var loginUserData={};
 	loginUserData.ename1=req.body.uName;
	 loginUserData.mypass2=req.body.uPassword;
	 console.log('from login form',loginUserData);
 	userService.loginOnValidation(loginUserData).then((result)=>{
 		console.log('inLoginValidation',result);
 		if(result.exists)
 		{
 			if(result.Valid)
 			{
 				res.send({value:true});
 			}
 			else
 			{
 				res.send({value:false});
 			}
 		}
 		else
 		{
 			res.send({value:false});
 		}

 	})
 });


 router.route('/event')
.post(jsonParser,parse.single('Path'),function(req,res){
	   var User1={};

	console.log(User1);
	userService.registerIfNewUser1(User1).then((result) => {
        User1.title=req.body.title;
        User1.start=req.body.start;
		User1.end=req.body.end;
		User1.Path= req.body.Path;

		console.log('path data',User1.Path)

		userService.registerIfNewUser1(User1).then((result) => {
		console.log('router register, userService result' , result);
		if(result.saved){
			console.log('data',User1);
			res.send(User1);
			// redirect to login page
		}
		else{
			 res.send('dashboard');
			 // user already exist
		   }
	},
	(error)=>{
		console.log(error);
	});
});

  });

  router.route('/eventlist')
  .get(jsonParser,(req, res)=> {
	console.log('entered eventlist....',Events)

	Events.find({},function(err,dbevent){
		if(err){
			throw err;
		}
		else{
			console.log('event data',dbevent)
			res.send(dbevent);

		}

	});

  });

  router.route('/delete/:_id')
  .get(jsonParser,(req, res)=> {
	console.log('entered eventlist....',req.params._id)

	Events.deleteOne({_id:req.params._id},function(err,dbevent){
		if(err){
			throw err;
		}
		else{
			console.log('event data',dbevent)
			res.send(dbevent);

		}

	});

  });

  router.route('/send')
   .post(jsonParser,(req, res) => {
	var User2={};

	console.log(User2);
        User2.name=req.body.name;
        User2.company=req.body.company;
		User2.email=req.body.email;
		User2.phone= req.body.phone;
		User2.messgae= req.body.message;


 let transporter = nodemailer.createTransport({
			service:'gmail',
			secure: false,
			port :25,
			auth: {
				user:'calendarevent9999@gmail.com',
				pass:'Calendar99'
			},
			tls:{
				rejectUnauthorized:false
			}
		});
		let HelperOptions = {
			from: '"vinay" <calendarevent9999@gmail.com>',
			  to: req.body.email,
			  subject: 'Calendar',
			  text: req.body.message
		};

  transporter.sendMail(HelperOptions,(error,info)=>{
	  if(error){
		  return console.log(error);
	  }
	  console.log("the message was sent");
	  console.log(info);
  });

  });

  module.exports=router;
