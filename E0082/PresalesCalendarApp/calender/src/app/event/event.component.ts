import { taskService } from '../Services/taskservice';
import { DashboardComponent } from './../dashboard/dashboard.component';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable ,  Subject } from 'rxjs';


@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {
  title=JSON.parse(localStorage.getItem('eventlist'));
  eventname = this.title;
  deletevalue={};

  constructor( private taskService:taskService,private Http:HttpClient) {
  this.deleteEvent(this.deletevalue);
  }

  ngOnInit() {
    let token = 'token';
    let postevent = 'eventlist';

    localStorage.setItem('token','1234');



    this.taskService.Data(token,(arg1)=>{
              localStorage.setItem('eventlist',JSON.stringify(arg1))

  });

}
deleteEvent(title1){

  console.log('delete function',title1);

              console.log('entering Event list....',title1);

     return this.Http.get(`http://localhost:3060/api/delete/${title1}`,title1)
      .subscribe((data)=>{ console.log('database value',data);

              })


}
}
