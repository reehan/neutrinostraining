import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { DashboardstickyComponent } from './task/task.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EventComponent } from './event/event.component';
import { DisplayComponent } from './display/display.component';
import { AuthGuard } from './Authguard'


const routes: Routes = [

  {
    path: '',
    component: DashboardstickyComponent
  },
  {
    path: 'Index',
    component: IndexComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'event',
    component: EventComponent
  },
  {
    path: 'display',
    component: DisplayComponent
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
