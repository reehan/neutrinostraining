import { Injectable , Injector} from '@angular/core';
import {HttpInterceptor} from '@angular/common/http';
import { loginService } from './Services/loginservice';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {
 intercept(req, next) {
                       let tokenizedReq = req.clone({
                       headers: req.headers.set('Authorization', 'bearer ' +localStorage.getItem('token'))
 })
 return next.handle(tokenizedReq)
}
  constructor() { }
}

