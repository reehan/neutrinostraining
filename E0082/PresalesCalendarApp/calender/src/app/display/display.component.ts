import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,FormBuilder ,Validators} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { DisplayData } from '../model/displaySchema';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router,Routes, RouterModule } from '@angular/router';
import { displayService } from '../Services/displayService';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css']
})
export class DisplayComponent implements OnInit {
  displayform:FormGroup;
  userModel:DisplayData;
;
  constructor(private modal: NgbModal,private  fb:FormBuilder,private displayService:displayService,private router:Router)
  {
    this.userModel=new DisplayData();
    }
    displayevent(){
      console.log('profile function',this.userModel);
      console.log('registerform function',this.displayform.value);





  this.displayService.Data(this.displayform.value,(arg1)=>{
console.log('arg',arg1);



    this.router.navigate(['dashboard']);

    });
  }

  ngOnInit() {
    this.displayform = this.fb.group({
      name: [''],
      company: [''],
      email:[''],
      phone: [''],
      message:['']

  });
  }

}
