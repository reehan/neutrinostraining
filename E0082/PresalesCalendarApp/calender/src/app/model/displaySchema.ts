export class DisplayData{
  name:string;
  company:string;
  email:string;
  phone: String;
  message:String;

  constructor(values: Object = {})
   {
      Object.assign(this, values);
   }

};
