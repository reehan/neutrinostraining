export class EventData{
  title:string;
  start:string;
  end:string;
  Path: String;

  constructor(values: Object = {})
   {
      Object.assign(this, values);
   }

};
