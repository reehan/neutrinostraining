const express = require('express');
const ejs = require('ejs');
const PORT = 3020;

var kings = new Map();
kings.set('Vikramaditya','vikram');
kings.set('Akbar','akbar');
kings.set('Raja','rajakrishna');

var nava = new Map();
nava.set('Vikramaditya', ['Amarasimha', 'Dhanvantari', 'Ghatakarapara', 'Kalidasa', 'Kshapanaka', 'Shanku', 'Varahamihira', 'Vararuchi', 'Vetala-Bhatta']);
nava.set('Akbar', ['Abul-Fazl', 'Raja Todar Mal', 'Abdul Rahim Khan-I-Khana', 'Birbal', 'Mulla Do-Piyaza', 'Faizi', 'Fakir Aziao-Din', 'Tansen', 'Raja Man Singh']);
nava.set('Raja', ['Gopal Bhar', 'Bharatchandra Ray', 'Ramprasad Sen']);

const app = express();
app.set('view engine', 'ejs');

app.get('/', function(req, res)
{
	console.log('Request was made to '+req.url);
   res.render('login');
});

app.post('/ds', (req, res) => {
	var data='';
	req.on('data', function(chunk){
		data += chunk;
	});

	req.on('end', function(){
	console.log('Request was made to '+req.url);
	console.log('data '+data);

	data = data.split('&')
	let king = data[0].split('=');
	let password = data[1].split('=');
	king = king[1];
	password = password[1];
	console.log(king);
	if(kings.get(king) === password)
	{
		res.render('render.ejs', {kingName: king, ratna:nava.get(king)});
	}
    else
    {
        res.render("invalid user");
    }
	});

});

app.listen(PORT, () => {
    console.log('Listening to port:' + PORT);
});