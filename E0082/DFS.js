function Node(data) {
this.data = data;
this.parent = null;
this.children = [];
}
function Tree(data) {
var node = new Node(data);
this.root = node;
}
var DFS = function (value) {
var newTree = {};
newTree.value = value;
newTree.children = [];
extend(newTree, treeMethods);
return newTree;
};
var tree = new Tree(1);
tree.root.children.push(new Node(2));
tree.root.children[0].parent=tree;

tree.root.children.push(new Node(3));
tree.root.children[1].parent=tree;

tree.root.children.push(new Node(4));
tree.root.children[2].parent=tree;

tree.root.children[0].children.push(new Node(5));
tree.root.children[0].children[0].parent = tree.root.children[0];

tree.root.children[0].children.push(new Node(6));
tree.root.children[0].children[1].parent = tree.root.children[0];

tree.root.children[2].children.push(new Node(7));
tree.root.children[2].children[0].parent = tree.root.children[2];


console.log(tree);




