import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardstickyComponent } from './dashboardsticky.component';

describe('DashboardstickyComponent', () => {
  let component: DashboardstickyComponent;
  let fixture: ComponentFixture<DashboardstickyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardstickyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardstickyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
