import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import{ loginService } from '../Services/loginservice';





@Component({
  selector: 'app-dashboardsticky',
  templateUrl: './dashboardsticky.component.html',
  styleUrls: ['./dashboardsticky.component.css']
})
export class DashboardstickyComponent implements OnInit {
  constructor(private router:Router) { }

  ngOnInit() {
  }

  logout()
{
  localStorage.clear();
  this.router.navigate(['/login']);

}

}
