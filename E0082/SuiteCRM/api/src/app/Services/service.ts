import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
@Injectable({
  providedIn: 'root'
})
export class Service{

  constructor(private http:Http){ }
  postUser(User:any,callback){
    console.log('user',User);
    return this.http.post('http://localhost:3090/api/register',User)
    .subscribe((data)=>{ console.log(data)
    callback(data);
  });
  }
}
