import { AuthGuard } from './auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SuitefrontComponent } from './suitefront/suitefront.component';
import { IndexComponent } from './index/index.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { DashboardstickyComponent } from './dashboardsticky/dashboardsticky.component';



const routes: Routes = [
  {
    path: '',
    component: SuitefrontComponent
  },
  {
    path: 'posts',
    component: IndexComponent
  },
  {
    path: 'reg',
    component: RegisterComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'dashboard',
    canActivate: [AuthGuard],
    component: DashboardstickyComponent
  }


];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
