import { RouterModule } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { User } from '../model/UserSchema';
import { Service } from '../Services/service';

import { FormGroup, FormControl, Validators, FormBuilder }  from '@angular/forms';
import { Router } from '@angular/router';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  msg1='';
  registerform: FormGroup;
  userModel: User;
  constructor(private fb:FormBuilder,private regService:Service,private router:Router) {
    this.userModel=new User();
   }

     postProfile(){
      console.log('profile function',this.userModel);
      console.log('registerform function',this.registerform.value);

  this.regService.postUser(this.userModel,(arg1)=>{

    console.log('function',arg1);
    this.router.navigate(['login']);

  });
  }
ngOnInit(){
  this.registerform   = this.fb.group({
    username :[''],
    name:[''],
    email:[''],
    confirmationemail:[''],
    password:[''],
    confirmationPassword:['']
  })
}


}
