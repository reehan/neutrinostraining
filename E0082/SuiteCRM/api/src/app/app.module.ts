import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Service } from './Services/service';
import { loginService } from './Services/loginservice';
import { TokenInterceptorService } from './token-interceptor.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ApiRouteComponent } from './api-route/api-route.component';
import { SuitefrontComponent } from './suitefront/suitefront.component';
import { IndexComponent } from './index/index.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { FormsModule }   from '@angular/forms';
import { ReactiveFormsModule }   from '@angular/forms';
import { AuthGuard } from './auth.guard';


import { DashboardstickyComponent } from './dashboardsticky/dashboardsticky.component';




@NgModule({
  declarations: [
    AppComponent,
    ApiRouteComponent,
    SuitefrontComponent,
    IndexComponent,
    RegisterComponent,
    LoginComponent,
    DashboardstickyComponent


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule
    ],

  providers: [Service,loginService,AuthGuard,{ provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
