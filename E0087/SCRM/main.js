const express = require('express');
const session = require('express-session');
const app=express();
const ejs=require('ejs');
app.set('view engine','ejs');

app.use(session({
    secret: 'neutrinos',
    resave: false,
    saveUninitialized: true,
    cookie: {secure:false } 
  }));



const PORT = 3050;
app.use(express.static('Public'));



var route=require('./Route/route');
app.use('/',route);
app.listen(PORT);
console.log('listening to port '+ PORT);