const express =require('express');
const ejs=require('ejs');
const service=require('../Service/service');
const bodyParser=require('body-parser');
var eventservice = require('../Service/eventservice');
const multer=require('multer');

const storage = multer.diskStorage({
	destination:(req,file,callback)=>{
		callback(null,'uploads/');
	},
	filename:(req,file,callback)=>{
		callback(null, new Date().toISOString()+file.originalname)
	}
});

const upload = multer({storage:storage});
var urlencodedParser = bodyParser.urlencoded({ extended:false });
var jsonParser=bodyParser.json();
const app=express();

var router=express.Router();


router.route('/login')
	.post(jsonParser,(req,res)=>{
		var userLoginData={};
		console.log(req.body);
		userLoginData.userName=req.body.userName;
		userLoginData.userPassword=req.body.userPassword;
		console.log('userLoginData',userLoginData)
		service.loginOnValidation(userLoginData).then((result)=>{
			console.log('result',result)
			if(result.userExist){
			res.send({msg:'redirect to index',redirect:true});}
			else{
				res.send({msg:'OOPS, something wrong. Please try again',redirect:false});
			}
		},(error)=>{
			console.log(error);
			// res.send({msg:'wrong user name and password',redirect:false});
		})
	});

	router.route('/event')
		.post(jsonParser,upload.single("file"),function(req,res){
			var userEventData={};
			console.log(req.body);
			userEventData.eventName=req.body.eventName;
			userEventData.eventDesc=req.body.eventDesc;
			userEventData.eventDate=req.body.eventDate;
			userEventData.eventFile=req.body.eventFile;
			console.log('userEventData',userEventData)
			eventservice.saveEvent(userEventData).then((result)=>{
				console.log('result',result)
				if(result.saved){
				res.send({msg:'redirect to index',redirect:true});}
				else{
					res.send({msg:'OOPS, something wrong. Please try again',redirect:false});
				}
			},(error)=>{
				console.log(error);
				// res.send({msg:'wrong user name and password',redirect:false});
			})
		});



	module.exports=router;
