const express=require('express');
const app=express();
const ejs=require('ejs');
const session=require('express-session');
const cors=require('cors');
app.use(session({ 
    resave: false, 
    saveUninitialized: true, 
    secret: 'Neutrinos' 
}));
app.use(cors());
var router=require('./Route/route');
app.use('/calender',router);
app.use(express.static('Public'));
app.set('view engine','ejs');




app.listen(3050);
console.log('port 3050 running');
