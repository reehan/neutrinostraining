const eventdao=require('../Dao/eventDao');
var saveEvent=async function(eventData){
	var returnValue={};
		var dbResult=await eventdao.find(eventData.eventName);
    console.log('db result in saveEventdata',dbResult);
    if(dbResult.exist === false){
      await eventdao.save(eventData);
      returnValue.saved = true;
      returnValue.eventobject = eventData;
    }
    else {
      returnValue.saved=false;
      returnValue.eventObj = dbResult.userdata;
    }
    return returnValue;
  }


var eventservice={};
eventservice.saveEvent=saveEvent;
module.exports=eventservice;
