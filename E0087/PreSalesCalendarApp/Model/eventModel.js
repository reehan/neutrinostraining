const mongoose=require('mongoose');
mongoose.connect('mongodb://localhost/suitecrm');


var EventSchema=mongoose.Schema({
  eventName:String,
  eventDesc:String,
  eventDate:String,
  eventFile:String
});


var eventModel=mongoose.model('eventcollections',EventSchema);

module.exports=eventModel;
