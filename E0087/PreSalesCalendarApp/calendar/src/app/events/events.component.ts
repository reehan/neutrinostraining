import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { eventDetail } from '../userData/eventDetail';
import { eventservice } from '../services/eventservice';
import { Router,Routes, RouterModule } from '@angular/router';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
  message='';
  eventPage:FormGroup;
  model:eventDetail;

  constructor(private formBuilder:FormBuilder,private eventservice:eventservice,private router:Router) {
  this.model = new eventDetail(); }

event(){
  this.eventservice.posteventDetails(this.model,(arg)=>{
    console.log('arg in event ts',arg);
    var responseObj=arg;
    if(responseObj.redirect===false)
    {
      this.message=responseObj.msg;
    }
    else if(responseObj.redirect===true){
      this.router.navigate(['']);
    }
  })
}

  ngOnInit() {
    this.eventPage=this.formBuilder.group({
      eventName:['',Validators.required],
      eventDesc:['',Validators.required],
      eventDate:['',Validators.required],
      eventFile:['',Validators.required]
    });
  }

}
