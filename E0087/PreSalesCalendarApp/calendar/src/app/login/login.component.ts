import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { loginDetail } from '../userData/loginDetail';
import { loginservice } from '../services/loginservice';
import { Router,Routes, RouterModule } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
message='';
loginPage:FormGroup;
submitted=false;
model:loginDetail;

  constructor(private formBuilder:FormBuilder,private loginservice:loginservice,private router:Router) {

this.model=new loginDetail();
   }
   login(){
   this.loginservice.postloginDetails(this.model,(arg)=>{
   console.log('arg in login ts',arg);

   var responseObj=arg;
if(responseObj.redirect===false)
{
	this.message=responseObj.msg;
}
else if(responseObj.redirect===true){
	this.router.navigate(['event']);
}

   })
   }

  ngOnInit() {
this.loginPage=this.formBuilder.group({
	userName:['',Validators.required],
	userPassword:['',[Validators.required,Validators.minLength(6)]]
});
  }
get f()
	{return this.loginPage.controls;}

onSubmit(){
	this.submitted=true;

	if(this.loginPage.invalid) {
	return;
	}
}

}
