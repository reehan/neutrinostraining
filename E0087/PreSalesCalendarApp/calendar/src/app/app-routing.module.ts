import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { LoginComponent } from './login/login.component';
import { EventsComponent } from './events/events.component';


const routes: Routes = [{
	path:'',
	component:IndexComponent
},
{
	path:'login',
	component:LoginComponent
},{
	path:'event',
	component:EventsComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
