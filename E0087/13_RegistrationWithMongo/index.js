const ejs = require('ejs');
const express = require('express');
const bodyParser = require('body-parser');
const validator = require('express-validator');
const session = require('express-session');
const mongoose = require('mongoose');
const passport = require('passport');
const multer = require('multer');


const app=express();
const PORT = 3020;

app.set('view engine', 'ejs');

 const storage = multer.diskStorage({
destination: (req, file, callback)=>{
callback(null, 'uploads/');
},
filename: function (req, file, callback) {
    callback(null, file.originalname);
  }
});


var parse = multer({storage:storage});

let logger=(req,res,next)=>{
  console.log('Request received from URL:' + req.url);
    next();
};

var router=express.Router();

router.get('/',function(req,res){
res.send('About');
});

app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
}))


var urlencodedParser = bodyParser.urlencoded({ extended: false });
app.use(validator());

app.use(passport.initialize());
app.use(passport.session());

app.use('/css',express.static('css'));
app.use('/js',express.static('js'));

mongoose.connect('mongodb://localhost/users100');

var userSchema=mongoose.Schema({
    myname2:String,
    myemail2:String,
    myconemail2:String,
    mypass2:String,
    photoPath: String
    
});
const user=mongoose.model("person100",userSchema);

app.post('/register',urlencodedParser,parse.single('userImage'),(req,res)=>{
    
     req.checkBody('myname','User name is invalid').isEmail();
     req.checkBody('myemail','password is invalid').isLength({min:5,max:10});
     req.checkBody('myconemail','password is invalid').isLength({min:5,max:10});
     req.checkBody('mypass','password is invalid').isLength({min:5,max:10});
     req.checkBody('userImage','password is invalid');

     console.log(req.file);
    
     let errors=req.validationErrors();
      


var newuser = new user({
    myname2: req.body.myname,
    myemail2: req.body.myemail,
    myconemail2: req.body.myconemail,
    mypass2: req.body.mypass,
    photoPath: req.file.path
  
});
    console.log(newuser);
newuser.save(function(err, user){
if(err)
res.render('errPage', {message: "Database error", type:
"error"});
else
res.send('User data is created');
});
    
});
         
        


app.get('/',(req,res)=>{
    var data={};
    data.crowd=[];
    data.gender='other';
    res.render('form',data);
});

app.get('/login',(req,res)=>{
    var data={};
    data.crowd=[];
    data.gender='other';
    res.render('login',data);
});


    app.listen(PORT,()=>{
    console.log('listening to port:' + PORT);
});