const express=require('express');
const bodyParser=require('body-parser');
var userServices=require('../Service/userservice');
var profileServices=require('../Service/profileservice');
const checkUser=require('../session');

var urlEncoder=bodyParser.urlencoded({extended:false});
const app=express();

var router=express.Router();
router.route('/')
.get(function(req,res){
   res.render('suitefront');
});

router.route('/index')
.get(function(req,res){
res.render('index');
});

router.route('/login')
.get(function(req,res){
res.render('login');
});

router.route('/register')
.get(function(req,res){
   res.render('register');
});

router.route('/profile1')
.get(function(req,res){
   res.render('profile1');
});

router.route('/createAccounts')
.get(function(req,res){
   res.render('createAccounts');
});

router.route('/createContacts')
.get(function(req,res){
   res.render('createContacts');
});

router.route('/dashboardpost')
.get(function(req,res){
   res.render('dashboardpost');
});

router.route('/dashboarduser')
.get(function(req,res){
   res.render('dashboarduser');
});


router.route('/dashboardsticky')
    .get(checkUser, (req, res) => {
    console.log('session created');
    res.render('dashboardsticky');
});


router.route('/delete')
.get(function(req,res){
   res.render('delete');
});

router.route('/register')
.post(urlEncoder,function(req,res){
  var User={};
        User.name1=req.body.username;
        User.name2=req.body.accntName;
        User.email1=req.body.email;
        User.email2=req.body.confirmationemail;
        User.password1=req.body.password;
        User.password2=req.body.confirmationPassword;
  
  userServices.RegisterUser(User).then((result)=>{
        
        console.log(result);
       
		console.log('router register, userService result' , result);
            if(result.saved){
			res.redirect('/login');
			// redirect to login page
            }
		else{
			 res.render('register',{msg:'User Name Exists'});
			 // user already exist
		   }
	},
	(error)=>{
		console.log(error);
	});
  });

router.route('/login')
  .post(urlEncoder,function(req,res){
  	console.log(req.body);
  	var loginUserData = {};
  	loginUserData.name1=req.body.uName;
  	loginUserData.password2=req.body.uPassword;
  	console.log(loginUserData);
  	userServices.loginOnValidation(loginUserData).then((result)=>{
           console.log('inLoginValidation', result);
           if(result.exists){
           	if(result.Valid){
               req.session.username=req.body.uName;
                
                console.log('session',req.session.username)

           		res.redirect('dashboardsticky');
           	}
           	else
           	{
           		res.render('login');
           	}
           }
           else
           {
           	res.render('login');
           }
       },
           (error)=>{
		console.log(error);
  });

  });


  router.route('/profile')
  .post(urlEncoder,function(req,res){
  	var user1={};
  	user1.pname=req.body.username;
    user1.pemail=req.body.fname;
    user1.pemail2=req.body.Active;
    user1.ppassword=req.body.lname;

    profileServices.profileData(user1).then((result)=>{
         console.log('profile',result.saved);
         if(result.saved){
          res.render('dashboardsticky');
         }
         else
         {
          res.render('profile1');
         }
    });
  });


  router.route('/logout')
    .get(function(req,res){
      
    
req.session.destroy(function(){
    res.redirect('/login');
        });
    })
    



module.exports=router;
