import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,FormBuilder ,Validators} from '@angular/forms';
import {userData} from '../userData/userData';
import { registerService }  from '../service/registerService';
import { Router,Routes, RouterModule } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
registerForm: FormGroup;
model:userData;
messege='';
  constructor(private rj: FormBuilder,private registerService: registerService,private router:Router) {
  this.model=new userData(); }
  register(){
    this.registerService.postUserData(this.model,(arg)=>{
      console.log(arg);
      if(arg.redirect===true)
      {
         this. router.navigate(['login']);
      }
      else{
      this.messege=arg.msg;
      }
    })
  }

  ngOnInit() {
    this.registerForm=this.rj.group({
      userName1:[''],
      userEmail1:[''],
      userPassword1:['']
    })
  }

}
