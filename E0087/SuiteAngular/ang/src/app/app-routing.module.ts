import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SuitefrontComponent } from './suitefront/suitefront.component';
import { IndexComponent } from './index/index.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {
    path:'',
    component : SuitefrontComponent
  },
  {
    path:'index',
    component : IndexComponent
  },
  {
    path:'register',
    component : RegisterComponent
  },
  {
    path:'login',
    component:LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
