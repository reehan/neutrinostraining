import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class registerService {
 constructor(private http:HttpClient) { }
postUserData(userData:any,callback) {
   console.log('registerService.postUserData:Post being called');
   return this.http.post('http://localhost:3050/ang/register', userData)
    .subscribe((data) => {console.log( 'in subscribe', data)
    callback(data,userData);

    });

  }

}
