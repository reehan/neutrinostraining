import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuitefrontComponent } from './suitefront.component';

describe('SuitefrontComponent', () => {
  let component: SuitefrontComponent;
  let fixture: ComponentFixture<SuitefrontComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuitefrontComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuitefrontComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
