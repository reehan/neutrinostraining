import { Component, OnInit } from '@angular/core';
import {userLogin} from '../userData/userLogin';
import { loginService }  from '../service/loginService';
import { Router,Routes, RouterModule } from '@angular/router';
import { FormGroup, FormControl,FormBuilder ,Validators} from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  model:userLogin;
  messege='';
  constructor(private rj: FormBuilder,private loginService: loginService,private router:Router) {
  this.model=new userLogin(); }
  login(){
    this.loginService.postUserLoginData(this.model,(arg)=>{
      console.log(arg);
      if(arg.redirect===true)
      {
         this. router.navigate(['index']);
      }
      else{
      this.messege=arg.msg;
      }
    })
  }


  ngOnInit() {
    this.loginForm=this.rj.group({
      userName:[''],
      userPassword:['']
    })
  }

  }
