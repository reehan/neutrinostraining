const express = require('express');
const morgan = require('morgan')
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const session = require('express-session');
const mongoose = require('mongoose');
const passport = require('passport');
const validator = require('express-validator');
const ejs = require('ejs');
const multer = require('multer');

//var models = require('./models/index');




//require('./config/passport');

const app = express();
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
      
//app.use('/', models);

const storage = multer.diskStorage({
destination: (req, file, callback)=>{
callback(null, 'uploads/');
},
filename: function (req, file, callback) {
    callback(null, file.originalname);
  }
});

var parse = multer({storage:storage});

app.use(bodyParser.json());
var urlencodedParser = bodyParser.urlencoded({ extended: false });

//app.use(bodyParser.urlencoded({ extended: true }));

app.use(session({
    secret: 'neutrinos',
    resave: false,
    saveUninitialized: true,
    cookie: {secure:false }  // needed certificate
  }));

app.use(passport.initialize());
app.use(passport.session());

app.get('/', (req, res) => {
    res.render('suitefront');
});

app.get('/index', (req, res) => {
    res.render('index');
});

app.get('/register',(req, res) => {
    res.render('register');
  });

app.get('/login',(req, res) => {
    res.render('login');
  });

app.get('/profile1',(req, res) => {
    res.render('profile1');
  });

app.get('/createAccounts',(req, res) => {
    res.render('createAccounts');
  });

app.get('/createContacts',(req, res) => {
    res.render('createContacts');
  });

app.get('/dashboardpost',(req, res) => {
    res.render('dashboardpost');
  });
  
app.get('/dashboardpage',(req, res) => {
    res.render('dashboardpage');
  });

app.get('/dashboarduser',(req, res) => {
    res.render('dashboarduser');
  });
app.get('/delete',(req, res) => {
    res.render('delete');
  });

app.use('/css',express.static('css'));
app.use('/js',express.static('js'));
app.use(express.static('public'));
app.use('/uploads', express.static('uploads'));

app.get('/back.jpg',(req,res)=>{
    //res.send('<h1>hi</h1>');
    res.sendFile(__dirname +'/'+ '/views/back.jpg');

});

app.use(session({
    secret: 'neutrinos',
    resave: false,
    saveUninitialized: true,
    cookie: {secure:false } 
  }));
  
  var checkSignedIn = function(req,res,next){
      console.log(req.session.ename1);
  
  if(req.session.ename1){
      next();
      
  }else{
      res.redirect('/login');
  }
  };
  app.get('/dashboardsticky',checkSignedIn,(req, res) => {
    res.render('dashboardsticky');
  });

  app.get('/logout',(req,res)=>{
    req.session.destroy(function(){
    res.redirect('/');
});
  });
var checkProfileIn = function(req,res,next){
      console.log(req.session.uname1);
  
  if(req.session.uname1){
      next();
      
  }else{
      res.redirect('/profile1');
  }
  };
  app.get('/dashboarduser',checkProfileIn,(req, res) => {
    res.render('dashboarduser');
  });

app.use(validator());
app.use(passport.initialize());
app.use(passport.session());

mongoose.connect('mongodb://localhost/suitecrm');

var userSchema=mongoose.Schema({
    ename1: String,
    //myemail2: String,
    //myname2: String,
    //myconemail3: String,
    password1: String,
    //mypass2:String
});

const user=mongoose.model("login",userSchema);

app.post('/register',urlencodedParser,(req,res)=>{
    
      
//Get the parsed information

var newuser = new user({
    ename1:req.body.username,
    //myemail2: req.body.name,
    //myname2: req.body.email,
    //myconemail3:req.body.confirmationemailcmdcm
    password1: req.body.password,
    //mypass2: req.body.confirmationPassword
    


});
    console.log(newuser);
newuser.save((err)=>{
    if(err)
        res.send('errrrrrr');
     else
    res.redirect('/login');
   //res.redirect('/login');
});
});

app.post('/login',urlencodedParser,(req,res)=>{
    console.log(req.body.uName);
    console.log(req.body.uPassword);
    //console.log(newuser);
    //console.log(user.pass1);
    
    
    user.findOne({ename1:req.body.uName}, (err, response)=>{
       console.log(response);
       console.log(response.ename1);
       

           if(req.body.uPassword===response.password1){
            req.session.ename1 = req.body.uName;
            //req.session.userparams = userobj;
            console.log(req.session.ename1);
//res.send('registered')
      //res.redirect('/dashboardsticky');
      res.render('dashboardsticky',{reg:response.ename1});
           }else
               {
      res.redirect('/login');
               }
     });

  });

var userSchema=mongoose.Schema({
    myname2:String,
    myemail2:String,
    myconemail2:String,
    mypass2:String,
    photoPath: String,
    myname3:String,
    myemail3:String,
    myconemail3:String,
    mypass3:String,
    myname4:String,
    myemail4:String,
    myconemail4:String,
    mypass4:String,
    myname5:String,
    myemail5:String,
    myconemail5:String,
    mypass5:String,
    myname6:String,
    myemail6:String,
    myconemail6:String,
    mypass6:String,
    myname7:String,
    myemail7:String,
    myconemail7:String,
    mypass7:String,
    address:String,
    client:String,
    editor:String
   
    
});
const user1=mongoose.model("acc1",userSchema);
app.post('/profile',urlencodedParser,parse.single('userImage'),(req,res)=>{
    
console.log(req.file);
    
var newuser = new user1({
myname2: req.body.username,
    myemail2: req.body.fname,
    myconemail2: req.body.Active,
mypass2: req.body.lname,
    photoPath: req.file.path,
    myname3: req.body.status,
    myemail3: req.body.file1,
    myconemail3: req.body.ename,
mypass3: req.body.wphone,
    myname4: req.body.title,
    myemail4: req.body.mobile,
    myconemail4: req.body.dept,
mypass4: req.body.ophone,
    myname5: req.body.report,
    myemail5: req.body.fax,
    myconemail5: req.body.field,
mypass5: req.body.iname,
    myname6: req.body.imname,
    myemail6: req.body.city,
    myconemail6: req.body.street,
mypass6: req.body.postal,
    myname7: req.body.state,
    myemail7: req.body.country,
    myconemail7: req.body.text1,
mypass7: req.body.report1,
    address: req.body.email11,
    client: req.body.element1,
editor: req.body.element2
});
    

console.log(newuser);
newuser.save(function(err, user1){
if(err)
res.render('errPage', {message: "Database error", type:
"error"});
     
});
    user1.find({myname3:req.body.myname2},function(err,response){
                    req.session.uname1 = req.body.myname2;

        console.log(response);
                

        res.render('dashboarduser',{reg1:response});
        });
    
    
});

app.post('/delete', function(req, res, next) {
  var id = req.body.id;
  user1.findOneAndDelete(id).exec();
  res.send('deleted successfully')
});

const PhotoUser=mongoose.model("contact",userSchema);

var userSchema=mongoose.Schema({
salutation:String,
firstname:String,
lastname:String,
officephone:String,
mobileNo:String,
title:String,
department:String,
accountname:String,
fax:String,
email:String,
address:String,
city:String,
state:String,
postalCode:String,
country:String,
photoPath: String
    
});


app.get("/contactDetails",(req,res)=>{
res.render("createContacts.ejs");

});

app.post('/createContacts',urlencodedParser,(req,res)=>{
var newuser = new PhotoUser({
    salutation:req.body.salutation,
    firstname: req.body.username,
    lastname: req.body.lastname,
    officephone: req.body.officephone,
    mobileNo: req.body.mobileNo,
    title:req.body.title,
    department:req.body.department,
    accountname:req.body.accountname,
    fax: req.body.fax,  
    email:req.body.email,
    address:req.body.address,
    city:   req.body.city,
    state:req.body.state,
    postalCode:req.body.postalCode,
    country:req.body.country
    
    });
    console.log('Printing user');
    console.log(newuser);
    newuser.save(function(err, PhotoUser){
        if(err)
      res.send('error occured while saving to db');
        else
         res.send('register data got created');
          });
});

//creating accounts




const AccountUser=mongoose.model("account",userSchema);

var userSchemaa=mongoose.Schema({
    name:String,
    officephone:String,
website:String,
fax:String,
email:String,
billing_address:String,
city:String,
state:String,
postal_code:String,
country:String,
description:String,
    photoPath:String
    
    
});


app.get("/accountDetails",(req,res)=>{
res.render("createAccounts.ejs");

});





app.post('/create',urlencodedParser,(req,res)=>{
var newuser = new AccountUser({
    name:req.body.name,
    officephone:req.body.officephone,
website:req.body.website,
fax:req.body.fax,
email:req.body.email,
billing_address:req.body.billing_address,
city:req.body.city,
state:req.body.state,
postal_code:req.body.postal_code,
country:req.body.country,
description:req.body.description
    
    });
    console.log('Printing users Account Details');
    console.log(newuser);
    newuser.save(function(err, AccountUser){
        console.log('to save');
        if(err)
      res.send('error occured while saving to db');
        
          });

          AccountUser.find({myname3:req.body.myname2},function(err,response){
            req.session.uname11 = req.body.name;

console.log(response);
        

res.render('accounts',{acc:response});
});
});

app.listen(3070, () => console.log('Server started listening on port 3000!'));


