export class Regdata{
    firstName:string;
    lastName:string;
    email:string;
    password:string;
    confirmPassword:string;


    constructor(values: Object = {}) {
        Object.assign(this, values);
    }

}
