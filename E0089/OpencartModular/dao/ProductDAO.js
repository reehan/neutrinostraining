const mongoose = require('mongoose');
const productModel = require('../models/productModel.js');

module.exports.saveProductData = function(body, file){
    var myPromise = new Promise(function(resolve, reject){
        var Product = productModel.productObject();
        var product = new Product({
            productName :body.productName,
            description: body.description,
            model: body.model,
            price: body.price,
            dimensions: body.length+'x'+body.height+'x'+body.width+body.lengthClass,
            weight: body.weight+body.weightClass,
            quantity:body.quantity,
            status:body.status,
            image:file.path
        });
        console.log(product);
        product.save((err)=>{   
           if(err)
               { 
                   console.log('productDAO,saveProductDAata', err);
                reject('Error while saving the product data in database'); 
               }
            else
                resolve(true);
        });
    });
    return myPromise;
}
module.exports.getProductData = function(){
    var myPromise = new Promise(function(resolve, reject){
        var Product = productModel.productObject();
        Product.find((err, response)=>{
            if(err){reject("Error while getting the products data from the database");}
            resolve(response);
        });
    });
    return myPromise;
}
module.exports.getSingleProductData = function(productName){
    var myPromise = new Promise(function(resolve, reject){
        var Product = productModel.productObject();
        Product.findOne({productName:productName.trim()},(err, response)=>{
            if(err){reject("Error while getting the products data from the database");}
            resolve(response);
        });
    });
    return myPromise;
}