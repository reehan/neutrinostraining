const mongoose = require('mongoose');
const userDAO = require('../dao/UserDAO');

module.exports.userStatus =async function(body){
    var responseObject = await userDAO.getUserData(body);
    if(!responseObject)
    {
        var result = await userDAO.saveUserData(body);
        if(result)
            return true;
    }
        else
            return false;
}

module.exports.userData = async function(body){
    var user =await userDAO.getUserData(body);
    return user;
}