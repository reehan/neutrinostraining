const productDAO = require('../dao/ProductDAO.js');

module.exports.saveProductData =async function(body, file){
    var result =await productDAO.getSingleProductData(body.productName);
    if(result === null)
    {
        console.log('Result:', result);
        isSaved = await productDAO.saveProductData(body, file);
        return isSaved;
    }
    else
        return false;
}
module.exports.getProductsData = async function(){
    var products =await productDAO.getProductData();
    return products;
}

module.exports.getCartProducts = async function(pnArray){
    var cartProducts = [];
    for(let i=0; i<pnArray.length;i++){
        var eachProduct = await productDAO.getSingleProductData(pnArray[i]);
        cartProducts.push(eachProduct);
    }
    return cartProducts;
}