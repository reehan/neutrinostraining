module.exports.isAdminLogged = function(req, res, next){
	if(req.session.username){
		next();
		}else
		res.redirect('/Admin-Login');
}

module.exports.checkSignedIn = function(req, res, next){
	if(req.session.username){
		next();
		}else
		res.redirect('/Login');
}

module.exports.createSession = async function(body){
    req.login(body.email, function(err){
        if(err)
            return false;
        else
            return true;
    }); 
}
module.exports.createAdminSession = async function(req){
    req.login(req.body.user, function(err){
        if(err)
            return false;
        else
            return true;
    }); 
}