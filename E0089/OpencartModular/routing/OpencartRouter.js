const express = require('express');
const ejs = require('ejs');
const mongoose = require('mongoose');
const multer = require('multer');
const userService = require('../services/UserService.js');
const orderService = require('../services/OrderService.js');
const productService = require('../services/ProductService.js');
const sessionService = require('../services/SessionService');
const validationService = require('../services/ValidationService');

const router = express.Router();

mongoose.connect('mongodb://localhost/ProductData');

var storage = multer.diskStorage({
	destination:(req, file, callback)=>{
		callback(null, 'Uploads/');
		},
	filename:(req, file, callback)=>{
		var fileName = file.originalname;
		callback(null, fileName);
	}
});
var multerParser = multer({storage: storage});

router.get('/opencart.com', (req,res)=>{
    console.log('Serving the Index page');
    res.render('Index');
})
.get('/logo.png',(req, res)=>{
    res.sendFile('./Images/logo.png');
})
.get('/Login',(req,res)=>{
    console.log('Serving the Login page');
    res.render('Login',{disp:'none', noUsers:""});
})
.get('/Register',(req,res)=>{
    console.log('Serving the Register page');
    res.render('Register', {userExist:"",userName:"",disp:'none'});
})
.get('/Admin-Login',(req, res)=>{   
    console.log('Serving Admin-Login Page');
    res.render('Admin-Login');
})
.post('/Login',(req,res)=>{
    console.log('Storing the user data serving the login page');
        userService.userStatus(req.body).then((result)=>{
            if(result)
            res.redirect('/Login');
            else{
                res.render('Register', {userExist:" you are an existing user. Please Login.",userName: req.body.firstname+' '+req.body.lastname, disp: 'block'});
            }
        }).catch((fail)=>{
            res.send(fail.toString());
        });
})
.post('/StoreFront', (req, res)=>{
    console.log('Serving the StoreFront page');
    userService.userData(req.body).then((user)=>{
        if( user !== null){
            if(user.password === req.body.pswd)
            {
                req.login(req.body.email, function(err){
                    req.session.username = user.userName;
                    productService.getProductsData().then((products)=>{
                        if(products === null)
                        {
                            res.render('StoreFront', {products: []});
                        }else
                            res.render('StoreFront', {products: products});
                    }).catch((fail)=>{
                        res.send(fail.toString());
                    });
                });
            }else
                res.redirect('/Login');
        }else
        {
            res.render('Login', {noUsers:'You have not registered with us, please ', disp:'block'});
        }
    }).catch((fail)=>{
        res.send(fail.toString());
    });
})
.get('/StoreFront', sessionService.checkSignedIn, (req, res)=>{
    console.log('Serving the StoreFront page');
    productService.getProductsData().then((products)=>{
        if(products === null)
        {
            res.render('StoreFront', {products: []});
        }else{
            res.render('StoreFront', {products: products});
        }
    }).catch((fail)=>{
        console.log('user', user); 
    });
})
.post('/Cart',(req, res)=>{
    console.log('Serving the cart page');
    validationService.getCartItems(req.body).then((data)=>{
        productService.getCartProducts(data.pnArray).then((cartData)=>{
            if(cartData)
            {
                req.session.cartItems = cartData;
                req.session.quantity = data.quantity;
                res.render('Cart', {productsArray:cartData, quantityArray:data.quantity});
            }else{
                res.render('Cart', {productsArray:[], quantityArray:[]});
            }
        }).catch((error)=>{
                res.send('Error while getting cart items data');
        });
    }).catch((error)=>{
                res.send('Error while getting inputs data from req.body');
    });
})

.post('/Dashboard', (req, res)=>{
    console.log('Serving the dashboard page');
    if(req.body.pwd === 'demo' && req.body.user === 'demo')
    {
        sessionService.createAdminSession(req).then((result)=>{
            req.session.username=req.body.user;
            res.cookie('OpencartCookie' , new Date());
            orderService.getOrdersData().then((response)=>{
                if(response)
                    res.render('Dashboard',{ordersList:response});
                else
                    res.render('Dashboard',{ordersList:[]});
            }).catch((err)=>{
                res.send(err.toString());
            });
        }).catch((err)=>{
            res.send(err.toString());
        });
    }
    else
        res.redirect('/Admin-Login');
})
.get('/Products', sessionService.isAdminLogged, (req, res)=>{
    console.log('Serving the Products page');
    productService.getProductsData().then((response)=>{
        if(response !== null)
        res.render('Products',{products:response});
        else
        res.render('Products',{products:[]});
    });
})
.get('/AddProduct', sessionService.isAdminLogged, (req, res)=>{
   console.log('Serving the add product page');
    res.render('AddProduct');
})
.post('/updatedProductsPage', multerParser.single('image'), (req, res)=>{
    console.log('Serving the Updated products page');
    productService.saveProductData(req.body, req.file).then((result)=>{
        res.redirect('/Products');
    }).catch((err)=>{
        res.send(err.toString());
    });
    
})
.post('/checkout', (req, res)=>{
    console.log('Serving the StoreFront page after the checkout');
    orderService.saveOrderData(req.session.cartItems, req.session.quantity, req.session.username).then((result)=>{
        console.log('order service.saving order data:',result);
        if(result)
        {
            res.redirect('/StoreFront');
        }
    }).catch((err)=>{
        res.send(err.toString());
    });
})
.get('/Admin-Logout', sessionService.isAdminLogged, function(req, res){
    req.session.destroy(function(){
        res.clearCookie('OpencartCookie');
        res.redirect('/Admin-Login');
    });
})
.get('/Logout', sessionService.checkSignedIn, function(req, res){
    req.session.destroy(function(){
        res.clearCookie('OpencartCookie');
        res.redirect('/Login');
    });
})
.get('/Empty',sessionService.checkSignedIn, (req, res)=>{
   res.render('Empty'); 
})
.get('/Empty1',sessionService.isAdminLogged, (req, res)=>{
   res.render('Empty1'); 
})
.get('*', function(req, res){
res.send('<h2>404 page not found...</h2>');
})

module.exports=router;