var express=require('express');
var app=express();
var multer=require('multer');
var bodyparser=require('body-parser');
var ejs=require('ejs');
var mongoose=require('mongoose');
mongoose.connect('mongodb://localhost/ambika');

var mon_sh=mongoose.Schema({
	name:String,
	email:String,
	pass:String,
	img:String
});

var user=mongoose.model("ambika",mon_sh);
var urlen=bodyparser.urlencoded({extended:false});

app.get("/",(req,res)=>{
	res.render("reg");
});

app.set("view engine","ejs");

const storage = multer.diskStorage({
destination: (req, file, callback)=>{
callback(null, 'uploads/');
},
filename: (req, file, callback)=>{
callback(null,file.originalname);
}
});

const upload=multer({storage:storage});



app.post("/user", upload.single('ufile'), (req,res)=>{
		var userInformation=req.body;
		var obj=new user({
			name:userInformation.uname,
			email:userInformation.uemail,
			pass:userInformation.upass,
			img:req.file.path
		});
obj.save((err,dbres)=>{
	res.render('success.ejs');
});
});


app.listen(3000);


