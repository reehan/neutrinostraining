import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HttpModule } from '../../node_modules/@angular/http';
import { RegisterService } from './services/register.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
       ReactiveFormsModule,
       HttpModule
  ],
  providers: [RegisterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
