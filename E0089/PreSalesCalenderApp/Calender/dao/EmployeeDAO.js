const mogoose = require('mongoose');
const empModel = require('../model/EmployeeModel');

module.exports.getEmpData = function(email){
	var promise  = new Promise(function(resolve, reject){
		var emp = empModel.employeeObject();
		emp.find({email:email},(err, response)=>{
			if(err)
			{
				console.log('Entered here...');
				reject(err);
			}
			else
				resolve(response);
		})
	})
	return promise;
}