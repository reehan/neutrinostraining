import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { CalenderService } from '../calender.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm:FormGroup;

  constructor(private calenderService:CalenderService, private router:Router) { }

  ngOnInit() {
	this.loginForm = new FormGroup({
  		email: new FormControl(),
  		password:new FormControl()
  	})
  }

  login(){
    sessionStorage.setItem("email", this.loginForm.value.email);
  	this.calenderService.postLogin(this.loginForm.value,(data)=>{
  		console.log('is Logged: ', data);
      if(data.error)
      {
      console.log('Error', data.error);
  		  this.router.navigate(['error']);
      }
  		else if(!data.isLogged)
      {
       console.log('isLogged:', data.isLogged);
        //this.loginForm.reset();
        this.router.navigate(['login']);
      }
      else{
      console.log('is Logged: ', data.isLogged);
        sessionStorage.setItem('eventsData', JSON.stringify(data.data));
        this.router.navigate(['calender']);
      }
  	})
  }

}
