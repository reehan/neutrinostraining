import { Component, Input } from '@angular/core';
import { ServiceComponent} from '../service/service-name.component';
@Component({
selector: 'app-server',
templateUrl: './server.component.html',
styleUrls: ['./server.component.css']
})
export class ServerComponent {

  private serverDetails: ServiceComponent;

  @Input() serverName1:string;

  constructor(private _serverDetailsService: ServiceComponent) {
    this.serverDetails = _serverDetailsService;
  }

  getServerName() {
  this.serverDetails.serverName=this.serverName1;
    return this.serverDetails.getName();
  }

  ngOnInit() {
  }

  }





