const ejs=require('ejs');

const express=require('express');
const app=express();

const bodyParser=require('body-parser');
const urlenc=bodyParser.urlencoded({extended:false});

const session=require('express-session');

app.set('view engine','ejs');

app.get('/login',(req,res)=>{
	console.log('login');
	res.render('login');
});

app.post('/success',urlenc,(req,res)=>{
	console.log(req.body.eemail);
	console.log(req.body.pswd);
	res.send('success');
});

app.listen(3000);