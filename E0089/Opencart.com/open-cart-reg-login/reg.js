const ejs=require('ejs');

const express=require('express');
const app=express();

const bodyParser=require('body-parser');
const urlenc=bodyParser.urlencoded({extended:false});

const session=require('express-session');

const mon=require('mongoose');

mon.connect('mongodb://localhost/register');

var schema=mon.Schema({
	Name:String,
	Fname:String,
	Email:String,
	Country:String,
	Password:String
});

var model=mon.model('users',schema);

app.set('view engine','ejs');

app.get('/',(req,res)=>{
	res.render('reg');
});

app.post('/login',(req,res)=>{
	console.log('login');
	res.render('login');
});

/*app.post('/success',urlenc,(req,res)=>{
	console.log(req.body.username);
	console.log(req.body.password);
	res.send('success');
});*/

app.use(session({secret:'ambika'}));

app.post('/success',urlenc,(req,res)=>{
	console.log(req.body.eemail);
	console.log(req.body.pswd);
	//res.send('success');
	model.findOne({Email:req.body.eemail},function(err,response){
		if(response.Password===req.body.pswd){
			res.send('success');
		}
		else{
			res.send('invalid user name or password');
		}
	});
});

app.post('/reg',urlenc,(req,res)=>{
	console.log(req.body.username);
	console.log(req.body.firstname);
	console.log(req.body.email);
	console.log(req.body.country_id);
	console.log(req.body.password);
	console.log('session id : '+req.sessionID);
	
	var obj=new model({
		Name:req.body.username,
		Fname:req.body.firstname,
		Email:req.body.email,
		Country:req.body.country_id,
		Password:req.body.password
	});
	
	obj.save((err,data)=>{
		res.send('registered successfully');
	});
});

app.get('/img.png',(req,res)=>{
	res.sendFile(__dirname+'/'+'/views/img.png');
});

/*app.post('/login',(req,res)=>{
	console.log('login');
});*/

app.listen(3000);

