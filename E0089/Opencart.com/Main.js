const express = require('express');
const ejs = require('ejs');
const session = require('express-session');
const mongoose = require('mongoose');
const multer = require('multer');
const bodyParser = require('body-parser');
const http = require('http');
const passport = require('passport');

const app = express();
const server = http.createServer(app);

var latestOrders=[];
var orderNumber=Math.round(Math.random()*10000);
var customerName='Manjunatha R';
var qtty =[];
app.set('view engine', 'ejs');

app.use(express.static('Images'));
app.use(express.static('css'));
app.use(express.static('Java Script'));
app.use(express.static('fonts'));
app.use('/Uploads', express.static('Uploads'));

mongoose.connect('mongodb://localhost/ProductData');

app.use(session({
secret: 'neutrinos',
resave: false,
saveUninitialized: true,
//cookie: { secure: true }
}));

app.use(passport.initialize());
app.use(passport.session());

var userSchema=mongoose.Schema({
	userName: String,
	name: String,
	email: String,
	countryCode: String,
	password: String
});

var productSchema = mongoose.Schema({
    productName: String,
    description: String,
    model: String,
    price : String,
    dimensions: String,
    weight: String,
    quantity: Number,
    status : String,
    image: String
    });

var latestOrderSchema =mongoose.Schema({
    orderId : Number,
    customer : String,
    status : String,
    date : String,
    cost : Number,
});

var storage = multer.diskStorage({
	destination:(req, file, callback)=>{
		callback(null, 'Uploads/');
		},
	filename:(req, file, callback)=>{
		var fileName = file.originalname;
		callback(null, fileName);
	}
});

var Product = mongoose.model("products", productSchema);
var User=mongoose.model('users', userSchema);
var LatestOrder = mongoose.model('orders', latestOrderSchema);

app.use('/Login.ejs', bodyParser.urlencoded({ extended: true}));
app.use('/Dashboard.ejs', bodyParser.urlencoded({ extended: true }));
app.use('/Cart.ejs', bodyParser.urlencoded({ extended: true }));
app.use('/StoreFront.ejs', bodyParser.urlencoded({ extended: true}));
var multerParser = multer({storage: storage});

app.get('/logo.png',(req, res)=>{
    res.sendFile('./Images/logo.png');
});

app.get('/',(req,res)=>{
    console.log('Serving the Index page');
    res.render('Index');
});

app.post('/Login.ejs',(req,res)=>{
    console.log('Storing the user data serving the login page');
    req.on('data' ,(data)=>{
        console.log('data:'+data);
    });
    console.log('User Name:'+req);
    var user = new User({
        userName: req.body.username,
        name : req.body.firstname+' '+req.body.lastname,
        email : req.body.email,
        countryCode : req.body.country_id,
        password : req.body.password
    });
    user.save((err)=>{   
       if(err)
        res.send('Something went wrong while storing User data into the database'); 
        else
        {
            res.redirect('/Login.ejs');
        }
    });
});

app.get('/Login.ejs',(req,res)=>{
    console.log('Serving the Login page');
    res.render('Login');
});

app.get('/Register.ejs',(req,res)=>{
    console.log('Serving the Register page');
    res.render('Register');
});

app.post('/StoreFront.ejs', (req, res)=>{
    console.log('Serving the StoreFront page');
    
    req.login(req.body.userName,function(err){});
   
    User.findOne({email:req.body.email}, (err,response)=>{
        console.log('printing the response:'+response);
        customerName=response.userName;
        if(response.password === req.body.pswd)
        {
            Product.find((err, response)=>{
            res.render('StoreFront', {products:response});
            });
        }
        else
            res.redirect('/Login.ejs');
    });
});

app.post('/Cart.ejs',(req, res)=>{
    
    var m = req.body.abc;
    var a = JSON.parse(m);
    console.log(a);
    var pNames = a.selectedProducts;
    var qty = a.quantity;
    var products=[];
    for(let i=0; i<pNames.length; i++)
    {
        Product.findOne({productName:pNames[i]},(err,response)=>{
            products.push(response);
        });
    }
    setTimeout(function(){ 
    res.render('Cart', {productsArray: products, quantityArray: qty});
    latestOrders = products;
    qtty = qty;
         }, 3000);
});

app.get('/Admin-Login.ejs',(req, res)=>{
    console.log('Serving Admin-Login Page');
    res.render('Admin-Login');
});

app.post('/Dashboard.ejs', (req, res)=>{
    console.log('Serving the dashboard page');
    console.log('UserName:'+req.body.user);
	console.log('Password:'+req.body.pwd);
    if(req.body.pwd === 'demo' && req.body.user === 'demo')
    {
        LatestOrder.find((err, response)=>{
            res.render('Dashboard',{ordersList:response});
        });            
    }
    else
        res.redirect('/Admin-Login.ejs');
});

app.get('/Products.ejs',(req, res)=>{
    console.log('Serving the Products page');
    Product.find((err, response)=>{
        res.render('Products',{products:response});
    });
});

app.get('/AddProduct.ejs', (req, res)=>{
   console.log('Serving the add product page');
    res.render('AddProduct');
});

app.post('/updatedProductsPage', multerParser.single('image'), (req, res)=>{
    console.log('Serving the Updated products page');
    console.log(req.body);
    console.log('The image path:'+req.file.path);
    var product = new Product({
        productName : req.body.productName,
        description: req.body.description,
        model: req.body.model,
        price: req.body.price,
        dimensions: req.body.length+'x'+req.body.height+'x'+req.body.width+req.body.lengthClass,
        weight: req.body.weight+req.body.weightClass,
        quantity:req.body.quantity,
        status:req.body.status,
        image:req.file.path
    });
    product.save((err)=>{   
       if(err)
        res.send('Something went wrong while storing data into the database'); 
        else
        res.redirect('/Products.ejs');
    });
    
});

app.post('/checkout',(req, res)=>{
    console.log('Serving the StoreFront page after the checkout');
    var d = new Date();
    for(let i=0; i<latestOrders.length; i++)
    {
        orderNumber += 1;
        var latestOrder = new LatestOrder({
            orderId : orderNumber,
            customer : customerName,
            status : 'pending',
            date : d.getDate()+'/'+d.getMonth()+'/'+d.getFullYear(),
            cost : latestOrders[i].price*qtty[i]
        });
        latestOrder.save((err)=>{
            if(err)
            res.send('Something went wrong while storing latest order data into the database'); 
        });
    }
    res.redirect('/StoreFront.ejs');
});

app.get('/StoreFront.ejs', (req, res)=>{
    console.log('Serving the StoreFront page');
    Product.find((err, response)=>{
        res.render('StoreFront', {products:response});
    });
});
server.listen(3000);
console.log('Main JS Running');