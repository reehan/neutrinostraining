//creating a Node
function Node(data) {
    this.data = data;
    this.parent = null;
    this.children = [];
}
//passing data to node
function Tree(data) {
    var node = new Node(data);
    this.root = node;
}
 //DFS: traversing
Tree.prototype.traverseDF = function(callback) {
 
//recurssion function    
    (function recurssion(currentNode) {
        
        for (var i = 0, length = currentNode.children.length; i < length; i++) {
            
            recurssion(currentNode.children[i]);
        }
 
        
        callback(currentNode);
 
        
    })(this.root);
 
};
//creating a tree
var tree = new Tree(1);
 
tree.root.children.push(new Node(2));
tree.root.children[0].parent = tree;
 
tree.root.children.push(new Node(3));
tree.root.children[1].parent = tree;
 
tree.root.children.push(new Node(4));
tree.root.children[2].parent = tree;
 
tree.root.children[0].children.push(new Node(5));
tree.root.children[0].children[0].parent = tree.root.children[0];
 
tree.root.children[0].children.push(new Node(6));
tree.root.children[0].children[1].parent = tree.root.children[0];

tree.root.children[2].children.push(new Node(7));
tree.root.children[2].children[0].parent = tree.root.children[2];
 tree.traverseDF(function(node) {
    console.log(node.data);
});
