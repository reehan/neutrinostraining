var express =require('express');
var app=express();
var ejs=require('ejs');
var service=require('../Service/service');
var bodyParser=require('body-parser');
var jsonParser=bodyParser.json();
var eventservice=require('../Service/eventservice');

var router=express.Router();

const multer=require('multer');
 const storage=multer.diskStorage({
 	destination:(req,file,callback)=>{
 		callback(null,'/uploads');
 	},
 	filename:(req,file,callback)=>{
 		callback(null,new Date().toISOString()+file.orihinalname);
 	}
 });

const upload = multer({storage:storage});;
var urlencodedparser=bodyParser.urlencoded({extended:false});



router.route('/login')
	.post(jsonParser,(req,res)=>{
		var userLoginData={};
		console.log(req.body);
		userLoginData.userName=req.body.userName;
		userLoginData.userPassword=req.body.userPassword;
		console.log('userLoginData',userLoginData)
		service.loginOnValidation(userLoginData).then((result)=>{
			console.log('result',result)
			if(result.userExist){
			res.send({msg:'redirect to index',redirect:true});}
			else{
				res.send({msg:'OOPS, something wrong. Please try again',redirect:false});
			}
		},(error)=>{
			console.log(error);
			// res.send({msg:'wrong user name and password',redirect:false});
		})
	});

router.route('/event')
	.post(jsonParser,upload.single("file"),function(req,res){
		var userEventData={};
		console.log(req.body);
		userEventData.EventName=req.body.EventName;
		userEventData.Date=req.body.Date;
		userEventData.time=req.body.time;
		userEventData.file=req.body.file;
		console.log('userEventData',userEventData)
		eventservice.saveEvent(userEventData).then((result)=>{
			console.log('result',result)
			if(result.saved){
			res.send({msg:'redirect to index',redirect:true});}
			else{
				res.send({msg:'OOPS, something wrong. Please try again',redirect:false});
			}
		},(error)=>{
			console.log(error);
			// res.send({msg:'wrong user name and password',redirect:false});
		})
	});



	module.exports=router;