import { Component,OnInit  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// import custom validator to validate that password and confirm password fields match

import { register } from './model/register';

import{RegisterService} from './services/register.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'register Form';
  model:register;



    registerForm: FormGroup;
    submitted = false;

    constructor(private formBuilder: FormBuilder, private registerService:RegisterService) { 
this.model=new register();
    }

submitPersonData(){
    console.log(this.model);
    this.registerService.postPData(this.model);

  }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(8)]],
        });
    }

    get f() { return this.registerForm.controls; 
    }

    onSubmit() {
        this.submitted = true;

        if (this.registerForm.invalid) {
            return;
        }

        alert('done\n\n' + JSON.stringify(this.registerForm.value))
    }
}
