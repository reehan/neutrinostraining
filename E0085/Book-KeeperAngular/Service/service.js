const dao = require('../Dao/dao');

var registration=async function(userEnteredDetails){
  var Object={};

  var returnedFromDao=await dao.find(userEnteredDetails.USEREMAIL);
  console.log(returnedFromDao)
if(returnedFromDao.exist===false){
  await dao.saveUser(userEnteredDetails);
  Object.saved=true;
}
else{
  Object.saved=false;
}
return Object.saved;
}
var login=async function(loginDetails){
  var ReturnVal={};
  var returnedFromDaoFind=await dao.find(loginDetails.EMAIL);
  if(returnedFromDaoFind.exist===true){
  var returnedFromDao=await dao.login(loginDetails);
  if(returnedFromDao===true){
    ReturnVal.success=true;
    ReturnVal.data=returnedFromDaoFind.userDbData;
    console.log('data',ReturnVal.data.Email);
  }
  else{
    ReturnVal.success=false;
  }
}
  else{
    ReturnVal.success=false;
  }
  return ReturnVal;
}
var updateFunc=async function(updateDetails,email){
  var updated={};
  var resultFind=await dao.find(email);
console.log("resultFind",resultFind);
await dao.updateInDao(updateDetails,resultFind.userDbData);

}
var findUser=async function(email){
var resultDao=await dao.find(email)
if(resultDao.exist===true){
  return resultDao.userDbData;
}
else{
  throw new Error("user doesnt exist");
}
}

var service={};
service.registration=registration;
service.login=login;
service.updateFunc=updateFunc;
service.findUser=findUser;
module.exports=service;
