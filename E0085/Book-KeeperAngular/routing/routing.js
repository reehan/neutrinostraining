const express=require('express');
const app=express();
const ejs=require('ejs');
const bodyParser=require('body-parser');
var urlencoder=bodyParser.urlencoded({extended:false});
var jsonParser=bodyParser.json();
var service=require('../Service/service');
var checkedSignedIn=require('../sessionHandler');
var router=express.Router();
// router.route('/')
// .get((req,res)=>{
//   res.render('index');
// });
// router.route('/register')
// .get((req,res)=>{
//   res.render('register');
// });
// router.route('/login')
// .get((req,res)=>{
//   res.render('login');
// });
router.route('/RegSuccess')
  .post(jsonParser,function(req,res){
    var userEnteredDetails={};
  console.log('in register',req.body);
    userEnteredDetails.USEREMAIL=req.body.email;
    userEnteredDetails.USERPASSWORD=req.body.password;
    userEnteredDetails.USERTELEPHONE=req.body.tele;
    userEnteredDetails.USERFIRSTNAME=req.body.fname;
    userEnteredDetails.USERLASTNAME=req.body.lname;
    userEnteredDetails.USERGST=req.body.gst;
    userEnteredDetails.USERCOUNTRY=req.body.country;
    console.log("userEnteredDetails::",userEnteredDetails);
service.registration(userEnteredDetails).then(resFromService=>{
  if(resFromService===true){
    res.send({msg:'user details saved',redirect:true});
  }
  else{
    res.send({msg:'user details exist',redirect:false});
  }
},(error) => {
  console.log(error);
});
});

router.route('/invoice')
.get(checkedSignedIn,(req,res)=>{
  console.log('session',req.session.email)
  service.findUser(req.session.email).then((result)=>{
    console.log('result in invoice',result);
    res.send({email:result.Email,Fname:result.FirstName,Lname:result.LastName,country:result.Country,tele:result.Telephone,gst:result.Gst,password:result.Password})
  },(error)=>{
    console.log('error in invoice',error)
  })
})



router.route('/sucess')
.post(jsonParser,function(req,res){
  var userEnteredLoginDetails={};
  userEnteredLoginDetails.EMAIL=req.body.email1;
  userEnteredLoginDetails.PASSWORD=req.body.pwd1;
  service.login(userEnteredLoginDetails).then((ResultFromService)=>{
console.log('data in routing',ResultFromService.data);
if(ResultFromService.success){
  console.log('resultfromservicce',ResultFromService.data);
  req.session.email=ResultFromService.data.Email;
res.send({redirect:true});
}
else{
  res.send('OOPS, something wrong. Please try again');
}

});
});
router.route('/update')
.post(checkedSignedIn,urlencoder,(req,res)=>{
  var updateUser={};
  updateUser.Fname=req.body.sname;
  updateUser.Lname=req.body.slname;
  updateUser.Country=req.body.scountry;
  updateUser.Telephone=req.body.stele;
  updateUser.Gst=req.body.sgst;
  updateUser.Password=req.body.spass;
  service.updateFunc(updateUser,req.session.email).then((result)=>{
    // console.log("result",result)
      res.send('profile updated');

  })
});

router.route('/logout')
.get(checkedSignedIn,(req,res)=>{
  req.session.destroy(()=>{
    res.send({redirect:true});
  })
})


module.exports=router;
