const express=require('express');
const app=express();
const ejs=require('ejs');
const bodyParser=require('body-parser');
var urlencoder=bodyParser.urlencoded({extended:false});
var service=require('../Service/service');
const checkSignedIn = require('../sessionHandler');


var router=express.Router();
router.route('/')
.get((req,res)=>{
  res.render('index');
});
router.route('/register')
.get((req,res)=>{
  res.render('register');
});
router.route('/login')
.get((req,res)=>{
  res.render('login');
});
router.route('/invoice')
.get(checkSignedIn,(req,res)=>{
    service.login(req.session.email1).then((ResultFromService)=>{
  res.render('invoice',{email:ResultFromService.data.Email,Fname:ResultFromService.data.FirstName,Lname:ResultFromService.data.LastName,country:ResultFromService.data.Country,tele:ResultFromService.data.Telephone,gst:ResultFromService
  .data.Gst,password:ResultFromService.data.Password});
});
});
router.route('/RegSuccess')
  .post(urlencoder,function(req,res){
    var userEnteredDetails={};
    userEnteredDetails.USEREMAIL=req.body.email;
    userEnteredDetails.USERPASSWORD=req.body.pwd;
    userEnteredDetails.USERTELEPHONE=req.body.tele;
    userEnteredDetails.USERFIRSTNAME=req.body.fname;
    userEnteredDetails.USERLASTNAME=req.body.lname;
    userEnteredDetails.USERGST=req.body.gst;
    userEnteredDetails.USERCOUNTRY=req.body.country;
    console.log("userEnteredDetails::",userEnteredDetails);
service.registration(userEnteredDetails).then(resFromService=>{
  if(resFromService===true){
    res.redirect('/login');
  }
  else{
    res.send('user error');
  }
},(error) => {
  console.log(error);
});
});
router.route('/sucess')
.post(urlencoder,function(req,res){
  var userEnteredLoginDetails={};
  userEnteredLoginDetails.EMAIL=req.body.email1;
  userEnteredLoginDetails.PASSWORD=req.body.pwd1;
  service.login(userEnteredLoginDetails).then((ResultFromService)=>{
console.log('data in routing',ResultFromService.data);

// if(ResultFromService){
  req.session.email1=ResultFromService.data.Email;
  console.log(req.session.email1);
res.redirect('/invoice');
// 
// }
// else{
//   re.send('OOPS, something wrong. Please try again');
// }

},(error) => {
  console.log(error);

});

});

router.route('/update')
.post(urlencoder,(req,res)=>{
  var updateDetails={};
  updateDetails.Fname=req.body.sname;
  updateDetails.Lname=req.body.slname;
  updateDetails.Country=req.body.scountry;
  updateDetails.Telephone=req.body.stele;
  updateDetails.Gst=req.body.sgst;
  updateDetails.Password=req.body.spass;

})
module.exports=router;
