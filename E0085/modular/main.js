const express=require('express');
const app=express();
const ejs=require('ejs');
const session=require('express-session');

app.use(session({ resave: false, saveUninitialized: true, secret: 'Neutrinos' }));

var router=require('./routing/routing');
app.use('/',router);
app.use(express.static('Public'));
app.set('view engine','ejs');




app.listen(3001);
console.log('port 3001 running');
