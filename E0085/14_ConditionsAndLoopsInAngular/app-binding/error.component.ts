import { Component } from '@angular/core';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html'
})
export class ErrorComponent {
  disp='none';
  black='black';
  abc(){
    this.disp='block';
    this.black='red';
  }
}
