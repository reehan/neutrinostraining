import { Component } from '@angular/core';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html'
})
export class ServerComponent {
  vari='none';
  veri='black';
  something(){
      this.vari='block';
      this.veri='yellow';
  }
  
}
